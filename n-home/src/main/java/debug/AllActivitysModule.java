package debug;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ren.nearby.home.ANearbyMain;
import ren.nearby.home.HomeActivity;
import ren.nearby.home.NearbyFragmentDynamic;
import ren.nearby.home.NearbyFragmentHome;
import ren.nearby.home.NearbyFragmentMessage;
import ren.nearby.home.NearbyFragmentPrivateLetter;
import ren.nearby.home.NearbyFragmentRelatedToMe;
import ren.nearby.home.mvp.FragmentMainModule;
import ren.nearby.home.mvp.FragmentMeModule;
import ren.nearby.home.NearbyFragmentMe;
import ren.nearby.http.di.ActivityScoped;
import ren.nearby.http.di.BaseActivityComponent;
import ren.nearby.http.di.BaseFragmentComponent;
import ren.nearby.http.di.BaseFragmentV4Component;


/**
 * 配置所有Activity Fragment注入
 * Created by Administrator on 2018/5/3 0003.
 */


@Module(subcomponents = {
        // 注入 activity
        BaseActivityComponent.class,
        //android.app.Fragment 兼容的最低版本是android:minSdkVersion="11" 即3.0版
        BaseFragmentV4Component.class,
        //android.support.v4.app.Fragment 兼容的最低版本是android:minSdkVersion="4" 即1.6版
        BaseFragmentComponent.class
})
public abstract class AllActivitysModule {


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMainModule.class)
    abstract ANearbyMain aNearbyMain();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMainModule.class)
    abstract HomeActivity homeActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentHome nearbyFragmentHome();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentMessage nearbyFragmentMessage();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentMe nearbyFragmentMe();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentDynamic nearbyFragmentDynamic();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentPrivateLetter nearbyFragmentPrivateLetter();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentRelatedToMe nearbyFragmentRelatedToMe();


}
