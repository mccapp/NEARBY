package ren.nearby.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.logger.Logger;

import in.srain.cube.views.ptr.PtrClassicDefaultFooter;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;
import ren.nearby.lib.ui.base.BaseLazyLoadFragment;
import ren.nearby.lib.ui.base.IPresenter;


/**
 * Created by Administrator on 2017/7/27 0027.
 */

public class NearbyFragmentRelatedToMe extends BaseLazyLoadFragment {



    @Override
    protected void initView(View view) {


    }

    @Override
    protected BaseLazyLoadFragment getFragment() {
        return this;
    }

    @Override
    public IPresenter getPresenter() {
        return null;
    }

    @Override
    public void onFragmentFirstVisible() {
        super.onFragmentFirstVisible();
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public void onFragmentPause() {
        super.onFragmentPause();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_neraby_fragment_related_to_me;
    }
}
