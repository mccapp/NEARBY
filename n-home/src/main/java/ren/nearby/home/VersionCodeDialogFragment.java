package ren.nearby.home;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

/**
 * 版本更新操作
 * Created by Administrator on 2018/5/31 0031.
 */

public class VersionCodeDialogFragment extends DialogFragment {

    //0.强制 1.推荐
    int mType;
    String mUrl;
    String mFileStoreDir;
    String mFileStoreName;
    String mRemark;
    boolean mInstall;

    public static VersionCodeDialogFragment newInstance(
            int type,
            String url,
            String fileStoreDir,
            String fileStoreName,
            String remark,
            boolean install) {
        VersionCodeDialogFragment dialogFragment = new VersionCodeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("url", url);
        bundle.putString("fileStoreDir", fileStoreDir);
        bundle.putString("fileStoreName", fileStoreName);
        bundle.putString("remark", remark);
        bundle.putBoolean("install", install);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * setStyle() 的第一个参数有四个可选值：
         * STYLE_NORMAL|STYLE_NO_TITLE|STYLE_NO_FRAME|STYLE_NO_INPUT
         * 其中 STYLE_NO_TITLE 和 STYLE_NO_FRAME 可以关闭标题栏
         * 每一个参数的详细用途可以直接看 Android 源码的说明
         */
        setStyle(VersionCodeDialogFragment.STYLE_NO_TITLE, R.style.into_DialogStyle);
        mType = getArguments().getInt("type", 0);
        mUrl = getArguments().getString("url", "");
        mFileStoreDir = getArguments().getString("fileStoreDir", "");
        mFileStoreName = getArguments().getString("fileStoreName", "");
        mRemark = getArguments().getString("remark", "");
        mInstall = getArguments().getBoolean("install", false);
        if (mType == 0) {
            setCancelable(false);
        }


    }

    public void setUProgress(String progress) {
        Logger.e("setUProgress = " + progress);

        if (btn_version_promptly != null) {
            btn_version_promptly.setText(progress);
        } else {
            Logger.e("progress = " + progress);
        }
    }

    public void setInstall() {
        Logger.e("setInstall = ");
        if (btn_version_promptly != null) {
            btn_version_promptly.setText("安装");
        }
    }

    OnCancelClick onCancelClick;

    public interface OnCancelClick {

        void updateV(String url,
                     String fileStoreDir,
                     String fileStoreName
        );

        void installV();

        void cancelV();

    }

    public void setOnCancelClick(OnCancelClick onCancelClick) {
        this.onCancelClick = onCancelClick;
    }

    Button btn_version_after;
    Button btn_version_promptly;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_update_version_code_dialog, container, false);
        btn_version_after = view.findViewById(R.id.home_btn_version_after);
        TextView tv_version_remark = view.findViewById(R.id.home_tv_version_remark);
        tv_version_remark.setText(mRemark);
        btn_version_promptly = view.findViewById(R.id.home_btn_version_promptly);
        if (mInstall) {
            btn_version_promptly.setText("安装");
        }
        if (mType == 0) {
            btn_version_after.setVisibility(View.GONE);
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_SEARCH) {
                        return true;
                    }
                    return false;
                }

            });
        } else {
            btn_version_after.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCancelClick.cancelV();
                }
            });
        }

        btn_version_promptly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String promptly = btn_version_promptly.getText().toString().trim();
                Logger.e("捡来了" + promptly);
                if (promptly.equals("立即更新")) {
                    btn_version_after.setVisibility(View.GONE);
                    onCancelClick.updateV(mUrl, mFileStoreDir, mFileStoreName);
                } else if (promptly.equals("安装")) {
                    onCancelClick.installV();
                }
            }
        });

        return view;
    }
}
