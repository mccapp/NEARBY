package ren.nearby.home;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;

import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;


import ren.nearby.lib.http.GlideImageLoader;
import ren.nearby.lib.ui.base.BaseLazyLoadFragment;
import ren.nearby.lib.ui.base.IPresenter;
import ren.nearby.lib.ui.view.PileLayout;
import ren.nearby.lib.ui.view.viewcard.CardFragmentPagerAdapter;
import ren.nearby.lib.ui.view.viewcard.CardItem;
import ren.nearby.lib.ui.view.viewcard.CardPagerAdapter;
import ren.nearby.lib.ui.view.viewcard.ShadowTransformer;


/**
 * Created by Administrator on 2017/7/27 0027.
 */

public class NearbyFragmentHome extends BaseLazyLoadFragment {
    //    @BindView(R2.id.swipe)
    SwipeRefreshLayout swipe;
    //    @BindView(R2.id.viewPager)
    ViewPager mViewPager;
    //    @BindView(R2.id.viewPager2)
    ViewPager mViewPager2;
    public PileLayout pileLayout;
    //    @BindView(R2.id.home_banner)
    public Banner home_banner;

    ArrayList<String> dataList = new ArrayList<>();

    private CardPagerAdapter mCardAdapter, mCardAdapter2;
    private CardFragmentPagerAdapter mFragmentCardAdapter;
    private ShadowTransformer mCardShadowTransformer, mCardShadowTransformer2;
    private ShadowTransformer mFragmentCardShadowTransformer, mFragmentCardShadowTransformer2;

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public void onStart() {
        super.onStart();
        //结束轮播
        home_banner.startAutoPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        //结束轮播
        home_banner.stopAutoPlay();
    }

    private List<String> adList = new ArrayList<>();


    @Override
    protected void initView(View view) {
        toolBarBuilder
                .setApplyStatusBarTranslucency(true)
                .build();
        swipe = view.findViewById(R.id.swipe);
        mViewPager = view.findViewById(R.id.viewPager);
        mViewPager2 = view.findViewById(R.id.viewPager2);
        pileLayout = view.findViewById(R.id.pileLayout);
        home_banner = view.findViewById(R.id.home_banner);


        initbanner(null);
        initpager(null);
        swipe.setRefreshing(false);
    }

    @Override
    protected BaseLazyLoadFragment getFragment() {
        return this;
    }

    @Override
    public IPresenter getPresenter() {
        return null;
    }

    public void initbanner(View view) {
        adList.add("http://img-cdn2.luoo.net/site/201707/5976f2d7b064f.jpg");
        adList.add("http://img-cdn2.luoo.net/site/201707/59771edc928c3.jpg");
        adList.add("http://img-cdn2.luoo.net/site/201707/5976eceddaac3.jpg");
        adList.add("http://img-cdn2.luoo.net/site/201707/5968a76cedb6b.jpg");
//        home_banner = (home_banner) view.findViewById(R2.id.home_banner);

        home_banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        home_banner.setViewPagerIsScroll(true);
        home_banner.setBannerAnimation(Transformer.Default);
        home_banner.isAutoPlay(true);
        home_banner.setImageLoader(new GlideImageLoader());
        home_banner.setDelayTime(3000);
        home_banner.setIndicatorGravity(BannerConfig.CENTER);
        home_banner.setImages(adList).start();
        home_banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
//                bannerSwitch(position);
            }
        });
    }

    public void initpager(View view) {
//        mViewPager = (ViewPager) view.findViewById(R2.id.viewPager);
//        mViewPager2 = (ViewPager) view.findViewById(R2.id.viewPager2);
        mCardAdapter = new CardPagerAdapter();
        mCardAdapter.addCardItem(new CardItem(R.string.title_1, R.string.text_2));
        mCardAdapter.addCardItem(new CardItem(R.string.title_1, R.string.text_3));
        mCardAdapter.addCardItem(new CardItem(R.string.title_1, R.string.text_4));
        mCardAdapter.addCardItem(new CardItem(R.string.title_1, R.string.text_5));


        mCardAdapter2 = new CardPagerAdapter();
        mCardAdapter2.addCardItem(new CardItem(R.string.title_1, R.string.text_6));
        mCardAdapter2.addCardItem(new CardItem(R.string.title_1, R.string.text_7));
        mCardAdapter2.addCardItem(new CardItem(R.string.title_1, R.string.text_8));
        mCardAdapter2.addCardItem(new CardItem(R.string.title_1, R.string.text_9));
        mFragmentCardAdapter = new CardFragmentPagerAdapter(getActivity().getSupportFragmentManager(),
                dpToPixels(2, getActivity()));

        mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);
        mFragmentCardShadowTransformer = new ShadowTransformer(mViewPager, mFragmentCardAdapter);

        mCardShadowTransformer2 = new ShadowTransformer(mViewPager2, mCardAdapter2);
        mFragmentCardShadowTransformer2 = new ShadowTransformer(mViewPager2, mFragmentCardAdapter);

        mViewPager.setAdapter(mCardAdapter);
        mViewPager.setPageTransformer(false, mCardShadowTransformer);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager2.setAdapter(mCardAdapter2);
        mViewPager2.setPageTransformer(false, mCardShadowTransformer2);
        mViewPager2.setOffscreenPageLimit(3);
    }


    @Override
    public void onFragmentFirstVisible() {
        super.onFragmentFirstVisible();
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public void onFragmentPause() {
        super.onFragmentPause();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_neraby_fragment_home;
    }

    public void initPle(View view) {
        pileLayout = (PileLayout) view.findViewById(R.id.pileLayout);
        pileLayout.setAdapter(new PileLayout.Adapter() {
            @Override
            public int getLayoutId() {
                // item's layout resource id
                return R.layout.item_layout;
            }

            @Override
            public void bindView(View view, int position) {
                ViewHolder viewHolder = (ViewHolder) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new ViewHolder();
                    viewHolder.imageView = (ImageView) view.findViewById(R.id.imageView);
                    view.setTag(viewHolder);
                }
                // recycled view bind new position
            }

            @Override
            public int getItemCount() {
                // item count
                return dataList.size();
            }

            @Override
            public void displaying(int position) {
                // right displaying the left biggest itemView's position
            }

            @Override
            public void onItemClick(View view, int position) {
                // on item click
            }
        });

    }

    class ViewHolder {
        ImageView imageView;
    }
}
