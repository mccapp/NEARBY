package ren.nearby.home;

import android.content.Context;
import android.content.Intent;

import java.util.Map;

import ren.nearby.lib.http.meepo.annotation.Bundle;
import ren.nearby.lib.http.meepo.annotation.QueryMap;
import ren.nearby.lib.http.meepo.annotation.RequestCode;
import ren.nearby.lib.http.meepo.annotation.TargetClass;
import ren.nearby.lib.http.meepo.annotation.TargetFlags;
import ren.nearby.lib.http.meepo.annotation.TargetPath;


/**
 * Created by Administrator on 2017/8/4 0004.
 */

public interface ActivityRouter {
    @TargetClass(HomeActivity.class)
    @TargetFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
    boolean gotoHome(Context context, @Bundle("title") String title);

    @TargetClass(NearbyFragmentPrivateLetter.class)
    @TargetFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
    boolean gotoFragment(Context context, @Bundle("title") String title);

    @TargetPath("login")
    boolean gotoMainLogin(Context context, @QueryMap Map<String, String> title);

    @TargetPath("login")
    boolean gotoMainLogin(Context context, @QueryMap Map<String, String> title, @RequestCode(102) int code);

    @TargetPath("welcome")
    boolean gotoViewWelcome(Context context, @QueryMap Map<String, String> title);

    @TargetPath("welcome")
    boolean gotoViewWelcome(Context context, @QueryMap Map<String, String> title, @RequestCode(102) int code);


    @TargetPath("login2")
    boolean gotoTestViewLogin(Context context, @QueryMap Map<String, String> title);

    @TargetPath("Compress")
    boolean gotoMainCompress(Context context, @QueryMap Map<String, String> title);

    @TargetPath("Dagger2Template")
    boolean gotoMainDagger2Template(Context context, @QueryMap Map<String, String> title);

    @TargetPath("Tinker")
    boolean gotoMainTinker(Context context, @QueryMap Map<String, String> title);

    @TargetPath("Platform")
    boolean gotoMainPlatform(Context context, @QueryMap Map<String, String> title);


    @TargetPath("h5Web")
    boolean gotoViewH5Web(Context context, @QueryMap Map<String, String> title);

    @TargetPath("X5h5Web")
    boolean gotoMainX5H5Web(Context context, @QueryMap Map<String, String> title);


//    @TargetPath("testviews")
//    boolean gotoTestView(Context context, @QueryMap Map<String, String> title, @RequestCode(102) int a);

    @TargetPath("main")
    boolean gotoC(Context context);
}
