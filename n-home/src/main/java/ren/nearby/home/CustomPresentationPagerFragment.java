package ren.nearby.home;

import android.content.Intent;
import android.view.View;

import com.cleveroad.slidingtutorial.PageFragment;
import com.cleveroad.slidingtutorial.PresentationPagerFragment;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2018/1/3 0003.
 */

public class CustomPresentationPagerFragment extends PresentationPagerFragment implements View.OnClickListener {

    @Override
    protected List<? extends PageFragment> getPageFragments() {
        List<PageFragment> pageFragments = new ArrayList<>();
        pageFragments.add(new FirstCustomPageFragment());
        pageFragments.add(new SecondCustomPageFragment());
        pageFragments.add(new ThirdCustomPageFragment());
        return pageFragments;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.home_fragment_presentation;
    }

    @Override
    public int getViewPagerResId() {
        return R.id.viewPager;
    }

    @Override
    public int getIndicatorResId() {
        return R.id.indicator;
    }

    @Override
    public int getButtonSkipResId() {
        return R.id.tvSkip;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == getButtonSkipResId()) {
          Logger.e("结束");
        }
    }


//滑动结束监听
    @Override
    public void onPageSelected(int position) {
        super.onPageSelected(position);
        Logger.e("结束");
    }
}
