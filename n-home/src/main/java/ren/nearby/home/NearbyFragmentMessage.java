package ren.nearby.home;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import ren.nearby.lib.ui.base.BaseLazyLoadFragment;
import ren.nearby.lib.ui.base.IPresenter;


/**
 * Created by Administrator on 2017/7/27 0027.
 */

public class NearbyFragmentMessage extends BaseLazyLoadFragment implements ViewPager.OnPageChangeListener {
//    @BindView(R2.id.home_message_tabLayout2)
    public SmartTabLayout home_message_tabLayout2;
//    @BindView(R2.id.home_message_viewPager)
    public NearbyViewPager home_message_viewPager;







    @Override
    protected void initView(View view) {
        toolBarBuilder
                .setTitle(getResources().getString(R.string.home_message))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        home_message_tabLayout2 = view.findViewById(R.id.home_message_tabLayout2);
        home_message_viewPager = view.findViewById(R.id.home_message_viewPager);
        FragmentPagerItems pages = FragmentPagerItems.with(getActivity().getApplicationContext())
                .add(R.string.home_nearby_fragment_dynamic, NearbyFragmentDynamic.class)
                .add(R.string.home_nearby_fragment_related_to_me, NearbyFragmentPrivateLetter.class)
                .add(R.string.home_nearby_fragment_private_letter, NearbyFragmentRelatedToMe.class)
                .create();
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getActivity().getSupportFragmentManager(),
                pages);
        home_message_viewPager.setOffscreenPageLimit(pages.size());
        home_message_viewPager.setAdapter(adapter);
        home_message_viewPager.setScanScroll(true);
        home_message_viewPager.addOnPageChangeListener(this);
//        setup(home_message_tabLayout2);
        home_message_tabLayout2.setViewPager(home_message_viewPager);
    }

    @Override
    protected BaseLazyLoadFragment getFragment() {
        return this;
    }

    @Override
    public IPresenter getPresenter() {
        return null;
    }



    @Override
    public void onFragmentFirstVisible() {
        super.onFragmentFirstVisible();
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public void onFragmentPause() {
        super.onFragmentPause();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_neraby_fragment_message;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    //添加tab图标
    public void setup(SmartTabLayout layout) {
        layout.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.main_menu_layout, container, false);
                ImageView icon = (ImageView) view.findViewById(R.id.icon);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText(adapter.getPageTitle(position));
                switch (position) {
                    case 0:
                        icon.setImageDrawable(getResources().getDrawable(R.drawable.nearby_home_menu_selector));
                        break;
                    case 1:

                        icon.setImageDrawable(getResources().getDrawable(R.drawable.nearby_message_menu_selector));
                        break;
                    case 2:
                        icon.setImageDrawable(getResources().getDrawable(R.drawable.nearby_me_menu_selector));
                        break;
                    default:
                        throw new IllegalStateException("Invalid position: " + position);
                }
                return view;
            }
        });

    }
}
