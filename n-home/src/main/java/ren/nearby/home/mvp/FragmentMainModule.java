package ren.nearby.home.mvp;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ren.nearby.bean.beans.LoginBean;
import ren.nearby.lib.http.scope.ActivityScoped;


/**
 * Created by Administrator on 2018/5/14 0014.
 */

@Module()
public abstract class FragmentMainModule {

    /**
     * 构建数据 使用注解  @Provides 标记方式 provideXXX 类为@Module
     */
    @Provides
    static LoginBean provideLoginBean() {
        return new LoginBean(" 帅气 ", true);
    }

    @ActivityScoped
    @Binds
    abstract FragmentContract.Action taskAction(FragmentActAction fragmentActAction);

}
