package ren.nearby.home.mvp;


import java.io.File;

import ren.nearby.bean.beans.VersionBean;
import ren.nearby.lib.ui.base.BasePresenter2;
import ren.nearby.lib.ui.base.BaseView;

/**
 * Created by Administrator on 2018/5/14 0014.
 */

public class FragmentContract {

    public interface View extends BaseView<FragmentActAction> {

        void showUpload(VersionBean bean);

        void setProgress(long contentLength, long progress, long total);

        void startUi();

        void installationApk(File file);

    }

    public interface Action extends BasePresenter2<View> {
        void updateApk(String url,
                       String mFileStoreDir,
                       String mFileStoreName
        );

        void test();
    }
}
