package ren.nearby.home;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.base.BaseActivity;


/**
 * 测试顶部标题栏
 * Created by Administrator on 2018/1/8 0008.
 */

public class TopBarBaseActivity extends BaseActivity {


//    @BindView(R2.id.container)
    FrameLayout container;

    @Override
    public int getLayoutRes() {
        Logger.e("child getLayoutRes");

        return R.layout.home_we;
    }



    @Override
    public boolean isShowLeftIcon() {
        return true;
    }

    @Override
    public boolean isShowRightIcon() {
        return true;
    }

    @Override
    public BaseActivity getActivity() {
        return this;
    }

    @Override
    public String showTitle() {
        return getResources().getString(R.string.home_name);
    }

    @Override
    public void initView() {
        super.initView();
        container = findViewById(R.id.container);
        Logger.e("进入");
        ImageView iv = new ImageView(this);
        iv.setBackgroundResource(R.mipmap.home_s_0_1);
        container.addView(iv);
    }
}
