package ren.nearby.home;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.orhanobut.logger.Logger;

import org.w3c.dom.Text;

//import co.mobiwise.materialintro.shape.Focus;
//import co.mobiwise.materialintro.shape.FocusGravity;
//import co.mobiwise.materialintro.view.MaterialIntroView;
import ren.nearby.lib.ui.base.BaseLazyLoadFragment;
import ren.nearby.lib.ui.base.IPresenter;


/**
 * Created by Administrator on 2017/7/27 0027.
 */

public class NearbyFragmentDynamic extends BaseLazyLoadFragment {


    @Override
    protected void initView(View view) {

    }

    @Override
    protected BaseLazyLoadFragment getFragment() {
        return this;
    }

    @Override
    public IPresenter getPresenter() {
        return null;
    }


    @Override
    public void onFragmentFirstVisible() {
        super.onFragmentFirstVisible();
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public void onFragmentPause() {
        super.onFragmentPause();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_neraby_fragment_dynamic;
    }


}
