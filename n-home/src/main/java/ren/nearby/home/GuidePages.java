package ren.nearby.home;

import android.net.Uri;
import android.support.v4.content.ContextCompat;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2018/1/3 0003.
 */

public class GuidePages extends BaseActivity {
    private int[] mPagesColors;
    private static final int ACTUAL_PAGES_COUNT = 3;

    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.home_home))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        mPagesColors = new int[]{
                ContextCompat.getColor(this, android.R.color.darker_gray),
                ContextCompat.getColor(this, android.R.color.holo_green_dark),
                ContextCompat.getColor(this, android.R.color.holo_red_dark),
                ContextCompat.getColor(this, android.R.color.holo_blue_dark),
                ContextCompat.getColor(this, android.R.color.holo_purple),
                ContextCompat.getColor(this, android.R.color.holo_orange_dark),
        };
        replaceTutorialFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.home_we;
    }


    public void replaceTutorialFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new CustomPresentationPagerFragment())
                .commit();
    }

    @Override
    public void onIntent() {
        super.onIntent();
        //获取浏览器传送过过来的值
        Uri uri = getIntent().getData();
        if (uri != null) {
            String title = uri.getQueryParameter("title");
            Logger.e(" uri != null title = " + title);
        } else {
            Logger.e(" uri = null");
        }

    }
}
