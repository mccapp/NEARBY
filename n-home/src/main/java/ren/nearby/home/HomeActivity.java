package ren.nearby.home;

import android.net.Uri;
import android.view.View;

import com.orhanobut.logger.Logger;

import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;
import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2017/8/3 0003.
 */

public class HomeActivity extends BaseActivity {

    PtrFrameLayout ptr_frame_layout;


    @Override
    public void onClickLeft(View v) {
        super.onClickLeft(v);
        Logger.e("on left");
    }

    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.home_home))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        ptr_frame_layout =  findViewById(R.id.ptr_frame_layout);
        //基础默认属性设置
        ptr_frame_layout.setPinContent(false);//[可选]原生状态[下拉遮盖ui]
        ptr_frame_layout.setLoadingMinTime(1000);
        ptr_frame_layout.setDurationToCloseHeader(1500);
        ptr_frame_layout.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                Logger.e("onLoadMoreBegin");
                ptr_frame_layout.refreshComplete();
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                Logger.e("onRefreshBegin");
                ptr_frame_layout.refreshComplete();
            }
        });
    }

    @Override
    public void onIntent() {
        super.onIntent();
        Uri uri = getIntent().getData();
        if (uri != null) {
            String value1 = uri.getQueryParameter("value1");
            String value2 = uri.getQueryParameter("value2");
            String value3 = uri.getQueryParameter("value3");
            setTitle(value1 + " - " + value2 + " - " + value3);
            if (value3 != null) {
                setResult(121);
                finish();
            }
        } else {
            String  title = getIntent().getStringExtra("title");
            if(title!=null){
                setTitle(title);
            }
        }

        Logger.e(" Home 组件");
    }

    @Override
    public int getLayoutRes() {
        return R.layout.home_home;
    }


}
