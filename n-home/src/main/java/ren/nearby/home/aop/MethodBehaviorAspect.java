package ren.nearby.home.aop;


import com.orhanobut.logger.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * https://www.jianshu.com/p/f90e04bcb326
 * https://www.cnblogs.com/weizhxa/p/8567942.html
 * Created by Administrator on 2018/9/30 0030.
 */

@Aspect
public class MethodBehaviorAspect {


    private static final String TAG = "aspect_aby";

    @Pointcut("execution(@ren.nearby.home.aop.HomeAspect * *(..))")
    public void vehavior() {
    }

    @Before("execution(* ren.nearby.home..*2(..))")
    public void sBefore(JoinPoint joinPoint) {
        Logger.e("来了...Before");
    }
//
//    @After("vehavior()")
//    public void sAfter(JoinPoint joinPoint) {
//        Logger.e("来了...After");
//    }

    @Around("vehavior()")
    public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger.e("来了...");

        Logger.e("@Around");
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();
        HomeAspect behaviorTrace = methodSignature.getMethod().getAnnotation(HomeAspect.class);
        String value = behaviorTrace.value();
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long duration = System.currentTimeMillis() - start;
        Logger.e(String.format("%s类中%s方法执行%s功能,耗时：%dms)", className, methodName, value, duration));

        return result;
    }

}
