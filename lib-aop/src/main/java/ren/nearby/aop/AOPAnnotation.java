package ren.nearby.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Target 注解目标，表示注解使用在什么地方，这里是METHOD方法；
 * @Retention 保留策略，表示注解调用时机，这里RUNTIME运行时
 */
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface AOPAnnotation {
    String value();
}

/**
 * https://www.cnblogs.com/weizhxa/p/8567942.html
 * Created by Administrator on 2018/9/30 0030.
 */
