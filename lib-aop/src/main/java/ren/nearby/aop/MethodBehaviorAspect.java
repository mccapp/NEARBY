package ren.nearby.aop;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * Created by Administrator on 2018/9/30 0030.
 */

@Aspect
public class MethodBehaviorAspect {


    private static final String TAG = "aspect_aby";

    @Pointcut("execution(@ren.nearby.aop.AOPAnnotation * *(..))")
    public void vehavior() {
    }

    @Around("vehavior()")
    public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("AOP库 来了...");

        System.out.println("AOP库 @Around");
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();
        AOPAnnotation behaviorTrace = methodSignature.getMethod().getAnnotation(AOPAnnotation.class);
        String value = /*behaviorTrace.value()*/"";
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long duration = System.currentTimeMillis() - start;
        System.out.println( String.format("AOP库%s类中%s方法执行%s功能,耗时：%dms)", className, methodName, value, duration));

        return result;
    }

}
