// MessageSender.aidl
//定义进程通信接口方法
// in  只能由客户端向服务器发送消息
// out 只能由服务器向客户端发送消息
// inout 双向发送消息
package ren.nearby.main.aidl;


import ren.nearby.main.aidl.data.MessageModel;

interface MessageSender {

 void sendMessage(in MessageModel messageModel);
}
