package debug;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ren.nearby.http.di.ActivityScoped;
import ren.nearby.http.di.BaseActivityComponent;
import ren.nearby.http.di.BaseFragmentComponent;
import ren.nearby.http.di.BaseFragmentV4Component;
import ren.nearby.main.CompressAct;
import ren.nearby.main.NWelcome;
import ren.nearby.main.PlatformAct;
import ren.nearby.main.TinkerAct;
import ren.nearby.main.login.LoginAct;
import ren.nearby.main.music.MusicAct;
import ren.nearby.main.tdagger2.TemplateDaggerAct;
import ren.nearby.main.tdagger2.TemplateDaggerAct2;
import ren.nearby.main.tdagger2.TemplateDaggerAct2Module;
import ren.nearby.main.tdagger2.TemplateDaggerActModule;
import ren.nearby.main.web.WebAct;


/**
 * 配置所有activity注入
 * Created by Administrator on 2018/5/3 0003.
 */


@Module(subcomponents = {
        // 注入 activity
        BaseActivityComponent.class,
        //android.app.Fragment 兼容的最低版本是android:minSdkVersion="11" 即3.0版
        BaseFragmentV4Component.class,
        //android.support.v4.app.Fragment 兼容的最低版本是android:minSdkVersion="4" 即1.6版
        BaseFragmentComponent.class
})
public abstract class AllActivitysModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerActModule.class)
    abstract LoginAct loginAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerActModule.class)
    abstract TemplateDaggerAct templateDaggerAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract TemplateDaggerAct2 templateDaggerAct2();


    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract MusicAct musicAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract TinkerAct tinkerAct();


    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract CompressAct compressAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract NWelcome nWelcome();


    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract WebAct webAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract PlatformAct platformAct();

}
