package ren.nearby.main;

import android.content.Context;
import android.content.Intent;

import java.util.Map;

import ren.nearby.lib.http.meepo.annotation.Query;
import ren.nearby.lib.http.meepo.annotation.QueryMap;
import ren.nearby.lib.http.meepo.annotation.RequestCode;
import ren.nearby.lib.http.meepo.annotation.TargetPath;


/**
 * Created by Administrator on 2017/8/4 0004.
 */

public interface ActivityRouter {
    @TargetPath("a")
    boolean gotoA(Context context, @Query("title") String title);

    @TargetPath("a")
    boolean gotoB(Context context, @QueryMap Map<String, String> title);

    @TargetPath("a")
    boolean gotoC(Context context, @QueryMap Map<String, String> title, @RequestCode(102) int a);

    @TargetPath("main")
    boolean gotoC(Context context);

    @TargetPath("welcome")
    boolean gotoViewWelcome(Context context, @QueryMap Map<String, String> title, @RequestCode(102) int code);


}
