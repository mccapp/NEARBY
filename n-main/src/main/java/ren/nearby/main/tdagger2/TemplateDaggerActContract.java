package ren.nearby.main.tdagger2;

import android.content.Context;

import ren.nearby.lib.ui.base.BasePresenter2;
import ren.nearby.lib.ui.base.BaseView;


/**
 * Created by Administrator on 2018/5/4 0004.
 */

public class TemplateDaggerActContract {

    interface View extends BaseView<TemplateDaggerActAction> {

        Context getContext();
    }

    interface Action extends BasePresenter2<View> {


        void dowApk();
    }
}
