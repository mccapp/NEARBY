package ren.nearby.main.tdagger2;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.orhanobut.logger.Logger;


import javax.inject.Inject;

import ren.nearby.bean.beans.LoginBean;
import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.main.R;


/**
 * 测试告别dagger2模板
 * Created by Administrator on 2018/5/2 0002.
 */


public class TemplateDaggerAct extends BaseActivity implements TemplateDaggerActContract.View{

    @Inject
    LoginBean loginBean;


    @Override
    public int getLayoutRes() {
        return R.layout.main_dagger2_template;
    }

    @Override
    public void onIntent() {

        templateDaggerActAction.takeView(this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setPresenter(TemplateDaggerActAction presenter) {

    }

    public void template1(View view) {
        startActivity(new Intent(TemplateDaggerAct.this, TemplateDaggerAct2.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void initView() {
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_template_dagger))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        Logger.e(loginBean == null ?
                "value = 1" :
                " value = 0" + loginBean.getUsername() + " - " + loginBean.isIps());

        Logger.e(templateDaggerActAction == null ?
                "templateDaggerActAction value = 1" :
                "templateDaggerActAction value = 0" );
        if(templateDaggerActAction!=null){
            templateDaggerActAction.dowApk();
        }

    }
    @Inject
    TemplateDaggerActContract.Action templateDaggerActAction;
}
