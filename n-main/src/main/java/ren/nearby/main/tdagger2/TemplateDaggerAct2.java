package ren.nearby.main.tdagger2;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import ren.nearby.bean.beans.LoginBean;

import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.main.R;

/**
 * 测试告别dagger2模板
 * Created by Administrator on 2018/5/2 0002.
 */


public class TemplateDaggerAct2 extends BaseActivity implements TemplateDaggerActContract.View{


    @Inject
    LoginBean loginBean;


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_dagger2_template2;
    }

    @Override
    public void setPresenter(TemplateDaggerActAction presenter) {

    }

    @Override
    public void onIntent() {

        templateDaggerActAction.takeView(this);
    }
    public void template2(View view) {
        startActivity(new Intent(TemplateDaggerAct2.this, TemplateDaggerAct.class));
        finish();
    }

    @Override
    public void initView() {
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_template_dagger))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        /**
         * @Component(modules = {TemplateDaggerActModule.class,})
         * public interface DTAMComponent {
         * void inject(TemplateDaggerAct templateDaggerAct);
         * }
         * @Module
         * public abstract class TemplateDaggerActModule {
         *
         *
         * public TemplateDaggerActModule(){}
         * @Provides
         * static LoginBean provideLoginBean() {
         * return new LoginBean("name", true);
         * }
         * }
         *  DaggerDTAMComponent.builder().build().inject(this); //可行方式
         *
         *
         */
//        DaggerDTAMComponent.builder().build().inject(this); //可行方式
        Logger.e(loginBean == null ?
                "value = 12" :
                " value = 02" + loginBean.getUsername() + " - " + loginBean.isIps());

        Logger.e(templateDaggerActAction == null ?
                "templateDaggerActAction value = 1" :
                "templateDaggerActAction value = 0" );
        if(templateDaggerActAction!=null){
            templateDaggerActAction.dowApk();
        }

    }
    @Inject
    TemplateDaggerActContract.Action templateDaggerActAction;
}
