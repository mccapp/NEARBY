package ren.nearby.main.tdagger2;



import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ren.nearby.bean.beans.LoginBean;
import ren.nearby.lib.http.scope.ActivityScoped;

/**
 * Created by Administrator on 2018/5/3 0003.
 */

@Module()
public abstract class TemplateDaggerActModule {


    /**
     * 构建数据 使用注解  @Provides 标记方式 provideXXX 类为@Module
     */
    @Provides
    static LoginBean provideLoginBean() {
        return new LoginBean(" 帅气 ", true);
    }


    @ActivityScoped
    @Binds
    abstract TemplateDaggerActContract.Action taskAction(TemplateDaggerActAction templateDaggerActAction);
}
