package ren.nearby.main.tdagger2;


import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import ren.nearby.lib.http.HttpApi;


/**
 * Created by Administrator on 2018/5/4 0004.
 */


public class TemplateDaggerActAction implements TemplateDaggerActContract.Action {


    @Inject
    HttpApi mApi;

    @Inject
    TemplateDaggerActAction() {
//        mApi = api;
    }

    @Override
    public void dowApk() {
        Logger.e(mView == null ? " mView =  0 " : " mView = 1 ");
        Logger.e(mApi == null ? " mApi =  0 " : " mApi = 1 ");
        if (mApi != null) {
            mApi.dowApk();
        }

    }

    TemplateDaggerActContract.View mView;  // 需要抽象出来

    @Override
    public void takeView(TemplateDaggerActContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }
}
