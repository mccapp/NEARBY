package ren.nearby.main.mvp;

import com.orhanobut.logger.Logger;

import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Observable;
import ren.nearby.bean.beans.APIBean;
import ren.nearby.lib.http.BaseObserver;
import ren.nearby.lib.http.HttpApi;
import ren.nearby.lib.http.RxResultHelper;
import ren.nearby.lib.http.SchedulersCompat;

/**
 * Created by Administrator on 2018/5/14 0014.
 */

public class RequestAPIAction implements RequestAPIContract.Action {
    RequestAPIContract.View mView;  // 需要抽象出来
    @Inject
    HttpApi mApi;

    @Inject
    RequestAPIAction() {
//        mApi = api;
    }

    @Override
    public void takeView(RequestAPIContract.View view) {
        Logger.e(mView == null ? " mView =  0 " : " mView = 1 ");
        Logger.e(mApi == null ? " mApi =  0 " : " mApi = 1 ");
        mView = view;
        if (mApi != null) {
//            mApi.dowApk();
        }
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void test() {
        Logger.e("test");
        Observable observable = mApi.testAPI("1012002");


        BaseObserver baseObserver = new BaseObserver<APIBean>(mView.getContext()) {
            @Override
            public void onError(Throwable e) {

                mView.loadError();
            }

            @Override
            public void onNext(APIBean bean) {
                int type = new Random().nextInt(3) + 1;
                Logger.e("onNext  / "+type);

                switch (type) {
                    case 1:
                        mView.loadSucces();
                        break;
                    case 2:
                        mView.loadEmpty();
                        break;
                    case 3:
                        mView.loadError();
                        break;

                }

            }
        };
        observable.compose(SchedulersCompat.IO_TRANSFORMER)//处理线程
                .

                        compose(RxResultHelper.handleResult44())
                .

                        subscribe(baseObserver);
    }
}
