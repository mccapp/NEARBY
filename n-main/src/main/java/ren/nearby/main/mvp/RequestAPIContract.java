package ren.nearby.main.mvp;

import android.app.Activity;
import ren.nearby.lib.ui.base.BasePresenter2;
import ren.nearby.lib.ui.base.BaseView;

/**
 * Created by Administrator on 2018/5/14 0014.
 */

public class RequestAPIContract {

    public interface View extends BaseView<RequestAPIAction> {

        Activity getContext();
        void loadEmpty();
        void loadSucces();
        void loadError();

    }

    public interface Action extends BasePresenter2<View> {

        void test();
    }
}
