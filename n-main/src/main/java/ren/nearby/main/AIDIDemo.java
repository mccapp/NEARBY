package ren.nearby.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.main.aidl.MessageSender;
import ren.nearby.main.aidl.TMessageService;
import ren.nearby.main.aidl.data.MessageModel;


/**
 * Created by Administrator on 2017/10/17 0017.
 */

public class AIDIDemo extends BaseActivity {
    private MessageSender messageSender;



    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_web))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        setupService();
    }

    public void setupService() {
        Intent intent = new Intent(this, TMessageService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
        
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Logger.e("服务器连接接成功");
            messageSender = MessageSender.Stub.asInterface(service);
            MessageModel messageModel = new MessageModel();
            messageModel.setFrom("client user id ");
            messageModel.setTo("receiver userid ");
            messageModel.setContent("this is message content");
            try {
                //发送消息
                messageSender.sendMessage(messageModel);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Logger.e("服务器在断开连接");
        }
    };

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        Logger.e("onDestroy serviceConnection = " + serviceConnection == null ? "1" : "0");
        super.onDestroy();


    }
}
