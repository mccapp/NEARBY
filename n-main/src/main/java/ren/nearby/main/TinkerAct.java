package ren.nearby.main;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.orhanobut.logger.Logger;
import com.tencent.tinker.lib.tinker.TinkerInstaller;

import java.io.File;

import javax.inject.Inject;

import io.github.tonnyl.light.Light;
import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.lib.ui.base.ToolBarHelper;
import ren.nearby.main.mvp.RequestAPIAction;
import ren.nearby.main.mvp.RequestAPIContract;


/**
 * Created by Administrator on 2018/4/25 0025.
 */

public class TinkerAct extends BaseActivity implements RequestAPIContract.View {
    String strPath;
    Button main_btn_tinker;
    Button main_btn_enter_exit;
    Button main_btn_api;
    Button main_btn_picture;
    ImageView main_iv_picture;
    @Inject
    RequestAPIContract.Action action;

    @Override
    public int getLayoutRes() {
        return R.layout.main_tiker;

    }


    @Override
    public void initView() {
        super.initView();
        action.takeView(this);
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_tinker))
                .setApplyStatusBarTranslucency(true)
                .setBackgroundColor(R.color.centre_button_color)
//                .setApplyStatusBarColor(true)
//                .setTitleColor(R.color.white)
                .setPageStateLayout(getLayoutRes(), pageStateLayout)
                .setonEmptyListener(new ToolBarHelper.OnEmptyListener() {
                    @Override
                    public void onEmptyClick() {
                        Logger.e("onEmptyClick");
                        toolBarBuilder.onLoading();
                        action.test();
                    }
                }).setonErrorListener(new ToolBarHelper.OnErrorListener() {
            @Override
            public void onOnErrorClick() {
                Logger.e("onOnErrorClick");
                toolBarBuilder.onLoading();
                action.test();
            }
        })
                .build();


        strPath = getExternalCacheDir().getAbsolutePath() + File.separatorChar;
        main_btn_tinker = findViewById(R.id.main_btn_tinker);
        main_btn_enter_exit = findViewById(R.id.main_btn_enter_exit);

        //添加热更新的代码-----------------------------------------------------------------start
        main_btn_picture = findViewById(R.id.main_btn_picture);
        main_btn_picture.setVisibility(View.VISIBLE);
        main_iv_picture = findViewById(R.id.main_iv_picture);
        main_btn_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_iv_picture.setVisibility(View.VISIBLE);
                Light.info(main_btn_tinker, "show image ", Light.LENGTH_SHORT).show();
            }
        });
        //添加热更新的代码-----------------------------------------------------------------end
        //加载tinker
        main_btn_tinker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File pathFile = new File(strPath, "patch_signed_7zip.apk");
                if (pathFile.exists()) {
                    TinkerInstaller.onReceiveUpgradePatch(
                            getActivity().getApplicationContext(),
                            pathFile.getAbsolutePath());
                    Light.info(main_btn_tinker, "File Exists,Please wait a moment ", Light.LENGTH_SHORT).show();
                } else {
                    Light.error(main_btn_tinker, "File No Exists", Light.LENGTH_SHORT).show();
                }
            }
        });
        main_btn_api = findViewById(R.id.main_btn_api);
        main_btn_api.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pageStateLayout.onLoading();
                toolBarBuilder.onLoading();
                action.test();
            }
        });
        main_btn_enter_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TinkerAct.this, PlatformAct.class));
            }
        });
//        builder.onLoading();
//        pageStateLayout.onLoading();
//        action.test();
    }

    @Override
    public void setPresenter(RequestAPIAction presenter) {

    }

    @Override
    public boolean isStateLayout() {
        return false;
    }


    @Override
    public void loadEmpty() {
        toolBarBuilder.onEmpty();
    }

    @Override
    public void loadSucces() {
        toolBarBuilder.onSucceed();

//        pageStateLayout.onSucceed();
    }

    @Override
    public void loadError() {
        toolBarBuilder.onError();
//        pageStateLayout.onError();
    }

    @Override
    public Activity getContext() {
        return this;
    }
}
