package ren.nearby.main.aidl;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.orhanobut.logger.Logger;

import ren.nearby.main.aidl.data.MessageModel;


/**
 * Created by Administrator on 2017/10/17 0017.
 */

public class TMessageService extends Service {

    public TMessageService() {

    }

    IBinder messageSender = new MessageSender.Stub() {
        @Override
        public void sendMessage(MessageModel messageModel) throws RemoteException {
            Logger.e("messageModel: " + messageModel.toString());
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messageSender;
    }

}
