package ren.nearby.main.service;

/**
 * 音乐回调接口
 * Created by Administrator on 2017/9/19 0019.
 */

public interface OnPlayerEventListener {

    //更新进度
    void onPublish(int progress);

    //切换歌曲
    void onChange(Music music);

    //暂停播放
    void onPlayerPause();

    //继续播放
    void onPlayerResume();

    //更新定时播放时间
    void onTimer(long remain);
}
