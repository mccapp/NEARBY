package ren.nearby.main.service;

/**
 * Created by Administrator on 2017/9/19 0019.
 */

public interface Actions {
    //暂停
    String ACTION_MEDIA_PLAY_PAUSE = "ren.nearby.ACTION_MEDIA_PLAY_PAUSE";
    //下一首
    String ACTION_MEDIA_NEXT = "ren.nearby.ACTION_MEDIA_NEXT";
    //上一首
    String ACTION_MEDIA_PREVIOUS = "ren.nearby.ACTION_MEDIA_PREVIOUS";
    //音量改变
    String VOLUME_CHANGED_ACTION = "ren.nearby.VOLUME_CHANGED_ACTION";

}
