package ren.nearby.main;


import dagger.Component;


import ren.nearby.lib.http.HttpModule;
import ren.nearby.lib.http.data.TasksRepository;
import ren.nearby.lib.http.data.TasksRepositoryComponent;
import ren.nearby.lib.http.scope.ContractScoped;
import ren.nearby.main.login.LoginAction;


@ContractScoped
@Component(dependencies = TasksRepositoryComponent.class, modules = HttpModule.class)
public interface MainComponent {

    TasksRepository getTasksRepository();


}
