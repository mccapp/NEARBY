package ren.nearby.main.login;

import dagger.Component;
import ren.nearby.lib.http.data.TasksRepositoryComponent;
import ren.nearby.lib.http.scope.ContractScoped;


/**
 * Created by Administrator on 2017/9/6 0006.
 */

@ContractScoped
@Component(dependencies = TasksRepositoryComponent.class, modules = ActionModule.class)
public interface LoginContractComponent {
    LoginAction getAction();
}
