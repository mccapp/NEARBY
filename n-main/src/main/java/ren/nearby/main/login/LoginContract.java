package ren.nearby.main.login;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import ren.nearby.lib.http.data.ResponseCallBack;
import ren.nearby.lib.ui.base.IPresenter;


/**
 * Created by Administrator on 2017/9/6 0006.
 */

public interface LoginContract {
    interface View  {

        Context getContext();
    }

    interface Action extends IPresenter {

        void registered(Map<String, Object> map, ResponseCallBack callBack);

        void login(Map<String, Object> map, ResponseCallBack callBack);

        void collection(Map<String, Object> map, ResponseCallBack callBack);

         void testToken(Map<String, Object> map, ResponseCallBack callBack);

         void dowApk(ResponseCallBack callBack);
    }
}
