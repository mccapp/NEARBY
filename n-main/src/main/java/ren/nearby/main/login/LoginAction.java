package ren.nearby.main.login;

import android.text.TextUtils;

import com.orhanobut.logger.Logger;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ren.nearby.lib.http.BasePresenter;
import ren.nearby.lib.http.data.ResponseCallBack;
import ren.nearby.lib.http.data.TasksRepository;


/**
 * 检验数据
 * Created by Administrator on 2017/9/6 0006.
 */

public class LoginAction extends BasePresenter implements LoginContract.Action {
    /*    private final HttpApi _httpApi;*/
    private final LoginContract.View view;
    private TasksRepository mTasksRepository;
/*    @Inject
    Retrofit _retrofit;*/

    @Inject
    public LoginAction(/*HttpApi _httpApi, */TasksRepository tasksRepository, LoginContract.View view) {
//        this._httpApi = _httpApi;
        this.view = view;
        mTasksRepository = tasksRepository;
//        Logger.e(_httpApi == null && view == null && _retrofit == null ? "1" : "0");

    }

    @Override
    public void registered(Map<String, Object> map, ResponseCallBack callBack) {
        mTasksRepository.registered(map, callBack);
    }
    @Override
    public void login(Map<String, Object> map, ResponseCallBack callBack) {
        mTasksRepository.login(map, callBack);
    }
    @Override
    public void collection(Map<String, Object> map, ResponseCallBack callBack) {
        mTasksRepository.collection(map, callBack);
    }
    @Override
    public void testToken(Map<String, Object> map, ResponseCallBack callBack) {
        mTasksRepository.testToken(map, callBack);
    }

    @Override
    public void dowApk(ResponseCallBack callBack) {
        mTasksRepository.dowApk(callBack);
    }
}
