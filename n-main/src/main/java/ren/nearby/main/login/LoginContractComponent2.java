package ren.nearby.main.login;

import dagger.Component;

import ren.nearby.lib.http.scope.PerActivity;
import ren.nearby.main.MainComponent;

/**
 * Created by Administrator on 2017/9/6 0006.
 * 网络请求 Component  and UI module
 */

@PerActivity
//@Component(dependencies = TasksRepositoryComponent.class, modules = ActionModule.class)
@Component(dependencies = MainComponent.class, modules = ActionModule2.class)
public interface LoginContractComponent2 {
    LoginAction2 getAction();
}
