package ren.nearby.main.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Administrator on 2017/9/6 0006.
 */

@Module
public class ActionModule2 {
    private final LoginContract2.View view;

    public ActionModule2(LoginContract2.View view) {
        this.view = view;
    }

    @Provides
    public LoginContract2.View provideContractView() {
        return view;
    }
}
