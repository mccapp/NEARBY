package ren.nearby.main.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Administrator on 2017/9/6 0006.
 */

@Module
public class ActionModule {
    private final LoginContract.View view;

    public ActionModule(LoginContract.View view) {
        this.view = view;
    }

    @Provides
    public LoginContract.View provideContractView() {
        return view;
    }
}
