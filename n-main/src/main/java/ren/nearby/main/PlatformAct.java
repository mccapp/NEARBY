package ren.nearby.main;

import android.widget.TextView;

import com.meituan.android.walle.WalleChannelReader;

import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2018/4/25 0025.
 */

public class PlatformAct extends BaseActivity {

    TextView main_tv_platform;



    @Override
    public int getLayoutRes() {
        return R.layout.main_platform;
    }



    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_platform))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        main_tv_platform = findViewById(R.id.main_tv_platform);
        //配置apk环境
        String channel = WalleChannelReader.getChannel(getApplicationContext());
//        int envType = BuildConfig;
        String packageName = getActivity().getPackageName();
        main_tv_platform.setText("channel = " + channel + " - packageName = " + packageName);

        /*
        switch (envType) {
            case EnvType.DEVELOP:
                tvEnv.setText("envType=" + "开发环境");
                break;
            case EnvType.CHECK:
                tvEnv.setText("envType=" + "测试环境");
                break;
            case EnvType.PRODUCT:
                tvEnv.setText("envType=" + "生产环境");
                break;
        }
        tvChannel.setText("channel=" + channel);
        tvPackage.setText("package=" + packageName);*/

    }
}
