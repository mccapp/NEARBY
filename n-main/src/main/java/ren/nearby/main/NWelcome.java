package ren.nearby.main;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import java.util.HashMap;
import java.util.Map;

import ren.nearby.lib.http.meepo.Meepo;
import ren.nearby.lib.http.meepo.config.UriConfig;
import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2017/8/4 0004.
 */

public class NWelcome extends BaseActivity {
    private Button mBtnJump;
    private ImageView mPicAD;
    ActivityRouter router;

    @Override
    public int getLayoutRes() {
        return R.layout.main_welcome;
    }

    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.main_compress))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();

        final Meepo meepo = new Meepo.Builder()
                .config(new UriConfig().scheme("main").host("nearby.main"))
                .build();
        router = meepo.create(ActivityRouter.class);
        mBtnJump = (Button) findViewById(R.id.sp_jump_btn);
        mPicAD = (ImageView) findViewById(R.id.sp_bg);
        Logger.e("哈哈哈");
        startClock();

        mBtnJump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(NWelcome.this, "跳转到主界面", Toast.LENGTH_LONG).show();


                //单个参数传递与跳转
//                router.gotoA(NWelcome.this, "AActivity Title");
                //多个参数传递与跳转
                Map<String, String> map = new HashMap<String, String>();
                map.put("value1", "值1");
                map.put("value2", "值2");

//                router.gotoB(NWelcome.this, map);
                //多个参数传递与跳转Code与返回
                map.put("value3", "值3");
                router.gotoC(NWelcome.this, map, 120);
//                finish();

            }
        });

        mPicAD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(NWelcome.this, "点击广告图", Toast.LENGTH_LONG).show();
            }
        });

    }

    //    @DebugLog
   /* @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //测试Meepo请求code返回

        Logger.e(" Main 组件 " + "requestCode = " + requestCode + " - resultCode = " + resultCode);
    }


    private void startClock() {
        mBtnJump.setVisibility(View.VISIBLE);
        countDownTimer.start();
    }


    private CountDownTimer countDownTimer = new CountDownTimer(6000, 1000) {


        @Override
        public void onTick(long l) {
            mBtnJump.setText(l / 1000 + " s");

            Logger.e(" Main 组件 onTick");
        }

        @Override
        public void onFinish() {
            mBtnJump.setText(0 + " s");
            Logger.e(" Main 组件 onFinish");
//            router.gotoC(NWelcome.this);
            finish();
        }
    };
}
