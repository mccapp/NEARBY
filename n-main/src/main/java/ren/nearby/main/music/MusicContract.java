package ren.nearby.main.music;

import android.content.Context;

import ren.nearby.lib.http.data.ResponseCallBack;
import ren.nearby.lib.ui.base.IPresenter;


/**
 * Created by Administrator on 2017/9/6 0006.
 */

public interface MusicContract {
    interface View {

        Context getContext();
    }

    interface Action extends IPresenter {


          void musicInterface(ResponseCallBack callBack);


    }
}
