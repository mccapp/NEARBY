package ren.nearby.main.music;

import android.os.Bundle;
import android.support.annotation.Nullable;


import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.main.R;

/**
 * Created by Administrator on 2017/9/14 0014.
 */

public class NAdaptation extends BaseActivity {

    //屏幕适配尺寸命令行

    /**
     *  1.使用 wm size 命令查看当前设备的尺寸。
     *  2.使用 wm size 命令将当前尺寸修改成了 1000x2000。
     *  3.再次使用 wm size 命令查看当前设备的尺寸。
     *  4.最后使用 wm size reset 命令，将屏幕尺寸还原。
     *  5.再用 wm size 命令，查看还原后的尺寸。
     *
     *  1.修改成 一加5
     *  2.adb shell wm density 420
     *  3.adb shell wm size 1080x1920
     *  1.修改成 Moto-tx1095
     *  2.adb shell wm density 432
     *  3.adb shell wm size 1080x1790
     *
     *
     *
     * @return
     */
    @Override
    public int getLayoutRes() {
        return R.layout.main_adaptation;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
