package ren.nearby.main.music;

import ren.nearby.main.service.PlayService;

/**
 * Created by Administrator on 2017/9/20 0020.
 */

public class ServiceUtils {
    private PlayService mPlayService;
    private static class SingletonHolder {
        private static ServiceUtils sAppCache = new ServiceUtils();
    }

    private static ServiceUtils getInstance() {
        return SingletonHolder.sAppCache;
    }

    public static PlayService getPlayService() {
        return getInstance().mPlayService;
    }

    public static void setPlayService(PlayService service) {
        getInstance().mPlayService = service;
    }
}
