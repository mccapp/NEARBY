package ren.nearby.main.music;

import com.orhanobut.logger.Logger;

import java.util.HashMap;

import javax.inject.Inject;

import ren.nearby.lib.http.BasePresenter;
import ren.nearby.lib.http.HttpApi;
import ren.nearby.lib.http.data.ResponseCallBack;
import ren.nearby.lib.http.data.TasksRepository;
import retrofit2.Retrofit;


/**
 * 检验数据
 * Created by Administrator on 2017/9/6 0006.
 */

public class MusicAction extends BasePresenter implements MusicContract.Action {
        private final HttpApi _httpApi;
    private final MusicContract.View view;
    private TasksRepository mTasksRepository;
    @Inject
    Retrofit _retrofit;

    @Inject
    public MusicAction(HttpApi _httpApi, /*TasksRepository tasksRepository,*/MusicContract.View view) {
        this._httpApi = _httpApi;
        this.view = view;
//        mTasksRepository = tasksRepository;
        Logger.e(_httpApi == null && view == null && _retrofit == null ? "1" : "0");

    }

    @Override
    public void musicInterface(ResponseCallBack callBack) {
        Logger.e("musicInterface");
        HashMap<String, String> map = new HashMap<>();

        mTasksRepository.music(map, callBack);


    }
}
