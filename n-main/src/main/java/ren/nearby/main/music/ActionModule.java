package ren.nearby.main.music;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Administrator on 2017/9/6 0006.
 */

@Module
public class ActionModule {
    private final MusicContract.View view;

    public ActionModule(MusicContract.View view) {
        this.view = view;
    }

    @Provides
    public MusicContract.View provideContractView() {
        return view;
    }
}
