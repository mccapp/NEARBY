package ren.nearby.main.music;

import dagger.Component;
import ren.nearby.lib.http.scope.AppComponent;
import ren.nearby.lib.http.scope.ContractScoped;


/**
 * Created by Administrator on 2017/9/6 0006.
 */

@ContractScoped
@Component(dependencies = AppComponent.class, modules = ActionModule.class)
//@Component(dependencies = TasksRepositoryComponent.class, modules = ActionModule.class)
public interface MusicContractComponent {
    MusicAction getAction();
}
