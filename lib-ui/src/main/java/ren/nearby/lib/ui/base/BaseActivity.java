package ren.nearby.lib.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.Visibility;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjection;
import ren.nearby.lib.ui.R;
import ren.nearby.lib.ui.view.back.SwipeBackLayout;
import ren.nearby.lib.ui.view.empty.PageStateLayout;


/**
 * Created by Administrator on 2016/4/19.
 */
public class BaseActivity extends BaseActivityTitle implements ToolBarHelper.OnBackListener{
    public ToolBarHelper.Builder toolBarBuilder;
    /**
     * 控件注解绑定
     */
    private Unbinder unbinder;
    /**
     * 侧滑关闭
     */
    private SwipeBackLayout swipeBackLayout;
    /**
     * 透明侧滑背景层
     */
    private ImageView ivShadow;

    /**
     * 预览 空数据 异常
     */
    public PageStateLayout pageStateLayout;

    static final int TYPE_PROGRAMMATICALLY = 0;

    private int type = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        //阿里路由注册获取参数
        ARouter.getInstance().inject(this);
        super.onCreate(savedInstanceState);
        //activity传递数据Intent
        onIntent();
        pageStateLayout = new PageStateLayout(this);
        if (isStateLayout()) {
//            Log.e( " onCreate - if ");
            //操作侧滑关闭
            setContentView(getLayoutRes());
            //预览
//            pageStateLayout = new PageStateLayout(this);
//            View view = LayoutInflater.from(this).inflate(this.getContentView(), null);
//            pageStateLayout.setOnEmptyListener(mOnEmptyListener).setOnErrorListener(mOnErrorListener).load(this, view);
        } else if (getLayoutRes() != 0) {
//            Log.e(" onCreate - else ");
            setContentView(getLayoutRes());
        }

        //操作标题栏
        initViewHeader();
        initKnife();
        initView();

    }

    @Override
    public void initView() {
        Logger.e("initView");
    }

    /**
     * 获取传递数据
     */
    public void onIntent() {
        Logger.e("onIntent");
    }

    protected void initViewHeader() {
        if (toolBarBuilder == null) {
            toolBarBuilder = new ToolBarHelper.Builder(this);
            toolBarBuilder.setOnBackListener(this);
        }
    }
    @Override
    public void onBackClick() {
        finish();
    }

    /**
     * 处理侧滑层
     *
     * @return
     */
    private View getContainer() {
        RelativeLayout container = new RelativeLayout(this);
        swipeBackLayout = new SwipeBackLayout(this);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
        ivShadow = new ImageView(this);
        ivShadow.setBackgroundColor(getResources().getColor(R.color.theme_black_7f));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        container.addView(ivShadow, params);
        container.addView(swipeBackLayout);
        swipeBackLayout.setOnSwipeBackListener(new SwipeBackLayout.SwipeBackListener() {
            @Override
            public void onViewPositionChanged(float fa, float fs) {
                ivShadow.setAlpha(1 - fs);
            }
        });
        return container;
    }
    @Override
    public Resources getResources() {
        Resources resources = super.getResources();
        Configuration newConfig = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        if (resources != null && newConfig.fontScale != 1) {
            newConfig.fontScale = 1;
            if (Build.VERSION.SDK_INT >= 17) {
                Context configurationContext = createConfigurationContext(newConfig);
                resources = configurationContext.getResources();
                displayMetrics.scaledDensity = displayMetrics.density * newConfig.fontScale;
            } else {
                resources.updateConfiguration(newConfig, displayMetrics);
            }
        }
        return resources;
    }
    /**
     * 处理那些activity可以侧滑
     *
     * @param layoutResID
     */
    @Override
    public void setContentView(int layoutResID) {
        if (isStateLayout()) {
            Logger.e(" setContentView - if ");
            super.setContentView(getContainer());
            View view = LayoutInflater.from(this).inflate(layoutResID, null);
            view.setBackgroundColor(getResources().getColor(R.color.window_background));
            swipeBackLayout.addView(view);
        } else {
            Logger.e(" setContentView - else ");
            super.setContentView(layoutResID);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.e("onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.e("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.e("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.e("onStop");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.e("onDestroy");
        unbinder.unbind();

    }

    @Override
    public IPresenter getPresenter() {
        return null;
    }

    @Override
    public int getLayoutRes() {
        Logger.e("father getLayoutRes");
        return 0;
    }


    @Override
    public void initKnife() {
        Logger.e("initKnife");
        unbinder = ButterKnife.bind(this);//初始化在oncreate()方法里面
    }


    @Override
    public BaseActivity getActivity() {
        return this;
    }

    /**
     * 动画
     *
     * @param i
     */
    @SuppressWarnings("unchecked")
    public void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }




    /**
     * 进入界面动画效果
     */
    public void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initWindowAnimations();
        }

    }

    @TargetApi(21)
    public void initWindowAnimations() {
        Transition transition;
        if (type == TYPE_PROGRAMMATICALLY) {
            transition = buildEnterTransition();
        } else {
            transition = TransitionInflater.from(this).inflateTransition(R.transition.slide_from_bottom);
        }
        getWindow().setEnterTransition(transition);
    }

    /**
     * 代码动画方式
     *
     * @return
     */
    @TargetApi(21)
    public Visibility buildEnterTransition() {
        Slide enterTransition = new Slide();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        enterTransition.setSlideEdge(Gravity.RIGHT);
        return enterTransition;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        Logger.e("finish 退出");
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        Logger.e("start 进入");
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_form_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_form_left, R.anim.slide_to_right);
    }

}
