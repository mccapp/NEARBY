package ren.nearby.lib.ui.permission;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


import java.util.ArrayList;
import java.util.List;

import ren.nearby.lib.ui.*;

/**
 * 权限弹出框
 * Created by Administrator on 2018/5/31 0031.
 */

public class PermissionDialogFragment extends DialogFragment {
    List<PermissionBean> list;

    public static PermissionDialogFragment newInstance(ArrayList<PermissionBean> beans) {
        PermissionDialogFragment fragment = new PermissionDialogFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("bean", beans);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * setStyle() 的第一个参数有四个可选值：
         * STYLE_NORMAL|STYLE_NO_TITLE|STYLE_NO_FRAME|STYLE_NO_INPUT
         * 其中 STYLE_NO_TITLE 和 STYLE_NO_FRAME 可以关闭标题栏
         * 每一个参数的详细用途可以直接看 Android 源码的说明
         */
        setStyle(PermissionDialogFragment.STYLE_NO_TITLE, R.style.into_DialogStyle);
        list = getArguments().getParcelableArrayList("bean");
        setCancelable(false);

    }

    OnCancelClick onCancelClick;

    public interface OnCancelClick {

        void ok();

        void cancel();


    }

    public void setOnCancelClick(OnCancelClick onCancelClick) {
        this.onCancelClick = onCancelClick;
    }

    PermissionAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.permission_dialog, container, false);
        RecyclerView permission_dialog_rv = view.findViewById(R.id.permission_dialog_rv);

        adapter = new PermissionAdapter(list, R.layout.permission_dialog_item);

        permission_dialog_rv.setHasFixedSize(true);//固定大小
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        permission_dialog_rv.setLayoutManager(linearLayoutManager);
        permission_dialog_rv.setAdapter(adapter);
        ImageView iv_cancal = view.findViewById(R.id.iv_cancal);
        iv_cancal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick.cancel();
            }
        });
        Button permission_btn = view.findViewById(R.id.permission_btn);
        permission_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick.ok();
            }
        });

        return view;
    }
}
