package ren.nearby.lib.ui.base.adapter.ext;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ren.nearby.lib.ui.base.adapter.base.BaseRecyclerViewAdapter;


/**
 * Created by Administrator on 2016/4/19.
 */
public class PublicAdapter extends BaseRecyclerViewAdapter<String, PublicAdapter.ViewHolder> {

    public PublicAdapter(List<String> list) {
        super(list);
    }


    @Override
    protected void bindDataToItemView(ViewHolder viewHolder, String item) {


    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {

        return new ViewHolder(inflateItemView(viewGroup, 0));

    }

    public class ViewHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
