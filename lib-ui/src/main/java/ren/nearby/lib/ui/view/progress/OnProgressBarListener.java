package ren.nearby.lib.ui.view.progress;

/**
 * Created by lelexxx on 15-4-23.
 */
public interface OnProgressBarListener {

    void onProgressChange(int current, int max);
}
