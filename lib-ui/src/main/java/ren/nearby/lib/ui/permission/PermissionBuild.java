package ren.nearby.lib.ui.permission;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;

import ren.nearby.lib.ui.base.BaseActivity;

/**
 * Created by Administrator on 2018/11/6 0006.
 */

public class PermissionBuild {


    public PermissionBuild() {
    }

    public interface PermiCancel {
        void perCancel();
    }

    public interface PermiOk {
        void perOk();
    }

    public static class Builder {
        @TargetApi(Build.VERSION_CODES.M)
        boolean okRevoked(String permission) {
            return activity.getPackageManager().isPermissionRevokedByPolicy(permission, activity.getPackageName());
        }

        @TargetApi(Build.VERSION_CODES.M)
        boolean okGranted(String permission) {
            return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        boolean isMarshmallow() {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
        }

        public boolean isGranted(String permission) {
            return !isMarshmallow() || okGranted(permission);
        }

        public boolean isRevoked(String permission) {
            return isMarshmallow() && okRevoked(permission);
        }

        private PermissionDialogFragment dialog;
        private ArrayList<PermissionBean> request = new ArrayList<>();
        public PermiCancel onCancelListener;
        public PermiOk onOkListener;
        public BaseActivity activity;

        public Builder(BaseActivity activity) {
            this.activity = activity;

        }


        public Builder setRequest(ArrayList<PermissionBean> request) {
            for (PermissionBean permission : request) {
                if (!isGranted(permission.getName()) && !isRevoked(permission.getName())) {
                    this.request.add(permission);
                }
            }
            return this;
        }


        public Builder setPermiCancel(PermiCancel onCancelListener) {
            this.onCancelListener = onCancelListener;
            return this;
        }

        public Builder setPermiOk(PermiOk onOkListener) {
            this.onOkListener = onOkListener;
            return this;
        }

        public PermissionBuild build() {
            if (dialog == null) {
                dialog = PermissionDialogFragment.newInstance(request);
            }
            if (dialog != null) {
                if (request.size() > 0) {
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    dialog.setOnCancelClick(new PermissionDialogFragment.OnCancelClick() {
                        @Override
                        public void ok() {
                            if(dialog!=null){
                                dialog.dismiss();
                            }
                            onOkListener.perOk();
                        }

                        @Override
                        public void cancel() {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            onCancelListener.perCancel();
                        }
                    });
                    dialog.show(transaction, "permissions");
                }else {
                    onOkListener.perOk();
                }
            }
            return new PermissionBuild();
        }
    }
}
