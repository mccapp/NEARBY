package ren.nearby.lib.ui.base;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.R;


/**
 * 需要toolbar标题栏 和 管理rx生命周期的 就由子类重写方法
 */
public abstract class BaseActivityTitle<T extends IPresenter> extends AppCompatActivity {
    /**
     * 是否左边图标按钮
     *
     * @return
     */
    public boolean isShowLeftIcon() {
        return false;
    }

    /**
     * 是否右边图标按钮
     *
     * @return
     */
    public boolean isShowRightIcon() {
        return false;
    }

    /**
     * 更新右边图标按钮
     *
     * @return
     */
    public int setLeftIcon() {
        return R.mipmap.btn_back_white;
    }

    /**
     * 更新右边图标按钮
     *
     * @return
     */
    public int setRightIcon() {
        return R.mipmap.btn_back_white;
    }

    /**
     * 显示标题栏
     *
     * @return
     */
    public String showTitle() {
        return getResources().getString(R.string.app_name);
    }

    /**
     * 左边点击事件
     *
     * @param v
     */
    public void onClickLeft(View v) {
        finish();
    }

    /**
     * 右边点击事件
     *
     * @param v
     */
    public void onClickRight(View v) {
    }

    /**
     * 设置标题栏默认颜色
     *
     * @return
     */
    public int getBGC() {
        return R.color.red;
    }

    /**
     * 重置标题栏颜色
     * @return
     */
    public int setBGC() {
        return 0;
    }

    /**
     * 是否需要加载数据失败预览
     *
     * @return
     */
    public boolean isStateLayout() {
        return false;
    }

    /**
     * 是否采用字体库
     *
     * @return
     */
    public boolean isTTF() {
        return false;
    }


    @Override
    protected void onStart() {
        super.onStart();
        //获取子类实现的操作层 管理生命周期
        mPresenter = getPresenter();
        Logger.e(mPresenter == null ? "mPresenter onStart - 1 " : " mPresenter onStart - 0 ");
    }

    /**
     * 管理操作数据层的生命周期
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.e(mPresenter == null ? "mPresenter onDestroy - 1 " : "mPresenter onDestroy - 0 ");
        if (mPresenter != null){ mPresenter.detachView();}

    }

    /**
     * 操作数据层
     */
    protected T mPresenter;

    /**
     * 抽象由子类实现
     *
     * @return
     */
    public abstract T getPresenter();

    /**
     * 加载布局文件
     *
     * @return
     */
    public abstract int getLayoutRes();


    /**
     * 初始化控件布局绑定
     */
    public abstract void initKnife();

    /**
     * 初始化控件
     */
    public abstract void initView();

    /**
     * 当前activity
     *
     * @return
     */
    public abstract BaseActivity getActivity();
}
