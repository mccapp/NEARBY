    https://github.com/iPaulPro/Android-ItemTouchHelper-Demo
    当前界面类实现 implements OnStartDragListener接口 重写 onStartDrag 方法
    @Override
       public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
           mItemTouchHelper.startDrag(viewHolder);
    }
    
    Adapter 实现 implements ItemTouchHelperAdapter 接口 重写 onItemDismiss onItemMove 方法
    构造函数传递 OnStartDragListener dragStartListener
    @Override
       public void onItemDismiss(int position) {
           mItems.remove(position);
           notifyItemRemoved(position);
    }
    @Override
       public boolean onItemMove(int fromPosition, int toPosition) {
           Collections.swap(mItems, fromPosition, toPosition);
           notifyItemMoved(fromPosition, toPosition);
           return true;
    }
    选中背景
     Adapter 实现 ItemTouchHelperViewHolder  接口 重写 onItemSelected onItemClear 方法
      @Override
          public void onItemSelected() {
                itemView.setBackgroundColor(Color.LTGRAY);
      }
     
      @Override
          public void onItemClear() {
                itemView.setBackgroundColor(0);
      }
    拽拉控件使用方式
    ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
    mItemTouchHelper = new ItemTouchHelper(callback);
    mItemTouchHelper.attachToRecyclerView(recyclerView);