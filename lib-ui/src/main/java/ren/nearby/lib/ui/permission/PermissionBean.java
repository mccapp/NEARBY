package ren.nearby.lib.ui.permission;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/11/3 0003.
 */

public class PermissionBean implements Parcelable {


    private int icon;
    private String name;
    private String title;
    private String content;


    public PermissionBean() {

    }

    public PermissionBean(String name, int icon, String title, String content) {

        this.name = name;
        this.icon = icon;
        this.title = title;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(icon);
        dest.writeString(title);
        dest.writeString(content);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public final Creator<PermissionBean> CREATOR = new Creator<PermissionBean>() {

        @Override
        public PermissionBean createFromParcel(Parcel source) {
            PermissionBean bean = new PermissionBean();
            bean.name = source.readString();
            bean.icon = source.readInt();
            bean.title = source.readString();
            bean.content = source.readString();


            return bean;
        }

        @Override
        public PermissionBean[] newArray(int size) {
            return new PermissionBean[0];
        }
    };

}
