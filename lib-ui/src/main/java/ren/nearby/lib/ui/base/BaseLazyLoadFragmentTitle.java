package ren.nearby.lib.ui.base;


import android.app.Fragment;
import android.content.Context;

import dagger.android.AndroidInjection;

/**
 * Created by Administrator on 2018/5/14 0014.
 */

public abstract class BaseLazyLoadFragmentTitle<T extends IPresenter> extends Fragment {

    @Override
    public void onAttach(Context context) {
        AndroidInjection.inject(this);
        super.onAttach(context);
    }


    /**
     * 操作层数据
     */
    protected T mPresenter;


    /**
     * 泛型注入操作层
     *
     * @param mPresenter
     */
    public void setPresenter(T mPresenter) {
        this.mPresenter = mPresenter;
    }


}
