package ren.nearby.lib.ui.base.adapter.listener;

import android.view.View;

/**
 * Created by lizhangqu on 2015/6/home_3.
 */
public interface OnItemClickListener<T> {
    void onClick(View view, T item);
}
