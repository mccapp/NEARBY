package ren.nearby.lib.ui.base;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.R;
import ren.nearby.lib.ui.statusbar.StatusBarCompat;
import ren.nearby.lib.ui.view.empty.PageStateLayout;


/**
 * Created by Administrator on 2018/5/29 0029.
 */

public class ToolBarHelper {


    /**
     * @param activity        Activity界面
     * @param onBackListener  是否开启左边点击事件
     * @param onNextListener  是否开启右边点击事件
     * @param view            Fragment
     * @param title           标题内容
     * @param titleColor      标题颜色
     * @param backgroundColor 标题栏背景色
     * @param titlePosition   标题显示的位置
     * @param iconLeft        是否更新左边按钮图标
     * @param isBack          是否开启左边点击事件
     * @param iconRight       是否更新右边按钮图标
     * @param fontTypeStr     标题字体类型
     * @param rightText       是否设置右边字体
     * @param rightTextColor  是否设置右边字体颜色
     */
    ToolBarHelper(
            BaseActivity activity,
            final OnBackListener onBackListener,
            final OnNextListener onNextListener,
            final OnEmptyListener onEmptyListener,
            final OnErrorListener onErrorListener,
            View view,
            String title,
            int titleColor,
            int backgroundColor,
            int titlePosition,
            int iconLeft,
            boolean isBack,
            int iconRight,
            String fontTypeStr,
            String rightText,
            int rightTextColor,
            int layout,
            PageStateLayout pageStateLayout,
            boolean isApplyStatusBarTranslucency,
            boolean isApplyStatusBarColor


    ) {
        if (activity == null) {
            return;
        }
        backgroundColor = backgroundColor == 0 ? R.color.colorPrimary : backgroundColor;
        //沉浸式状态栏
        StatusBarCompat statusBarCompat = new StatusBarCompat(activity);
        if (isApplyStatusBarTranslucency) {
            statusBarCompat.setImmersionBar();

        }
        if (!isApplyStatusBarTranslucency && isApplyStatusBarColor) {
            statusBarCompat.setColorBar(activity.getResources().getColor(backgroundColor));
        }
        if (view != null) {
            Toolbar id_toolbar = view.findViewById(R.id.toolbar_id);
            if (id_toolbar == null) {
                return;
            }
            id_toolbar.setTitle("");
            activity.setSupportActionBar(id_toolbar);
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            id_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackListener.onBackClick();
                }
            });

            //标题栏信息
            TextView index_tv_title = view.findViewById(R.id.toolbar_tv_title);
            index_tv_title.setTextColor(titleColor == 0 ? ContextCompat.getColor(activity, R.color.red) : ContextCompat.getColor(activity, titleColor));

            RelativeLayout relative = view.findViewById(R.id.relative);
            LinearLayout bgc = (LinearLayout) view.findViewById(R.id.bgc);
            Toolbar.LayoutParams lp = new Toolbar
                    .LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
            //标题栏位置
            if (titlePosition == 0) {
                relative.setGravity(Gravity.LEFT);
                relative.setLayoutParams(lp);
            }
            //标题内容
            index_tv_title.setText(title == null ? "" : title);
            view.findViewById(R.id.view_line).setVisibility(title == null || title.equals("") ? View.GONE : View.VISIBLE);
            //标题栏字体类型
            if (fontTypeStr != null) {//字体类型
                TextView toolbar_tv_title_bttom = view.findViewById(R.id.toolbar_tv_title_bttom);
                toolbar_tv_title_bttom.setVisibility(View.VISIBLE);
                Typeface face2 = Typeface.createFromAsset(activity.getAssets(), /*fontTypeStr*/"fonts/hwcy.ttf");
                toolbar_tv_title_bttom.setTypeface(face2);
                Typeface face = Typeface.createFromAsset(activity.getAssets(), /*fontTypeStr*/"fonts/hwcy.ttf");
                index_tv_title.setTypeface(face);
            }


            //左边图标
            if (isBack) {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                id_toolbar.setNavigationIcon(iconLeft == 0 ? R.mipmap.btn_back_white : iconLeft);
            } else {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }

            //右边图标
            if (iconRight != 0 && rightText == null) {
                ImageView index_iv_right = view.findViewById(R.id.toolbar_iv_right);
                index_iv_right.setVisibility(View.VISIBLE);
                index_iv_right.setBackgroundResource(iconRight);
                index_iv_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNextListener.onNextClick();

                    }
                });
            }
            //右边字体
            if (iconRight == 0 && rightText != null) {
                TextView toolbar_tv_right_title = (TextView) view.findViewById(R.id.toolbar_tv_right_title);
                toolbar_tv_right_title.setText(rightText);
                toolbar_tv_right_title.setVisibility(View.VISIBLE);
                toolbar_tv_right_title.setTextColor(ContextCompat.getColor(activity, rightTextColor));
                toolbar_tv_right_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNextListener.onNextClick();

                    }
                });
            }
            //背景色
            if (backgroundColor == 0) {
                bgc.setBackgroundResource(R.drawable.shape_status_color2);
            } else {
                bgc.setBackgroundResource(backgroundColor);
            }
        } else {
            if (layout != 0) {
                Logger.e("laile");
                View stateview = LayoutInflater.from(activity).inflate(layout, null);
                pageStateLayout.setOnEmptyListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onEmptyListener.onEmptyClick();
                    }
                }).setOnErrorListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onErrorListener.onOnErrorClick();
                    }
                }).load(activity, stateview);
            }


            Toolbar id_toolbar = (Toolbar) activity.findViewById(R.id.toolbar_id);
            if (id_toolbar == null) {
                return;
            }
            id_toolbar.setTitle("");
            activity.setSupportActionBar(id_toolbar);
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            id_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackListener.onBackClick();
                }
            });

            //标题栏信息
            TextView index_tv_title = (TextView) activity.findViewById(R.id.toolbar_tv_title);
            index_tv_title.setTextColor(titleColor == 0 ? ContextCompat.getColor(activity, R.color.red) : ContextCompat.getColor(activity, titleColor));
            RelativeLayout relative = (RelativeLayout) activity.findViewById(R.id.relative);
            LinearLayout bgc = (LinearLayout) activity.findViewById(R.id.bgc);
            Toolbar.LayoutParams lp = new Toolbar
                    .LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
            //标题栏位置
            if (titlePosition == 0) {
                relative.setGravity(Gravity.LEFT);
                relative.setLayoutParams(lp);
            }
            //标题内容
            index_tv_title.setText(title == null ? "" : title);
            activity.findViewById(R.id.view_line).setVisibility(title == null || title.equals("") ? View.GONE : View.VISIBLE)
            ;
            //标题栏字体类型
            if (fontTypeStr != null) {//字体类型
                TextView toolbar_tv_title_bttom = (TextView) activity.findViewById(R.id.toolbar_tv_title_bttom);
                toolbar_tv_title_bttom.setVisibility(View.VISIBLE);
                Typeface face2 = Typeface.createFromAsset(activity.getAssets(), /*fontTypeStr*/"fonts/hwcy.ttf");
                toolbar_tv_title_bttom.setTypeface(face2);
                Typeface face = Typeface.createFromAsset(activity.getAssets(), /*fontTypeStr*/"fonts/hwcy.ttf");
                index_tv_title.setTypeface(face);
            }


            //左边图标
            if (isBack) {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                id_toolbar.setNavigationIcon(iconLeft == 0 ? R.mipmap.btn_back_white : iconLeft);
            } else {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }

            //右边图标
            if (iconRight != 0 && rightText == null) {
                ImageView index_iv_right = (ImageView) activity.findViewById(R.id.toolbar_iv_right);
                index_iv_right.setVisibility(View.VISIBLE);
                index_iv_right.setBackgroundResource(iconRight);
                index_iv_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNextListener.onNextClick();

                    }
                });
            }
            //右边字体
            if (iconRight == 0 && rightText != null) {
                TextView toolbar_tv_right_title = (TextView) activity.findViewById(R.id.toolbar_tv_right_title);
                toolbar_tv_right_title.setText(rightText);
                toolbar_tv_right_title.setVisibility(View.VISIBLE);
                toolbar_tv_right_title.setTextColor(ContextCompat.getColor(activity, rightTextColor));
                toolbar_tv_right_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNextListener.onNextClick();

                    }
                });
            }
            //背景色
            if (backgroundColor == 0) {
                bgc.setBackgroundResource(R.drawable.shape_status_color2);
            } else {
                bgc.setBackgroundResource(backgroundColor);
            }
        }


    }

    public interface OnEmptyListener {
        void onEmptyClick();

    }

    public interface OnErrorListener {
        void onOnErrorClick();
    }

    public interface OnBackListener {
        void onBackClick();
    }

    public interface OnNextListener {
        void onNextClick();
    }

    public static class Builder {
        private BaseActivity activity;
        public OnBackListener onBackListener;
        public OnNextListener onNextListener;
        public OnEmptyListener onEmptyListener;
        public OnErrorListener onErrorListener;
        private View view;
        private String title;
        private int titleColor;
        private int backgroundColor;
        private int titlePosition = 1;
        private String fontTypeStr;
        private int iconLeft;
        private boolean isBack;
        private int iconRight;
        private String rightText;
        private int rightTextColor;
        private int statusBarTintResource;
        private int layout;
        private PageStateLayout pageStateLayout;
        private boolean isApplyStatusBarTranslucency;
        private boolean isApplyStatusBarColor;


        //初始化基本数据
        public Builder(BaseActivity activity) {
            this.activity = activity;

        }

        public Builder onLoading() {
            Logger.e(pageStateLayout == null ? "pageStateLayout - 1 " : "pageStateLayout - 2");
            if (pageStateLayout != null) {
                pageStateLayout.onLoading();
            }
            return this;
        }

        public Builder onRequesting() {
            if (pageStateLayout != null) {
                pageStateLayout.onRequesting();
            }
            return this;
        }

        public Builder onEmpty() {
            if (pageStateLayout != null) {
                pageStateLayout.onEmpty();
            }
            return this;
        }

        public Builder onError() {
            if (pageStateLayout != null) {
                pageStateLayout.onError();
            }
            return this;
        }

        public Builder onSucceed() {
            if (pageStateLayout != null) {
                pageStateLayout.onSucceed();
            }
            return this;
        }

        public Builder setPageStateLayout(int layout, PageStateLayout pageStateLayout) {
            this.layout = layout;
            this.pageStateLayout = pageStateLayout;
            return this;
        }

        public Builder setOnBackListener(OnBackListener onBackListener) {
            this.onBackListener = onBackListener;
            return this;
        }

        public Builder setOnNextListener(OnNextListener onNextListener) {
            this.onNextListener = onNextListener;
            return this;
        }

        public Builder setonEmptyListener(OnEmptyListener onEmptyListener) {
            this.onEmptyListener = onEmptyListener;
            return this;
        }

        public Builder setonErrorListener(OnErrorListener onErrorListener) {
            this.onErrorListener = onErrorListener;
            return this;
        }

        public Builder setApplyStatusBarTranslucency(boolean isApplyStatusBarTranslucency) {
            this.isApplyStatusBarTranslucency = isApplyStatusBarTranslucency;
            return this;
        }

        public Builder setApplyStatusBarColor(boolean isApplyStatusBarColor) {
            this.isApplyStatusBarColor = isApplyStatusBarColor;
            return this;
        }


        public Builder setView(View view) {
            this.view = view;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }


        public Builder setTitleColor(int titleColor) {
            this.titleColor = titleColor;
            return this;
        }


        public Builder setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }


        public Builder setTitlePosition(int titlePosition) {
            this.titlePosition = titlePosition;
            return this;
        }


        public Builder setFontTypeStr(String fontTypeStr) {
            this.fontTypeStr = fontTypeStr;
            return this;
        }

        public Builder setIconLeft(int iconLeft) {
            this.iconLeft = iconLeft;
            return this;
        }

        public Builder setBack(boolean back) {
            isBack = back;
            return this;
        }

        public Builder setIconRight(int iconRight) {
            this.iconRight = iconRight;
            return this;
        }


        public Builder setIsRightText(String rightText) {
            this.rightText = rightText;
            return this;
        }

        public Builder setIsRightTextColor(int rightTextColor) {
            this.rightTextColor = rightTextColor;
            return this;
        }

        public Builder setStatusBarTintResource(int statusBarTintResource) {
            this.statusBarTintResource = statusBarTintResource;
            return this;
        }

        /**
         * 构建工具
         *
         * @return
         */
        public ToolBarHelper build() {
            return new ToolBarHelper(
                    activity,
                    onBackListener,
                    onNextListener,
                    onEmptyListener,
                    onErrorListener,
                    view,
                    title,
                    titleColor,
                    backgroundColor,
                    titlePosition,
                    iconLeft,
                    isBack,
                    iconRight,
                    fontTypeStr,
                    rightText,
                    rightTextColor,
                    layout,
                    pageStateLayout,
                    isApplyStatusBarTranslucency,
                    isApplyStatusBarColor

            );
        }
    }
}

