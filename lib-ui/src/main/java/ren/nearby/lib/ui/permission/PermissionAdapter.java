package ren.nearby.lib.ui.permission;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ren.nearby.lib.ui.R;
import ren.nearby.lib.ui.base.adapter.base.BaseRecyclerViewAdapter;


/**
 * Created by Administrator on 2016/4/19.
 */
public class PermissionAdapter extends BaseRecyclerViewAdapter<PermissionBean, PermissionAdapter.IndexViewHolder> {

    int mXml;
    public PermissionAdapter(List<PermissionBean> list, int mXml) {
        super(list);
        this.mXml = mXml;
    }


    @Override
    protected void bindDataToItemView(IndexViewHolder myViewHolder, PermissionBean item) {
        myViewHolder.setBackgroundResource(R.id.permission_dialog_iv_icon, item.getIcon());
        myViewHolder.setText(R.id.permission_dialog_tv_title, item.getTitle());
        myViewHolder.setText(R.id.permission_dialog_tv_content, item.getContent());

    }


    @Override
    public IndexViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {

        return new IndexViewHolder(inflateItemView(viewGroup, mXml));

    }

    public class IndexViewHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {
        public IndexViewHolder(View itemView) {
            super(itemView);
        }
    }


}
