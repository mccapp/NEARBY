package ren.nearby.lib.ui.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import ren.nearby.lib.ui.R;


/**
 * Created by Administrator on 2018/1/27 0027.
 */

public class PublicDialog extends DialogFragment {
    private Builder builder;
    public static final String TAG = PublicDialog.class.getSimpleName();
    private static PublicDialog instance = new PublicDialog();
    private CardView cardView;

    private TextView title, body;
    private Button positive, negative;

    public static PublicDialog getInstance() {
        return instance;
    }

    public interface OnPositiveClicked {
        void OnClick(View view, Dialog dialog);
    }

    public interface OnNegativeClicked {
        void OnClick(View view, Dialog dialog);
    }

    public static class Builder implements Parcelable {
        private Context context;
        private String positiveButtonText;
        private String negativeButtonText;

        private int positiveTextColor;
        private int negativeTextColor;

        private String textTitle;
        private int textTitleColor;

        private String body;
        private int bodyColor;

        private boolean cancelable;


        private OnPositiveClicked onPositiveClicked;
        private OnNegativeClicked onNegativeClicked;


        public Builder(Context context) {
            this.context = context;
        }

        protected Builder(Parcel in) {
            positiveButtonText = in.readString();
            negativeButtonText = in.readString();
            positiveTextColor = in.readInt();
            negativeTextColor = in.readInt();
            positiveTextColor = in.readInt();
            body = in.readString();
            bodyColor = in.readInt();
            textTitle = in.readString();
            textTitleColor = in.readInt();
            cancelable = in.readByte() != 0;
        }

        public static final Creator<Builder> CREATOR = new Creator<Builder>() {
            @Override
            public Builder createFromParcel(Parcel in) {
                return new Builder(in);
            }

            @Override
            public Builder[] newArray(int size) {
                return new Builder[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(positiveButtonText);
            dest.writeString(negativeButtonText);
            dest.writeInt(positiveTextColor);
            dest.writeInt(negativeTextColor);
            dest.writeString(body);
            dest.writeInt(bodyColor);
            dest.writeString(textTitle);
            dest.writeInt(textTitleColor);

            dest.writeByte((byte) (cancelable ? 1 : 0));
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public boolean isCancelable() {
            return cancelable;
        }

        public Builder setPositiveButtonText(String positiveButtonText) {
            this.positiveButtonText = positiveButtonText;
            return this;
        }

        public String getPositiveButtonText() {
            return positiveButtonText;
        }

        public Builder setPositiveTextColor(int positiveTextColor) {
            this.positiveTextColor = positiveTextColor;
            return this;
        }

        public int getPositiveTextColor() {
            return positiveTextColor;
        }

        public Builder setNegativeTextColor(int negativeTextColor) {
            this.negativeTextColor = negativeTextColor;
            return this;
        }

        public int getNegativeTextColor() {
            return negativeTextColor;
        }

        public Builder setNegativeButtonText(String negativeButtonText) {
            this.negativeButtonText = negativeButtonText;
            return this;
        }

        public String getNegativeButtonText() {
            return negativeButtonText;
        }

        public Builder setTextTitle(String textTitle) {
            this.textTitle = textTitle;
            return this;
        }

        public String getTextTitle() {
            return textTitle;
        }

        public Builder setTextTitleColor(int textTitleColor) {
            this.textTitleColor = textTitleColor;
            return this;
        }

        public int getTextTitleColor() {
            return textTitleColor;
        }


        public Builder setbody(String body) {
            this.body = body;
            return this;
        }

        public String getBody() {
            return body;
        }

        public Builder setbodyColor(int bodyColor) {
            this.bodyColor = bodyColor;
            return this;
        }

        public int getBodyColor() {
            return bodyColor;
        }


        public Builder setOnPositiveClicked(OnPositiveClicked onPositiveClicked) {
            this.onPositiveClicked = onPositiveClicked;
            return this;
        }

        public OnPositiveClicked getOnPositiveClicked() {
            return onPositiveClicked;
        }

        public Builder setOnNegativeClicked(OnNegativeClicked onNegativeClicked) {
            this.onNegativeClicked = onNegativeClicked;
            return this;
        }

        public OnNegativeClicked getOnNegativeClicked() {
            return onNegativeClicked;
        }

        public Builder build() {
            return this;
        }

        public Dialog show() {
            return PublicDialog.getInstance().show(((Activity) context), this);
        }
    }

    public Dialog show(Activity activity, Builder builder) {
        this.builder = builder;
        if (!isAdded())
            show(((AppCompatActivity) activity).getSupportFragmentManager(), TAG);
        return getDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.setCancelable(true);//设置可取消
        if (savedInstanceState != null) {
            if (builder != null) {
                builder = (Builder) savedInstanceState.getParcelable(Builder.class.getSimpleName());
            }
        }
        setRetainInstance(true);//保存实例
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (builder != null)
            outState.putParcelable(Builder.class.getSimpleName(), builder);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (builder != null) {
            dialog.setCancelable(builder.isCancelable());
            instance.setCancelable(builder.isCancelable());
        }
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_permissions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        if (builder != null) {
            if (builder.getTextTitle() != null) {
                title.setText(builder.getTextTitle());
                if (builder.getTextTitleColor() != 0) {
                    positive.setTextColor(ContextCompat.getColor(getActivity(), builder.getTextTitleColor()));
                }
            } else {
                title.setVisibility(View.GONE);
            }

            if (builder.getBody() != null) {
                body.setText(builder.getBody());
                if (builder.getBodyColor() != 0) {
                    positive.setTextColor(ContextCompat.getColor(getActivity(), builder.getBodyColor()));
                }
            } else {
                body.setVisibility(View.GONE);
            }
            body.setText(builder.getBody());

            if (builder.getPositiveButtonText() != null) {
                positive.setText(builder.getPositiveButtonText());
                if (builder.getPositiveTextColor() != 0) {
                    positive.setTextColor(ContextCompat.getColor(getActivity(), builder.getPositiveTextColor()));
                }
                if (builder.getOnPositiveClicked() != null) {
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            builder.getOnPositiveClicked().OnClick(v, getDialog());
                            instance.dismiss();
                        }
                    });
                }
            } else {
                positive.setVisibility(View.GONE);
            }
        }

        if (builder.getNegativeButtonText() != null) {
            negative.setText(builder.getNegativeButtonText());
            if (builder.getNegativeTextColor() != 0) {
                negative.setTextColor(ContextCompat.getColor(getActivity(), builder.getNegativeTextColor()));
            }
            if (builder.getOnNegativeClicked() != null) {
                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.getOnNegativeClicked().OnClick(v, getDialog());
                        instance.dismiss();
                    }
                });
            }
        } else {
            negative.setVisibility(View.GONE);
        }
    }

    private void initViews(View view) {
        cardView = (CardView) view.findViewById(R.id.card_view);
        title = (TextView) view.findViewById(R.id.title);
        body = (TextView) view.findViewById(R.id.body);
        positive = (Button) view.findViewById(R.id.position);
        negative = (Button) view.findViewById(R.id.negative);
    }
}
