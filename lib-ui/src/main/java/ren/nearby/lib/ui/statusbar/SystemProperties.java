package ren.nearby.lib.ui.statusbar;

import android.text.TextUtils;

import java.lang.reflect.Method;

/**
 * Created by wuchundu on 2018/9/17.
 */
public class SystemProperties {
    private static Method getStringProperty = getMethod(getClass("android.os.SystemProperties"));

    private static Class<?> getClass(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            try {
                return ClassLoader.getSystemClassLoader().loadClass(name);
            } catch (ClassNotFoundException e1) {
                return null;
            }
        }
    }

    private static Method getMethod(Class<?> clz) {
        try {
            return clz.getMethod("get", String.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static String get(String key) {
        try {
            String value = (String) getStringProperty.invoke(null, key);
            if (!TextUtils.isEmpty(value)) {
                return value.trim();
            } else {
                return "";
            }
        } catch (Exception ignored) {
            return "";
        }
    }

    public static String get(String key, String def) {
        try {
            String value = (String) getStringProperty.invoke(null, key);
            if (!TextUtils.isEmpty(value)) {
                return value.trim();
            } else {
                return def;
            }
        } catch (Exception ignored){
            return def;
        }
    }
}
