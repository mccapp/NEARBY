package ren.nearby.lib.ui.base;

/**
 * Created by Administrator on 2018/5/4 0004.
 */

public interface BasePresenter2<T> {

    /**
     * 绑定P和V 的关系
     * Binds presenter with a view when resumed. The Presenter will perform initialization here.
     *
     * @param view the view associated with this presenter
     */
    void takeView(T view);

    /**
     * 当 页面销毁的时候取消对view 的引用
     * Drops the reference to the view when destroyed
     */
    void dropView();

}
