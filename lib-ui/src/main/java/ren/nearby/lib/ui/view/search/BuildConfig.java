package ren.nearby.lib.ui.view.search;

/**
 * Created by Administrator on 2016/9/8.
 */
public class BuildConfig {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "br.com.mauker.materialsearchview";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 10;
    public static final String VERSION_NAME = "1.1.3";
    // Fields from default config.
    public static final int MAX_HISTORY = 10;
}
