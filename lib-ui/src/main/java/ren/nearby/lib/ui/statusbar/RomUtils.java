package ren.nearby.lib.ui.statusbar;

import android.os.Build;
import android.text.TextUtils;

import java.util.Locale;

/**
 * Created by wuchundu on 2018/10/12.
 */
public class RomUtils {

    public static boolean isHuaweiRom() {
        String manufacturer = Build.MANUFACTURER;
        return !TextUtils.isEmpty(manufacturer) && manufacturer.contains("HUAWEI");
    }

    public static boolean isMiuiRom() {
        return !TextUtils.isEmpty(getSystemProperty("ro.miui.ui.version.name"));
    }

    public static boolean isOppoRom() {
        String a = getSystemProperty("ro.product.brand");
        return !TextUtils.isEmpty(a) && a.toLowerCase(Locale.getDefault()).contains("oppo");
    }

    public static boolean isVivoRom() {
        String a = getSystemProperty("ro.vivo.os.name");
        return !TextUtils.isEmpty(a)
                && a.toLowerCase(Locale.getDefault()).contains("funtouch");
    }

    private static String getSystemProperty(String propName) {
        return SystemProperties.get(propName, null);
    }
}
