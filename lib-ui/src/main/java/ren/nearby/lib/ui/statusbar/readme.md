        if(isApplyStatusBarTranslucency()) {
            if (statusBarCompat == null) {
                statusBarCompat = new StatusBarCompat(this);
            }
            setTranslucentStatus();
        }
        if (!isApplyStatusBarTranslucency() && isApplyStatusBarColor()) {
            if (statusBarCompat == null) {
                statusBarCompat = new StatusBarCompat(this);
            }
            setStatusBarColor(getResources().getColor(R.color.tab_bg_color));
        }

	    /**
     * 设置透明状态栏
     */
    protected void setTranslucentStatus() {
        if (statusBarCompat != null) {
            statusBarCompat.setImmersionBar();
        }
    }
       /**
     * 设置状态栏颜色
     *
     * @param color
     */
    protected void setStatusBarColor(int color) {
        if (statusBarCompat != null) {
            statusBarCompat.setColorBar(color);
        }
    }

     /**
         * 是否使用透明状态栏 默认false
         *
         * @return
         */
        protected boolean isApplyStatusBarTranslucency(){
            return false;
        }

        /**
         * 是否使用状态栏颜色 默认true,isApplyStatusBarTranslucency,则不会有颜色
         *
         * @return
         */
        protected boolean isApplyStatusBarColor(){
            return true;
        }