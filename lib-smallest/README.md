 根据设计图标注，在布局写上对应的值。
 在安卓中，系统密度为160dpi的中密度手机屏幕为基准屏幕，即320×480的手机屏幕。在这个屏幕中，1dp=1px。320x480分辨率对应的其他分辨率的比例如下：
 密度       ldpi           mdpi          hdpi          xhdpi        xxdpi
 分辨率    204*320        320*480       480*800       720*1280     1080*1920
 比例        3               4             6             8            12
 3-4  = 0.75
 4-6  = 1.5
 4-8  = 2
 4-12 = 3
 所以，如果UI给的是720x1280分辨率的图， 那么dp = px / 2， 给的是1080x1920分辨率的图，那么 dp = px / 3，即根据比例即可。

 举例：UI在720x1280上做的图，其中一个按钮的宽高分辨为：宽720px，高为100px，字体大小为30px，在布局中则这样使用：
 <Button
 android:layout_width="@dimen/dp_360"
 android:layout_height="@dimen/dp_50"
 android:textSize="@dimen/sp_15"
 android:text="Button"/>

 在Android Studio中安装ScreenMatch插件，