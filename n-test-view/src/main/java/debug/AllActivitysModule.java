package debug;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ren.nearby.http.di.ActivityScoped;
import ren.nearby.http.di.BaseActivityComponent;
import ren.nearby.http.di.BaseFragmentComponent;
import ren.nearby.http.di.BaseFragmentV4Component;


import ren.nearby.textview.StoragePathAct;
import ren.nearby.textview.TextViews;
import ren.nearby.textview.di.ActModule;


/**
 * 配置所有activity注入
 * Created by Administrator on 2018/5/3 0003.
 */



@Module(subcomponents = {
        // 注入 activity
        BaseActivityComponent.class,
        //android.app.Fragment 兼容的最低版本是android:minSdkVersion="11" 即3.0版
        BaseFragmentV4Component.class,
        //android.support.v4.app.Fragment 兼容的最低版本是android:minSdkVersion="4" 即1.6版
        BaseFragmentComponent.class
})
public abstract class AllActivitysModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract StoragePathAct storagePathAct();




    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract TextViews textViews();

}
