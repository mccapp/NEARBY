package debug;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;


/**
 * <p>类说明</p>
 *
 * @author nearby 2017/2/15 20:09
 * @version V1.2.0
 * @name HomeApplication
 */
public class DebugApplication extends Application implements HasActivityInjector, HasFragmentInjector, HasSupportFragmentInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return fragmentInjector;
    }

    @Inject
    DispatchingAndroidInjector<android.support.v4.app.Fragment> supportFragmentInjector;

    @Override
    public AndroidInjector<android.support.v4.app.Fragment> supportFragmentInjector() {
        return supportFragmentInjector;
    }

    private String tag = "BMain";


    @Override
    public void onCreate() {
        super.onCreate();
        Logger.e("进入debug....");
        initLog();
        DebugDiModuleComponent debugDiModuleComponent =
                DaggerDebugDiModuleComponent
                        .builder()
                        .debugHttpModule(new DebugHttpModule(this, 0))
                        .build();
        debugDiModuleComponent.inject(this);


    }

    public void initLog() {
        FormatStrategy formatStrategy = PrettyFormatStrategy
                .newBuilder()
                .showThreadInfo(true)
//                .methodCount(5)
//                .methodOffset(7)
//                .logStrategy()
                .tag("MAIN")
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
    }

    /**
     * 在这里模拟登陆，然后拿到sessionId或者Token
     * 这样就能够在组件请求接口了
     */
//    private void login() {
//        HttpClient client = new HttpClient.Builder()
//                .baseUrl("http://gank.io/api/data/")
//                .url("福利/10/1")
//                .build();
//        client.get(new OnResultListener<String>() {
//
//            @Override
//            public void onSuccess(String result) {
//                Logger.e(result);
//            }
//
//            @Override
//            public void onError(int code, String message) {
//                Logger.e(message);
//            }
//
//            @Override
//            public void onFailure(String message) {
//                Logger.e(message);
//            }
//        });
//    }
}
