package ren.nearby.textview;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import ren.nearby.lib.ui.base.BaseActivity;


/**
 * android 存储路径问题
 * Created by Administrator on 2017/8/30 0030.
 */

public class StoragePathAct extends BaseActivity {
    Button btn;
    TextView tvValue;
    EditText et_write;
    FileOutputStream fos;
    FileInputStream fis;

    @Override
    public int getLayoutRes() {
        return R.layout.testview_storage_path;
    }

    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.testview_mm_tubiao))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        btn = (Button) findViewById(R.id.btn);
        tvValue = (TextView) findViewById(R.id.tv_value);
        et_write = (EditText) findViewById(R.id.et_write);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //内存储存 ->
                //1.data
                String dataDirectory = Environment.getDataDirectory() + "/NBlock";
                //2.cache
                String cacheDirectory = Environment.getDownloadCacheDirectory() + "/NBlock";
                //3.system
                String rootDirectory = Environment.getRootDirectory() + "/NBlock";


                //外部存储 ->
                //1.SD卡路径  Storage/sdcard0 共有目录
                String storageDirectory = Environment.getExternalStorageDirectory() + "/NBlock";

                //2.私有目录 在APP包名下，如/Storage/emulated/0/Android/data/包名/files/test

                // /Storage/emulated/0/Android/data/包名/files/abc
                String filesDir = getExternalFilesDir("abc") + "/NBlock";
                // /Storage/emulated/0/Android/data/包名/cache/abc
                String cacheDir = getExternalCacheDir() + "/NBlock";

                Logger.e(dataDirectory + " \n" +
                        cacheDirectory + " \n" +
                        rootDirectory + " \n" +
                        storageDirectory + " \n" +
                        filesDir + " \n" +
                        cacheDir + " \n");

                tvValue.setText(dataDirectory + " \n" +
                        cacheDirectory + " \n" +
                        rootDirectory + " \n" +
                        storageDirectory + " \n" +
                        filesDir + " \n" +
                        cacheDir + " \n");

                try {
                    String fileName = "NBlock";
                    File file = new File(getFilesDir(), fileName);
                    if (!file.exists()) {

                        file.createNewFile();
                        Logger.e("创建" + file.getAbsolutePath());
                    } else {
                        String value = et_write.getText().toString().trim();

                        if (TextUtils.isEmpty(value)) {
                            Toast.makeText(StoragePathAct.this, "写入数据不能为空", Toast.LENGTH_LONG).show();
                            return;
                        }
                        fos = openFileOutput(fileName, Context.MODE_PRIVATE);
                        fos.write(value.getBytes());
                        fos.close();

                        fis = openFileInput(fileName);
                        byte[] bytes = new byte[1024];
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        while (fis.read(bytes) != -1) {
                            baos.write(bytes, 0, bytes.length);
                        }
                        fis.close();
                        baos.close();
                        tvValue.setText(new String(baos.toByteArray()));
                    }
                } catch (Exception e) {

                }
            }
        });
    }



    public void testStr (){
      /*  List<String> strs = new ArrayList<>();
        List<Object> objs = strs;
        objs.add(1);*/

    }


}
