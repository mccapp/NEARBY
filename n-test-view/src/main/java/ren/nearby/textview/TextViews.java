package ren.nearby.textview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;

import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ren.nearby.lib.ui.base.BaseActivity;
import ren.nearby.lib.utils.MD5Utils;
import ren.nearby.lib.utils.RequestJsonUtil;


/**
 * Created by Administrator on 2017/8/16 0016.
 */

public class TextViews extends BaseActivity {

    RecyclerView recyView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            JSONObject json = new JSONObject();
            json.put("userName", "111");
            json.put("passWord", "123");
            map.put("action", "add");
            map.put("nonceStr", RequestJsonUtil.random());
            map.put("content", json);
            map.put("controller", "user");
            String stringATemp = RequestJsonUtil.sortByJoinKey(map, "123456");
            Logger.e("stringATemp = " + stringATemp);
            String md5 = MD5Utils.MD5(stringATemp);
            Logger.e("md5 = " + md5);
            map.put("sign", md5);
            Logger.e("request = " + new JSONObject(map).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.testview_snap_helper);
        recyView = (RecyclerView) findViewById(R.id.testview_recy_view);

        CardAdapter cardAdapter = new CardAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        recyView.setLayoutManager(linearLayoutManager);

        recyView.setAdapter(cardAdapter);
        snapHelper.attachToRecyclerView(recyView);
        recyView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Logger.e("newState = " + newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                Logger.e("dx = " + dx + " - dy = " + dy);
            }
        });
    }
}
