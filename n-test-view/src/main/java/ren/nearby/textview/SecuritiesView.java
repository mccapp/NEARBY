package ren.nearby.textview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.orhanobut.logger.Logger;

/**
 * Created by Administrator on 2018/5/16 0016.
 */

public class SecuritiesView extends LinearLayout {

    private Paint mPaint;

    //圆的间距
    private float gap = 8;

    //半径
    private float radius = 10;

    //圆数量
    private int circleNum;

    //剩余
    private float remain;


    public SecuritiesView(Context context) {
        super(context);
    }

    public SecuritiesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SecuritiesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setDither(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (remain == 0) {
            remain = (int) (w - gap) % (2 * radius + gap);
        }
        circleNum = (int) ((w - gap) / (2 * radius + gap));
        Logger.e("remain = "+remain);
        Logger.e("w = "+w);
        Logger.e("gap = "+gap);
        Logger.e("gap = "+(2 * radius + gap));
        Logger.e("circleNum = "+circleNum);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < circleNum; i++) {
            float x = gap + radius + remain / 2 + ((gap + radius * 2) * i);
            canvas.drawCircle(x, 0, radius, mPaint);
            canvas.drawCircle(x, getHeight(), radius, mPaint);
        }
    }
}
