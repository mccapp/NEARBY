package ren.nearby.textview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.orhanobut.logger.Logger;

/**
 * Created by Administrator on 2017/8/16 0016.
 */

public class ViewXing extends View {


    Context mContext;
    //定义一支笔
    Paint paint = new Paint();

    public ViewXing(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewXing(Context context) {
        this(context, null);
    }

    public ViewXing(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
//        canvas.drawCircle(300, 300, 200, paint);

//        canvas.drawRect(100, 100, 500, 500, paint);
//        paint.setStrokeWidth(20);
//        paint.setStrokeCap(Paint.Cap.ROUND);
//        canvas.drawPoint(50, 50, paint);
//        paint.setStrokeCap(Paint.Cap.SQUARE);
//        canvas.drawPoint(80, 50, paint);
//        float[] points = {0, 0, 50, 50, 50, 100, 100, 50, 100, 100, 150, 50, 150, 100};
//        canvas.drawPoints(points, 2, 8, paint);


//        float[] points2 = {
//                20, 20 ,
//              120, 20 ,
//                70, 20,
//                70, 120,
//                20, 120,
//                120, 120,
//                150, 20,
//                250, 20,
//                150, 20,
//                150, 120,
//                250, 20,
//                250, 120,
//                150, 120,
//                250, 120};
//        canvas.drawLines(points2, paint);

        drawRect(canvas);
    }

    public void drawRect(Canvas canvas) {
//        canvas.drawRect(100, 100, 300, 200,paint);

//        RectF rect2 = new RectF(400, 10, 600, 100);
//        canvas.drawArc(rect2, 150, 240, false, paint);
//        Path path = new Path();
//        path.moveTo(10,10);
//        path.lineTo(10,100);
//        path.lineTo(300, 100);//画第二条直线
//        path.lineTo(500, 100);//第三条直线
//        path.close();//闭环
//        canvas.drawPath(path,paint);

        Logger.e(" width = " + (getWidth() - 3) + " - height = " + (getHeight() - 3));
        RectF rectF1 = new RectF(3, 3, mWidth - 3, mHeight - 3);
//        canvas.drawRect(rectF1,paint);
        canvas.drawArc(rectF1, 150, 240, false, paint);
        drawerNum(canvas);

    }

    private void drawerNum(Canvas canvas) {
        canvas.save(); //记录画布状态
        canvas.rotate(-(180 - 150 + 90), getWidth() / 2, getHeight() / 2);
        Logger.e(getWidth() + "- "+getHeight());
        Logger.e((-(180 - 150 + 90)) + " - " + (getWidth() / 2) + "- " + (getHeight() / 2));
        float rAngle = 240 / 10;
        for (int i = 0; i < 10 + 1; i++) {
            canvas.save(); //记录画布状态
            canvas.rotate(rAngle * i, getWidth() / 2, getHeight() / 2);
            Logger.e("rAngle = " + (rAngle * i));
            if (i % 2 == 0) {
                canvas.drawLine(getWidth() / 2, 30, getWidth() / 2, 15, paint);//画刻度线
                canvas.drawText("" + i * 10, getWidth() / 2 - 3 * 2, 50, paint);//画刻度
            } else {
                canvas.drawLine(getWidth() / 2, 30, getWidth() / 2, 15, paint);//画刻度线
            }


            canvas.restore();
        }
        canvas.restore();

    }

    private int mWidth;
    private int mHeight;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
       /* int realWidth = startMeasure(widthMeasureSpec);
        int realHeight = startMeasure(heightMeasureSpec);

        setMeasuredDimension(realWidth, realHeight);*/
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (widthMode == MeasureSpec.EXACTLY) {
            mWidth = widthSize;
        } else {
            mWidth = dpToPx(200, mContext);
        }


        if (heightMode == MeasureSpec.EXACTLY) {
            mHeight = heightSize;
        } else {
            mHeight = dpToPx(200, mContext);
        }
        Log.e("wing", mWidth + "");
        setMeasuredDimension(mWidth, mHeight);
    }

    public static int dpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int spToPx(int sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
}
