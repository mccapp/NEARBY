package ren.nearby.lib.http.cache;

import android.text.TextUtils;
import android.util.LruCache;

import com.google.gson.Gson;
import com.orhanobut.logger.Logger;

import java.io.UnsupportedEncodingException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by wukewei on 16/6/19.
 */
public class MemoryCache implements ICache{

    private LruCache<String, String> mCache;

    public MemoryCache() {
        final int maxMemory = (int) Runtime.getRuntime().maxMemory();
        final int cacheSize = maxMemory / 8;
        mCache = new LruCache<String, String>(cacheSize) {
            @Override
            protected int sizeOf(String key, String value) {
                try {
                    return value.getBytes("UTF-8").length;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return value.getBytes().length;
                }
            }
        };
    }

    @Override
    public <T> Observable<T> get(final String key, final Class<T> cls) {
        Logger.e("来了 MemoryCache get");
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<T> e) throws Exception {
                String result = mCache.get(key);
                Logger.e("来了 MemoryCache subscribe");
                if (e.isDisposed()) {
                    return;
                }

                if (TextUtils.isEmpty(result)) {
                    //rxjava1.x 支持null rxjava2.x 不支持否则报错
                    //throwError = onNext called with null. Null values are generally not allowed in 2.x operators and sources.
//                    e.onNext(null);
                    Logger.e("来了 MemoryCache subscribe null");
                } else {
                    Logger.e("来了 MemoryCache subscribe t");
                    T t = new Gson().fromJson(result, cls);
                    e.onNext(t);
                }

                e.onComplete();
            }
        }) .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public <T> void put(String key, T t) {
        if (null != t) {
            mCache.put(key, new Gson().toJson(t));
        }
    }

    public void clearMemory(String key) {
        mCache.remove(key);
    }
}
