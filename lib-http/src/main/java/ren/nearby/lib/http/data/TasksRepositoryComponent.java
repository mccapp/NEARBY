package ren.nearby.lib.http.data;

import javax.inject.Singleton;

import dagger.Component;
import ren.nearby.lib.http.HttpModule;


/**
 * Created by Angel on 2017/03/02.
 */
@Singleton
@Component(modules = {TasksRepositoryModule.class, HttpModule.class})
public interface TasksRepositoryComponent {

    /**
     * @see TasksRepository
     *  构建注入网络请求与控制器层到类  TasksRepository.class
     *  此处有个返回值就是有利于在调用者容易获得使用者实例
     */
    TasksRepository getTasksRepository();



}
