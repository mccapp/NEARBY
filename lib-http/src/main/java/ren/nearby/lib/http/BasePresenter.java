package ren.nearby.lib.http;


import com.orhanobut.logger.Logger;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ren.nearby.lib.ui.base.IPresenter;

/**
 * rx的生命周期管理
 */
public abstract class BasePresenter implements IPresenter {
    protected CompositeDisposable mCompositeSubscription;

    protected void unSubscribe() {
        if (mCompositeSubscription != null) {
            Logger.e("unsubscribe start ");
            mCompositeSubscription.dispose();
        }
        Logger.e("unsubscribe end ");
    }

    protected void addSubscrebe(Disposable subscription) {
        if (mCompositeSubscription == null) {
            Logger.e("addSubscrebe  start ");
            mCompositeSubscription = new CompositeDisposable();

        }
        mCompositeSubscription.add(subscription);
        Logger.e("addSubscrebe end ");
    }

    @Override
    public void detachView() {
        Logger.e("detachView "); unSubscribe();
    }
}
