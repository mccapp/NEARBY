package ren.nearby.lib.http;

import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Administrator on 2017/12/13 0013.
 */

public class RxSpUtil {


    /**
     *
     * @param context  上下文
     * @param fileKey 文件路径
     * @param valueKey 保存的键
     * @param result 保存的值
     */
    public static final synchronized void saveValue(final Context context, final String fileKey, final String valueKey, final Object result) {
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                SharedPreferences sp = context.getSharedPreferences(fileKey, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                if (result instanceof String) {
                    editor.putString(valueKey, (String) result);
                } else if (result instanceof Boolean) {
                    editor.putBoolean(valueKey, (Boolean) result);
                } else if (result instanceof Integer) {
                    editor.putInt(valueKey, (Integer) result);
                } else if (result instanceof Long) {
                    editor.putLong(valueKey, (Long) result);
                } else if (result instanceof Float) {
                    editor.putFloat(valueKey, (Float) result);
                }
                editor.commit();
            }
        }).compose(SchedulersCompat.applySchedulers()).subscribe();//处理线程;
    }

    public interface SpResult<T> {
        void onResult(T t);
    }

    /**
     *
     * @param context 上下文
     * @param defaultValue 默认值
     * @param fileKey 文件路径
     * @param valueKey 取值的键
     * @param resultListener 回调
     */
    public static synchronized void getValue(final Context context, final Object defaultValue, final String fileKey, final String valueKey, final SpResult resultListener) {
        Observable.just("")
                .map(new Function<String, Object>() {
                    @Override
                    public Object apply(String s) throws Exception {
                        SharedPreferences sp = context.getSharedPreferences(fileKey, Context.MODE_PRIVATE);
                        Object result = null;
                        if (defaultValue instanceof String) {
                            result = sp.getString(valueKey, (String)defaultValue);
                        } else if (defaultValue instanceof Boolean) {
                            result = sp.getBoolean(valueKey, (Boolean)defaultValue);
                        } else if (defaultValue instanceof Integer) {
                            result = sp.getInt(valueKey, (Integer) defaultValue);
                        } else if (defaultValue instanceof Long) {
                            result = sp.getLong(valueKey, (Long) defaultValue);
                        } else if (defaultValue instanceof Float) {
                            result = sp.getFloat(valueKey, (Float) defaultValue);
                        }
                        com.orhanobut.logger.Logger.e("result = " + result);
                        return result;
                    }
                }).compose(SchedulersCompat.applySchedulers())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        if (o == null) {

                        }
                        if (resultListener != null) {
                            resultListener.onResult(o);
                        }
                        com.orhanobut.logger.Logger.e("o = " + o);
                    }
                });

//        }).compose(SchedulersCompat.applySchedulers()).subscribe();//处理线程;
    }
}
