package ren.nearby.lib.http.scope;

import android.app.Fragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

import ren.nearby.lib.ui.base.BaseLazyLoadFragment;

/**
 * Created by Administrator on 2018/5/3 0003.
 */

@Subcomponent(modules = {
        AndroidInjectionModule.class
})
public interface BaseFragmentV4Component extends AndroidInjector<BaseLazyLoadFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseLazyLoadFragment> {
    }

}
