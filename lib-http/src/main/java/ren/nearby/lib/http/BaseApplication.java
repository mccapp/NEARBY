package ren.nearby.lib.http;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.tencent.smtt.sdk.QbSdk;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

import me.drakeet.library.CrashWoodpecker;
import me.drakeet.library.PatchMode;
import ren.nearby.lib.http.data.TasksRepositoryComponent;
import ren.nearby.lib.http.scope.AppComponent;

//import com.squareup.leakcanary.LeakCanary;


/**
 * Created by Administrator on 2017/8/3 0003.
 */

public class BaseApplication extends Application implements HasActivityInjector {

    private String tag = "NBlock";

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }


    private static TasksRepositoryComponent mRepositoryComponent;

    public static TasksRepositoryComponent getTasksRepositoryComponent() {
        return mRepositoryComponent;
    }

//    private ApplicationDelegate applicationDelegate;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Logger.e("BaseApplication - attachBaseContext");
//        https://blog.csdn.net/javazejian/article/details/73413292
//        applicationDelegate = new ApplicationDelegate();
//        applicationDelegate.attachBaseContext(base);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        initLog();
        initCrash();
        initLeakCanary();
        initStetho();
        initDagger2();
        x5Web();
//        组件全局application的实现和数据的初始化
//        applicationDelegate.onCreate(null, null);
    }

    public void x5Web() {
        //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。

        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                // TODO Auto-generated method stub
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Logger.e(" onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
                // TODO Auto-generated method stub
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }


    /**
     * dagger2注入
     */
    public void initDagger2() {
        //第一种方式  简单方式
        /**
         * {@link ren.nearby.main.music.MusicAct#onCreate}
         */
     /*   appComponent = DaggerAppComponent
                .builder()
                .httpModule(new HttpModule(this, 0))
                .build();*/

        /**
         * {@link ren.nearby.main.MainComponent#getTasksRepository }
         *   第二种方式 注入式初始化application
         */

//        mRepositoryComponent = DaggerTasksRepositoryComponent
//                .builder()
//                .httpModule(new HttpModule(this, 0))
//                .build();



    }


    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    /**
     * 网络请求分析
     */
    public void initStetho() {
        Stetho.initializeWithDefaults(this);
        //配置okhttp库的网络请求分析 我们打开Chrome，在地址栏输入chrome://inspect/
        //new OkHttpClient.Builder()
        //.addNetworkInterceptor(new StethoInterceptor())
        //.build()
    }

    /**
     * 内存泄漏检测
     */
    public void initLeakCanary() {
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return;
//        }
//        LeakCanary.install(this);
    }

    /**
     * 初始化输出日志配置
     */
    public void initLog() {
        FormatStrategy formatStrategy = PrettyFormatStrategy
                .newBuilder()
                .showThreadInfo(true)
//                .methodCount(5)
//                .methodOffset(7)
//                .logStrategy()
                .tag(getLogTag())
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
    }

    /**
     * 异常处理分析
     */
    public void initCrash() {
        CrashWoodpecker.instance()
                .withKeys("widget", "me.drakeet")
                .setPatchMode(PatchMode.SHOW_LOG_PAGE)
                .setPatchDialogUrlToOpen("https://drakeet.me")
                .setPassToOriginalDefaultHandler(true)
                .flyTo(this);
    }

    /**
     * 获取log标题
     *
     * @return
     */
    public String getLogTag() {
        return tag;
    }
}
