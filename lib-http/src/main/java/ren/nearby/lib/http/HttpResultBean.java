package ren.nearby.lib.http;

/**
 * Created by Administrator on 2016/4/28.
 */
public class HttpResultBean<T>   {

    public int count;
    public int status;
    private String error;
    //建议json格式以下
    /**
     * {"sattus{code}":1,
     * "message{info}":"\u67e5\u8be2\u6210\u529f",
     * "data{list}":{"JsonObject":"","JsonArray":[{"JsonObject":"a"},{"JsonObject":"b"},{"JsonObject":"c"}]}}
     */
    public int currPage;
    public int code;
    public int totalCount;
    public String msg;
    //用来模仿Data
    public T data;
    public T musics;

    public T getMusics() {
        return musics;
    }

    public void setMusics(T musics) {
        this.musics = musics;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpResultBean{" +
                "currPage=" + currPage +
                ", code=" + code +
                ", totalCount=" + totalCount +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
