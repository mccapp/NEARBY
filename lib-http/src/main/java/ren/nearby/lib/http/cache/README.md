 使用方式
 1.
     networkCache = new NetworkCache<MusicBeanList>() {
                @Override
                public Observable<MusicBeanList> get(String key, Class<MusicBeanList> cls) {

                    return observable2
                            .compose(SchedulersCompat.applySchedulers())//处理线程
                            .compose(RxResultHelper.handleResult())
                            .flatMap(new Function<List<MusicBean>, ObservableSource<MusicBeanList>>() {
                                @Override
                                public ObservableSource<MusicBeanList> apply(@io.reactivex.annotations.NonNull List<MusicBean> musicBeen) throws Exception {
                                    Logger.e("来了 ...");
                                    MusicBeanList popular = new MusicBeanList(musicBeen);
                                    return Observable.fromArray(popular);
                                }
                            });
                }
            };
 2.
  Subscription subscription;
  subscription = CacheLoader.getInstance(mView.getContext())
                 .asDataObservable(key + sign, ListIndexBean.class, networkCache)
                 .map(new Func1<Object, List<Object>>() {
                     @Override
                     public List<Object> call(ListIndexBean listPopular) {
                         return listPopular.data;
                     }
                 }).subscribe(new SubscriberInterface<List<Object>>() {
                                     @Override
                                     public void onStart() {
                                         super.onStart();
                                     }
                                     @Override
                                     public void onCompleted() {
                                     }
                                     @Override
                                     public void _onError(String msg) {
                                     }
                                     @Override
                                     public void onNext(List<Object> t) {
                                     }
                                 });
                         addSubscrebe(subscription);