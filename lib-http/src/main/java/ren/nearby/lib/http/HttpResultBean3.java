package ren.nearby.lib.http;


import ren.nearby.bean.beans.TestBean;

/**
 * Created by Administrator on 2016/4/28.
 */
public class HttpResultBean3<T> {
    private ResultHead REP_HEAD;
    private TestBean REP_BODY;

    public ResultHead getREP_HEAD() {
        return REP_HEAD;
    }

    public void setREP_HEAD(ResultHead REP_HEAD) {
        this.REP_HEAD = REP_HEAD;
    }

    public TestBean getREP_BODY() {
        return REP_BODY;
    }

    public void setREP_BODY(TestBean REP_BODY) {
        this.REP_BODY = REP_BODY;
    }

    class ResultHead {
        private String SIGN;

        public String getSIGN() {
            return SIGN;
        }

        public void setSIGN(String SIGN) {
            this.SIGN = SIGN;
        }

        @Override
        public String toString() {
            return "ResponseHead{" +
                    "SIGN='" + SIGN + '\'' +
                    '}';
        }

    }
}
