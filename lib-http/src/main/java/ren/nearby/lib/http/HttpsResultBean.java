package ren.nearby.lib.http;

/**
 * Created by Administrator on 2017/6/22 0022.
 */

public class HttpsResultBean<T> {

    private int nstatus;
    private String nmsg;
    private String ntoken;
    private String naesKey;
    private String nrawKey;
    private String ndata;
    private T data;

    //移动接口安全规范
    private String action;
    private T content;
    private String controller;
    private int returnCode;
    private String returnMsg;
    private String sign;


    public int getNstatus() {
        return nstatus;
    }

    public void setNstatus(int nstatus) {
        this.nstatus = nstatus;
    }

    public String getNmsg() {
        return nmsg;
    }

    public void setNmsg(String nmsg) {
        this.nmsg = nmsg;
    }

    public String getNtoken() {
        return ntoken;
    }

    public void setNtoken(String ntoken) {
        this.ntoken = ntoken;
    }

    public String getNaesKey() {
        return naesKey;
    }

    public void setNaesKey(String naesKey) {
        this.naesKey = naesKey;
    }

    public String getNrawKey() {
        return nrawKey;
    }

    public void setNrawKey(String nrawKey) {
        this.nrawKey = nrawKey;
    }

    public String getNdata() {
        return ndata;
    }

    public void setNdata(String ndata) {
        this.ndata = ndata;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    //移动接口安全规范


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
