package ren.nearby.lib.http.scope;

import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2018/5/3 0003.
 */

@Subcomponent(modules = {
        AndroidInjectionModule.class
})
public interface BaseActivityComponent extends AndroidInjector<BaseActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseActivity> {
    }

}
