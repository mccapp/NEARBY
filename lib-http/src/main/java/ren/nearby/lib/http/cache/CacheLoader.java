package ren.nearby.lib.http.cache;

import android.app.Application;
import android.content.Context;

import com.orhanobut.logger.Logger;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by wukewei on 16/6/19.
 */
public class CacheLoader {
    private static Application application;

    public static Application getApplication() {
        return application;
    }

    private ICache mMemoryCache, mDiskCache;

    private CacheLoader() {

        mMemoryCache = new MemoryCache();
        mDiskCache = new DiskCache();
    }

    private static CacheLoader loader;

    public static CacheLoader getInstance(Context context) {
        application = (Application) context.getApplicationContext();
        if (loader == null) {
            synchronized (CacheLoader.class) {
                if (loader == null) {
                    loader = new CacheLoader();
                }
            }
        }
        return loader;
    }


    public <T> Observable<T> asDataObservable(String key, Class<T> cls, NetworkCache<T> networkCache) {
        Logger.e("asDataObservable..");

        Observable<T> observable = Observable.concat(
                memory(key, cls),
                disk(key, cls),
                network(key, cls, networkCache))
               .firstElement().toObservable();

        return observable;
    }

    private <T> Observable<T> memory(String key, Class<T> cls) {

        return mMemoryCache.get(key, cls).doOnNext(
                new Consumer<T>() {
                    @Override
                    public void accept(T t) throws Exception {
                        if (null != t) {
                            Logger.e("我是来自内存");
                        } else {
                            Logger.e("我是来自内存 null");
                        }
                    }
                }
        );
    }

    private <T> Observable<T> disk(final String key, Class<T> cls) {

        return mDiskCache.get(key, cls)
                .doOnNext(new Consumer<T>() {
                    @Override
                    public void accept(T t) throws Exception {
                        if (null != t) {
                            Logger.e("我是来自磁盘");
                            mMemoryCache.put(key, t);
                        } else {
                            Logger.e("我是来自磁盘 null");
                        }
                    }
                });
    }




    private <T> Observable<T> network(final String key, Class<T> cls
            , NetworkCache<T> networkCache) {
        Logger.e("来了 network get = " + key);
        return networkCache.get(key, cls)
                .doOnNext(new Consumer<T>() {
                    @Override
                    public void accept(T t) throws Exception {
                        if (null != t) {
                            Logger.e("我是来自网络");
                            mDiskCache.put(key, t);
                            mMemoryCache.put(key, t);
                        } else {
                            Logger.e("我是来自网络 null");
                        }
                    }
                });
    }


    public void clearMemory(String key) {
        ((MemoryCache) mMemoryCache).clearMemory(key);
    }


    public void clearMemoryDisk(String key) {
        ((MemoryCache) mMemoryCache).clearMemory(key);
        ((DiskCache) mDiskCache).clearDisk(key);
    }
}
