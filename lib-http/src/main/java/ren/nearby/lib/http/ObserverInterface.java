package ren.nearby.lib.http;


import com.orhanobut.logger.Logger;

import java.net.SocketTimeoutException;

import io.reactivex.Observer;
import ren.nearby.lib.http.exception.ServerException;


/**
 * 抽象类由子类具体实现数据处理 由#Persenter层实现处理业务逻辑
 */
public abstract class ObserverInterface<T>  implements Observer<T>{

    @Override
    public void onError(Throwable e) {
        Logger.e("进入异常开始....." + e.getMessage() + "\n" + e.toString());
        if (e instanceof java.net.UnknownHostException) {
            throwError("网络连接异常");
            return;
        }
        if (e instanceof java.net.ConnectException) {
            //HTTP没有互联网连接异常
            throwError("服务连接异常");
            return;
        } else if (e instanceof SocketTimeoutException) {
            //HTTP服务器向下异常HTTP一般错误异常
            throwError("网络连接超时");
            return;
        } else if (e instanceof ServerException) {
            throwError("服务器异常");
            return;
        } else if (e instanceof IllegalStateException) {
            throwError("数据解析异常");
            return;
        }
        Logger.e("进入异常结束-" + e.getMessage() + "\n" + e.toString());
        throwError(e.getMessage());
    }

    public abstract void throwError(String msg);


}
