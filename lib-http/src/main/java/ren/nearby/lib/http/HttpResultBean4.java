package ren.nearby.lib.http;


import ren.nearby.bean.beans.TestBean;

/**
 * Created by Administrator on 2016/4/28.
 */
public class HttpResultBean4<T> {
    private int code;
    private String message;
    private T result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
