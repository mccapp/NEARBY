/*
 * Copyright 2015 zengzhihao.github.io. All rights reserved.
 * Support: http://zengzhihao.github.io
 */

package ren.nearby.lib.http.exception;


public class Server2Exception extends ApiException {

    private ApiError _apiError;

    public ApiError getApiError() {
        return _apiError;
    }

    public Server2Exception(String detailMessage, Throwable throwable, ApiError apiError) {
        super(detailMessage, throwable);
        _apiError = apiError;
    }

}
