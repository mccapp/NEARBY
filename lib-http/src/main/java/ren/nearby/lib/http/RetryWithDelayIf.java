package ren.nearby.lib.http;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;


/**
 * Created by Administrator on 2016/5/7.
 */
public class RetryWithDelayIf implements Function<Observable<? extends Throwable>, Observable<?>> {
    private final int maxRetries;
    private final int retryDelayMillis;
    private int retryCount;
    private Function<Throwable, Boolean> retryIf;

    public RetryWithDelayIf(final int maxRetries, final int retryDelayMillis, Function<Throwable, Boolean> retryIf) {
        this.maxRetries = maxRetries;
        this.retryDelayMillis = retryDelayMillis;
        this.retryCount = 0;
        this.retryIf = retryIf;
    }


    @Override
    public Observable<?> apply(Observable<? extends Throwable> attempts) {
        return attempts.zipWith(Observable.range(1, 5), new BiFunction<Throwable, Integer, Object>() {
            @Override
            public Object apply(@NonNull Throwable throwable, @NonNull Integer integer) throws Exception {
                return integer;
            }
        }).flatMap(new Function<Object, ObservableSource<?>>() {
            @Override
            public ObservableSource<?> apply(@NonNull Object o) throws Exception {
                return null;
            }
        });
    }
}
