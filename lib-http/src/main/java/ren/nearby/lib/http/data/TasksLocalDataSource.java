package ren.nearby.lib.http.data;

import android.support.annotation.NonNull;

import com.orhanobut.logger.Logger;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Angel on 2017/03/02.
 */

@Singleton
class TasksLocalDataSource implements TasksDataSource {
    @Inject
    TasksLocalDataSource() {

    }
    @Override
    public void login(Map maps, final ResponseCallBack callBack) {
        Logger.e("TasksLocalDataSource - login");


    }
    @Override
    public void music(Map maps, final ResponseCallBack callBack) {
        Logger.e("TasksLocalDataSource - login");


    }


    @Override
    public void registered(@NonNull Map map, @NonNull final ResponseCallBack callback) {
        Logger.e("TasksRemoteDataSource - register");
    }
    @Override
    public void collection(@NonNull Map map, @NonNull final ResponseCallBack callback) {
        Logger.e("TasksRemoteDataSource - register");
    }
    @Override
    public void testToken(@NonNull Map map, @NonNull final ResponseCallBack callback) {
        Logger.e("TasksRemoteDataSource - register");
    }

    @Override
    public void dowAapk(@NonNull ResponseCallBack callback) {

    }
}
