/*
 * Copyright 2015 zengzhihao.github.io. All rights reserved.
 * Support: http://zengzhihao.github.io
 */

package ren.nearby.lib.http.exception;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.HttpException;
import retrofit2.Retrofit;

//http://blog.csdn.net/ysmintor_/article/details/67637295

public class ApiException extends RuntimeException {

    public ApiException(Throwable throwable) {
        super(throwable);
    }

    public ApiException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public static ApiException create(Throwable throwable, Retrofit retrofit) {
        if (throwable instanceof HttpException) {
            Converter<ResponseBody, ApiError> errorConverter = retrofit
                    .responseBodyConverter(ApiError.class, new Annotation[0]);
            ApiError apiError = null;
            try {
                apiError = errorConverter
                        .convert(((HttpException) throwable).response().errorBody());
            } catch (IOException e) {
                // ignore. apiError is null.
            }
            return new Server2Exception("Server empty_error", throwable, apiError);
        } else if (throwable instanceof IOException) {
            return new NetworkException("network empty_error", throwable);
        } else {
            return new UnexpectedException("unexpected empty_error", throwable);
        }
    }
}
