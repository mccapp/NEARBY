package ren.nearby.lib.http.data;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by Angel on 2017/03/02.
 * 所有请求API入口
 */

public interface TasksDataSource {
    void registered(@NonNull Map map, @NonNull ResponseCallBack callback);

    void login(@NonNull Map map, @NonNull ResponseCallBack callback);

    void collection(@NonNull Map map, @NonNull ResponseCallBack callback);

    void music(@NonNull Map map, @NonNull ResponseCallBack callback);

    void testToken(@NonNull Map map, @NonNull ResponseCallBack callback);

    void  dowAapk(@NonNull ResponseCallBack callback);
}
