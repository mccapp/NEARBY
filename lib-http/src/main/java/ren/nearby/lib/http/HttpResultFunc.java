package ren.nearby.lib.http;


import io.reactivex.functions.Function;

/**
 * 数据格式的状态判断抽象类 由子类实现
 */
public abstract class HttpResultFunc<T> implements Function<HttpResultBean2<T>, T> {
    abstract public T apply(HttpResultBean2<T> httpResult);
}
