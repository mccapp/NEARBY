package ren.nearby.lib.http.data;

/**
 * Created by Angel on 2017/03/03.
 * 控制层实现回调给视图层的接口
 */

public interface ResponseCallBack<T> {


    /**
     * 请求开始
     *
     * @param action
     */
    void onDataStart(int action);

    /**
     * 响应反馈的信息
     * @param message
     */
    void onToast(String message);
    /**
     * 请求结果
     *
     * @param from 指定是哪个动作请求
     * @param results 响应的结果集
     */
    void onDataSuccess(int from, Object results);

    /**
     * 请求结果
     * @param from 指定是哪个动作请求
     * @param error 响应失败的信息
     */
    void onDataFailure(int from, String error);


}
