/*
 * Copyright 2015 zengzhihao.github.io. All rights reserved.
 * Support: http://zengzhihao.github.io
 */

package ren.nearby.lib.http.exception;


public class UnexpectedException extends ApiException {

    public UnexpectedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
