/*
 * Copyright 2015 zengzhihao.github.io. All rights reserved.
 * Support: http://zengzhihao.github.io
 */

package ren.nearby.lib.http.exception;


public class NetworkException extends ApiException {

    public NetworkException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
