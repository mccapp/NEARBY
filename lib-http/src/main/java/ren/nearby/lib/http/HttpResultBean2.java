package ren.nearby.lib.http;


/**
 * Created by Administrator on 2016/4/28.
 */
public class HttpResultBean2<T> {
    //状态码
    private int error;
    //提示消失
    private String msg;
    //用来模仿Data
    private T list;
    private int totalNum;

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public T getList() {
        return list;
    }

    public void setList(T list) {
        this.list = list;
    }



    /**
     * 登录属性
     * start 别问为什么
     *
     * @return
     */
    //冻结金额
    private float freeze;
    //头像
    private String headImg;
    //加密的userId
    private String userId;
    //账户总额
    private float accountAmount;
    //用户名
    private String username;
    //可用余额
    private float availableBalance;
    //是否已经开通托管账户（返回true或者false）
    private boolean isIps;

    public float getFreeze() {
        return freeze;
    }

    public void setFreeze(float freeze) {
        this.freeze = freeze;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(float accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(float availableBalance) {
        this.availableBalance = availableBalance;
    }

    public boolean isIps() {
        return isIps;
    }

    public void setIps(boolean ips) {
        isIps = ips;
    }

    /*************************************************************************************************/

    /**
     * 我的账户属性
     *
     * @return
     */
    //账户总额
    private String amount;
    //待收总额
    private double frozen_amount;
    //已赚收益
    private double sum_income;
    //可用余额
    private double balance;
    //用户名称
    private String name;
    //用户头像
    private String photo;
    //更新头像
    private String imgStr;


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public double getFrozen_amount() {
        return frozen_amount;
    }

    public void setFrozen_amount(double frozen_amount) {
        this.frozen_amount = frozen_amount;
    }

    public double getSum_income() {
        return sum_income;
    }

    public void setSum_income(double sum_income) {
        this.sum_income = sum_income;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getImgStr() {
        return imgStr;
    }

    public void setImgStr(String imgStr) {
        this.imgStr = imgStr;
    }

    /*************************************************************************************************/


    /**
     * 推广好友属性
     */
    private String spreadLink;

    public String getSpreadLink() {
        return spreadLink;
    }

    public void setSpreadLink(String spreadLink) {
        this.spreadLink = spreadLink;
    }

    /*************************************************************************************************/
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(" empty_error = " + error + " msg = " + msg);
        if (null != list) {
            sb.append(" list : " + list.toString());
        }
        return sb.toString();
    }
}
