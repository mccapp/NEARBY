package ren.nearby.lib.http.data;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 *  * This is a Dagger module. We use this to pass in the Context dependency to the
 * {@link
 *
 */
@Module
abstract class TasksRepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract TasksDataSource provideTasksLocalDataSource(TasksLocalDataSource dataSource);

    @Singleton
    @Binds
    @Remote
    abstract TasksDataSource provideTasksRemoteDataSource(TasksRemoteDataSource dataSource);




}
