package ren.nearby.lib.http.data;

import android.content.Context;

import com.orhanobut.logger.Logger;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class TasksRepository {

    /**
     * {@link TasksRemoteDataSource}
     */
    private final TasksDataSource mTasksRemoteDataSource;

    /**
     * {@link TasksLocalDataSource}
     */
    private final TasksDataSource mTasksLocalDataSource;

    private final Context mContext;


    @Inject
    TasksRepository(@Remote TasksDataSource tasksRemoteDataSource,
                    @Local TasksDataSource tasksLocalDataSource, Context context) {
        Logger.e("TasksRepository - 进入初始化了...");
        mTasksRemoteDataSource = tasksRemoteDataSource;
        mTasksLocalDataSource = tasksLocalDataSource;
        mContext = context;
    }

    public void login(Map<String, Object> map, ResponseCallBack callBack) {
       /* if (NetWorkUtils.isConnectedByState(mContext)) {
            //这里处理网络网络状态，异常处理*/

            /**
             * {@link TasksRemoteDataSource#login(Map, ResponseCallBack)}  }
             */
            mTasksRemoteDataSource.login(map, callBack);
       /* } else {
            *//**
             * {@link TasksLocalDataSource#login(Map, ResponseCallBack)}  }
             *//*
            mTasksLocalDataSource.login(map, callBack);
        }*/

    }
    public void registered(Map<String, Object> map, ResponseCallBack callBack) {
       /* if (NetWorkUtils.isConnectedByState(mContext)) {
            //这里处理网络网络状态，异常处理*/

        /**
         * {@link TasksRemoteDataSource#registered(Map, ResponseCallBack)}  }
         */
        mTasksRemoteDataSource.registered(map, callBack);
       /* } else {
            *//**
         * {@link TasksLocalDataSource#registered(Map, ResponseCallBack)}  }
         *//*
            mTasksLocalDataSource.registered(map, callBack);
        }*/

    }
    public void collection(Map<String, Object> map, ResponseCallBack callBack) {
       /* if (NetWorkUtils.isConnectedByState(mContext)) {
            //这里处理网络网络状态，异常处理*/

        /**
         * {@link TasksRemoteDataSource#collection(Map, ResponseCallBack)}  }
         */
        mTasksRemoteDataSource.collection(map, callBack);
       /* } else {
            *//**
         * {@link TasksLocalDataSource#collection(Map, ResponseCallBack)}  }
         *//*
            mTasksLocalDataSource.collection(map, callBack);
        }*/

    }

    public void music(HashMap<String, String> map, ResponseCallBack callBack) {
       /* if (NetWorkUtils.isConnectedByState(mContext)) {
            //这里处理网络网络状态，异常处理*/

        /**
         * {@link TasksRemoteDataSource#music(Map, ResponseCallBack)}  }
         */
        mTasksRemoteDataSource.music(map, callBack);
       /* } else {
            *//**
         * {@link TasksLocalDataSource#music(Map, ResponseCallBack)}  }
         *//*
            mTasksLocalDataSource.login(map, callBack);
        }*/

    }
    public void testToken(Map<String, Object> map, ResponseCallBack callBack) {
       /* if (NetWorkUtils.isConnectedByState(mContext)) {
            //这里处理网络网络状态，异常处理*/

        /**
         * {@link TasksRemoteDataSource#testToken(Map, ResponseCallBack)}  }
         */
        mTasksRemoteDataSource.testToken(map, callBack);
       /* } else {
            *//**
         * {@link TasksLocalDataSource#testToken(Map, ResponseCallBack)}  }
         *//*
            mTasksLocalDataSource.login(map, callBack);
        }*/

    }

    public void registered() {

    }

    public void loadInfo() {

    }

    public void dowApk(ResponseCallBack callBack) {
        /**
         * {@link TasksRemoteDataSource#dowAapk(ResponseCallBack)}
         */
        mTasksRemoteDataSource.dowAapk(callBack);
    }

}
