package ren.nearby.lib.http;

import android.app.Activity;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import ren.nearby.lib.ui.view.progress.DialogRequest;

/**
 * Created by wukewei on 16/5/26.
 */
public class SchedulersCompat {


    /**
     * 请求弹出框
     *
     * @param activity
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> ObservableTransformer<T, T> applyProgress(@NonNull final Activity activity, final String msg) {
        final WeakReference<Activity> contextWeakReference = new WeakReference<>(activity);
        DialogRequest.getInstance(contextWeakReference.get()).show(contextWeakReference.get(), msg);
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream.doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
//                        Logger.e("accept");

                    }
                }).doOnTerminate(new Action() {
                    @Override
                    public void run() throws Exception {
                        Activity context;
//                        Logger.e(" activity = " + contextWeakReference.get() + " - " + contextWeakReference.get().isFinishing());
                        if ((context = contextWeakReference.get()) != null) {
//                            Logger.e("run  - doOnTerminate - 1");
                            DialogRequest.getInstance(contextWeakReference.get()).dismiss(contextWeakReference.get());
//                            Logger.e("run - doOnTerminate - 1");
                        } else {
//                            Logger.e("run - doOnTerminate - 2");
                        }
                    }
                }).doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
//                                Logger.e("观察者订阅了它生成的Observable.....................");

                    }
                });
            }
        };
    }


    /*  private final static Observable.Transformer ioTransformer = new Observable.Transformer() {
          @Override
          public Object call(Object o) {
              return ((Observable) o)
                      .subscribeOn(Schedulers.io())
                      // Be notified on the main thread
                      .observeOn(AndroidSchedulers.mainThread())


          }
      };

      public static <T> Observable.Transformer<T, T> applyIoSchedulers() {
          return (Observable.Transformer<T, T>) ioTransformer;
      }*/
    public static final ObservableTransformer IO_TRANSFORMER = new ObservableTransformer() {
        @Override
        public ObservableSource apply(@NonNull Observable upstream) {
            return
                    upstream
                            .subscribeOn(Schedulers.io())
                            .unsubscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());
        }
    };

    public static final <T> ObservableTransformer<T, T> applySchedulers() {
        return (ObservableTransformer<T, T>) IO_TRANSFORMER;
    }

    public static <T> ObservableTransformer<T, T> toMain() {

        return new ObservableTransformer<T, T>() {

            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }


}
