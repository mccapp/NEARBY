package ren.nearby.lib.http.cache;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.orhanobut.logger.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by wukewei on 16/6/19.
 */
public class DiskCache implements ICache {

    private static final String NAME = ".db";
    public static long OTHER_CACHE_TIME = 10 * 60 * 1000;
    public static long WIFI_CACHE_TIME = 30 * 60 * 1000;
    File fileDir;

    public DiskCache() {
        fileDir = CacheLoader.getApplication().getCacheDir();
    }

    @Override
    public <T> Observable<T> get(final String key, final Class<T> cls) {
        Logger.e("来了 DiskCache get");
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<T> e) throws Exception {
                T t = (T) getDiskData1(key + NAME);

                if (e.isDisposed()) {
                    return;
                }

                if (t == null) {
                    Logger.e("来了 DiskCache subscribe null");
                    //rxjava1.x 支持null rxjava2.x 不支持否则报错
                    //throwError = onNext called with null. Null values are generally not allowed in 2.x operators and sources.
//                    e.onNext(null);
                } else {
                    Logger.e("来了 DiskCache subscribe t");
                    e.onNext(t);
                }
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());


    }

    @Override
    public <T> void put(final String key, final T t) {
        Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<T> e) throws Exception {
                boolean isSuccess = isSave(key + NAME, t);

                if (!e.isDisposed() && isSuccess) {

                    e.onNext(t);
                    e.onComplete();
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * 保存数据
     */
    private <T> boolean isSave(String fileName, T t) {
        File file = new File(fileDir, fileName);

        ObjectOutputStream objectOut = null;
        boolean isSuccess = false;
        try {
            FileOutputStream out = new FileOutputStream(file);
            objectOut = new ObjectOutputStream(out);
            objectOut.writeObject(t);
            objectOut.flush();
            isSuccess = true;
        } catch (IOException e) {
            Logger.e("写入缓存错误" + e.getMessage());
        } catch (Exception e) {
            Logger.e("写入缓存错误" + e.getMessage());
        } finally {
            closeSilently(objectOut);
        }
        return isSuccess;
    }

    /**
     * 获取保存的数据
     */
    private Object getDiskData1(String fileName) {
        File file = new File(fileDir, fileName);

        if (isCacheDataFailure(file)) {
            return null;
        }

        if (!file.exists()) {
            return null;
        }
        Object o = null;
        ObjectInputStream read = null;
        try {
            read = new ObjectInputStream(new FileInputStream(file));
            o = read.readObject();
        } catch (StreamCorruptedException e) {
            Logger.e("读取错误" + e.getMessage());
        } catch (IOException e) {
            Logger.e("读取错误" + e.getMessage());
        } catch (ClassNotFoundException e) {
            Logger.e("错误" + e.getMessage());
        } finally {
            closeSilently(read);
        }
        return o;
    }


    private void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception ignored) {
            }
        }
    }

    // 手机网络类型
    public static final int NETTYPE_WIFI = 0x01;
    public static final int NETTYPE_CMWAP = 0x02;
    public static final int NETTYPE_CMNET = 0x03;

    /**
     * 获取当前网络类型
     *
     * @return 0：没有网络 1：WIFI网络 2：WAP网络 3：NET网络
     */
    public static int getNetworkType(Context context) {
        int netType = 0;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = networkInfo.getExtraInfo();
            if (!TextUtils.isEmpty(extraInfo)) {
                if (extraInfo.toLowerCase().equals("cmnet")) {
                    netType = NETTYPE_CMNET;
                } else {
                    netType = NETTYPE_CMWAP;
                }
            }
        } else if (nType == ConnectivityManager.TYPE_WIFI) {
            netType = NETTYPE_WIFI;
        }
        return netType;


    }

    /**
     * 判断缓存是否已经失效
     */
    private boolean isCacheDataFailure(File dataFile) {
        if (!dataFile.exists()) {
            return false;
        }
        long existTime = System.currentTimeMillis() - dataFile.lastModified();
        boolean failure = false;
        if (getNetworkType(CacheLoader.getApplication()) == NETTYPE_WIFI) {
            failure = existTime > WIFI_CACHE_TIME ? true : false;
        } else {
            failure = existTime > OTHER_CACHE_TIME ? true : false;
        }

        return failure;
    }

    public void clearDisk(String key) {
        File file = new File(fileDir, key + NAME);
        if (file.exists()) file.delete();
    }
}
