package ren.nearby.lib.http;


import android.app.Activity;
import android.content.Context;

import com.orhanobut.logger.Logger;

import org.reactivestreams.Subscriber;

import java.lang.ref.WeakReference;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import ren.nearby.lib.http.exception.NetworkError;


/**
 * 抽象类由子类具体实现数据处理 由#Persenter层实现处理业务逻辑
 */
public abstract class BaseObserver<T> implements Observer<T> {

    private final WeakReference<Activity> mContent;
    private Disposable disposable;

    public BaseObserver() {
        super();
        mContent = new WeakReference<>(null);
    }

    public BaseObserver(Activity activity) {
        super();
        Logger.e("网络块 - BaseObserver");
        mContent = new WeakReference<Activity>(activity);

    }

    @Override
    public void onSubscribe(Disposable d) {
        Logger.e("网络块 - onSubscribe");
        disposable = d;
    }


    @Override
    public void onNext(T t) {
        Logger.e("网络块 - onNext");
    }

    @Override
    public void onError(Throwable e) {
        Logger.e("网络块 - onError");
        if (mContent.get() != null) {
            NetworkError.error(mContent.get(), e);
        }
        disposeIt();
    }


    @Override
    public void onComplete() {
        Logger.e("网络块 - onComplete");
        disposeIt();
    }

    /**
     * 销毁disposable
     */
    private void disposeIt() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
//            mContent.clear();
//            Logger.e("disposeIt");
            disposable = null;
        }
    }


}
