package ren.nearby.lib.http;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import ren.nearby.bean.beans.APIBean;
import ren.nearby.bean.beans.CollectionBean;
import ren.nearby.bean.beans.FromAndAction;
import ren.nearby.bean.beans.MusicBean;
import ren.nearby.bean.beans.UserBean;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;


/**
 * Created by Administrator on 2017/9/1 0001.
 */

public interface HttpApi {
    //     public final static String BASE_URL = "http://blog.nearby.ren";
     public final static String BASE_URL = "http://172.16.1.1:8080/MccServer/";
//    public final static String BASE_URL = BuildConfig.BASE_URL;

//     @GET("http://api.dagoogle.cn/music/search")
//     Observable<HttpResultBean<List<MusicBean>>> search(@Query("keyword") String keyword);

    public final static String BASE_URL2 = "http://39.108.187.40:9002/";
    @GET("https://api.douban.com/v2/music/search")
    Observable<HttpResultBean<List<MusicBean>>> search(@Query("q") String keyword);


    @FormUrlEncoded
    @POST(FromAndAction.Action.REGISTERED)
    Observable<HttpsResultBean<String>> registered(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST(FromAndAction.Action.LOGIN)
    Observable<HttpsResultBean<UserBean>> login(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST(FromAndAction.Action.COLLECTION)
    Observable<HttpsResultBean<List<CollectionBean>>> collection(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST(FromAndAction.Action.TESTTOKEN)
    Observable<HttpsResultBean<List<CollectionBean>>> testToken(@FieldMap Map<String, Object> map);

    @GET("http://imtt.dd.qq.com/16891/A531E981E64921832EA744E028AD884F.apk")
    Call<ResponseBody> dowApk();

    @GET
    Call<ResponseBody> onlineUpload(@Url String url);


    @GET("https://api.apiopen.top/EmailSearch")
    Observable<HttpResultBean4<APIBean>> testAPI(@Query("number") String number);
}
