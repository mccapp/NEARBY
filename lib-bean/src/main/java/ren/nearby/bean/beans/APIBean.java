package ren.nearby.bean.beans;

import java.util.List;

/**
 * Created by Administrator on 2019/1/21 0021.
 */

public class APIBean {
    private int code;
    private String message;
    private List<Result> result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {


    }
}
