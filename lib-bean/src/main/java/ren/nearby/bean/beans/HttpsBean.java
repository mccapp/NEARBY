package ren.nearby.bean.beans;

/**
 * Created by Administrator on 2017/6/15 0015.
 */

public class HttpsBean {
    private int id;
    private String title;
    private String content;
    private String releaseSubTitle;
    private int infoType;
    private int userId;
    private String releaseInfoUrl;
    private int releaseInfoId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getReleaseSubTitle() {
        return releaseSubTitle;
    }

    public void setReleaseSubTitle(String releaseSubTitle) {
        this.releaseSubTitle = releaseSubTitle;
    }

    public int getInfoType() {
        return infoType;
    }

    public void setInfoType(int infoType) {
        this.infoType = infoType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getReleaseInfoUrl() {
        return releaseInfoUrl;
    }

    public void setReleaseInfoUrl(String releaseInfoUrl) {
        this.releaseInfoUrl = releaseInfoUrl;
    }

    public int getReleaseInfoId() {
        return releaseInfoId;
    }

    public void setReleaseInfoId(int releaseInfoId) {
        this.releaseInfoId = releaseInfoId;
    }

    @Override
    public String toString() {
        return "HttpsBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", releaseSubTitle='" + releaseSubTitle + '\'' +
                ", infoType=" + infoType +
                ", userId=" + userId +
                ", releaseInfoUrl='" + releaseInfoUrl + '\'' +
                ", releaseInfoId=" + releaseInfoId +
                '}';
    }
}
