package ren.nearby.bean.beans;



/**
 * Created by Administrator on 2016/7/20.
 * 登录
 */
public class LoginBean {
    //冻结金额
    private float freeze;
    //头像
    private String headImg;
    //加密的userId
    private String userId;
    //账户总额
    private float accountAmount;
    //用户名
    private String username;
    //可用余额
    private float availableBalance;
    //是否已经开通托管账户（返回true或者false）
    private boolean isIps;


    public LoginBean(String username,boolean isIps) {
        this.username = username;
        this.isIps = isIps;
    }

    public float getFreeze() {
        return freeze;
    }

    public void setFreeze(float freeze) {
        this.freeze = freeze;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(float accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(float availableBalance) {
        this.availableBalance = availableBalance;
    }

    public boolean isIps() {
        return isIps;
    }

    public void setIps(boolean ips) {
        isIps = ips;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "freeze='" + freeze + '\'' +
                ", headImg='" + headImg + '\'' +
                ", userId='" + userId + '\'' +
                ", accountAmount=" + accountAmount +
                ", username='" + username + '\'' +
                ", availableBalance=" + availableBalance +
                ", isIps=" + isIps +
                '}';
    }
}
