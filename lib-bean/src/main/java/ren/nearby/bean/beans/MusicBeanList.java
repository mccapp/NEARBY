package ren.nearby.bean.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/9/7 0007.
 */

public class MusicBeanList implements Serializable {
    public List<MusicBean> data;

    public MusicBeanList(List<MusicBean> data) {
        this.data = data;
    }
}
