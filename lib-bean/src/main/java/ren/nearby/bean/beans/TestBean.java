package ren.nearby.bean.beans;

/**
 * Created by Angel on 2017/02/27.
 */

public class TestBean {
    private String custId;
    private String custLogin;

    private String token;
    private String custName;

    private String RSPMSG;
    private String RSPCOD;

    public String getRSPMSG() {
        return RSPMSG;
    }

    public void setRSPMSG(String RSPMSG) {
        this.RSPMSG = RSPMSG;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustLogin() {
        return custLogin;
    }

    public void setCustLogin(String custLogin) {
        this.custLogin = custLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getRSPCOD() {
        return RSPCOD;
    }

    public void setRSPCOD(String RSPCOD) {
        this.RSPCOD = RSPCOD;
    }
}
