package ren.nearby.bean.beans;

/**
 * Created by Administrator on 2018/11/27 0027.
 */

public class VersionBean {


    int inversion;
    int appType;
    int update;
    String remark;
    int id;
    String ambientType;
    String url;
    int updateType;

    public int getInversion() {
        return inversion;
    }

    public void setInversion(int inversion) {
        this.inversion = inversion;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public int getUpdate() {
        return update;
    }

    public void setUpdate(int update) {
        this.update = update;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmbientType() {
        return ambientType;
    }

    public void setAmbientType(String ambientType) {
        this.ambientType = ambientType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUpdateType() {
        return updateType;
    }

    public void setUpdateType(int updateType) {
        this.updateType = updateType;
    }
}
