package ren.nearby.bean.beans;

import java.io.Serializable;

/**
 * @author: lr
 * @created on: 2019-06-08 20:56
 * @description:
 */
public class ARouterBean implements Serializable {

    private String name;
    private int age;
    public ARouterBean() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
