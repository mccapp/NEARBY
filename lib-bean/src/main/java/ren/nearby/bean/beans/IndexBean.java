package ren.nearby.bean.beans;

/**
 * Created by Administrator on 2016/4/28.
 */
public class IndexBean      {
    String id;
    String type;
    String cid;
    String qid;
    String tname;
    double total;
    double price;
    double money;
    String is_end;
    String image_title;
    String image;
    String add_time;
    String summary;
    String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getIs_end() {
        return is_end;
    }

    public void setIs_end(String is_end) {
        this.is_end = is_end;
    }

    public String getImage_title() {
        return image_title;
    }

    public void setImage_title(String image_title) {
        this.image_title = image_title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "IndexBean{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", cid='" + cid + '\'' +
                ", qid='" + qid + '\'' +
                ", tname='" + tname + '\'' +
                ", total=" + total +
                ", price=" + price +
                ", money=" + money +
                ", is_end='" + is_end + '\'' +
                ", image_title='" + image_title + '\'' +
                ", image='" + image + '\'' +
                ", add_time='" + add_time + '\'' +
                ", summary='" + summary + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
