package ren.nearby.bean.beans;

/**
 * Created by Administrator on 2017/11/8 0008.
 */

public class FromAndAction {
    public static class From {
        public static final int LOGIN = 0x001;
        public static final int REGISTERED = 0x002;
        public static final int COLLECTION = 0x003;

        public static final int TESTTOKEN = 0x004;
        public static final int TESTDOWAPK = 0x005;


    }

    public static class Action {
        public static final String LOGIN = "v1.0/login";
        public static final String REGISTERED = "v1.0/registered";
        public static final String COLLECTION = "v1.0/collection";
        public static final String TESTTOKEN = "v1.0/testToken";
    }
}
