package ren.nearby.bean.beans;

/**
 * Created by Administrator on 2017/11/8 0008.
 */

public class CollectionBean {
    private String id ;
    private String releaseSubTitle;
    private String releaseInfoUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReleaseSubTitle() {
        return releaseSubTitle;
    }

    public void setReleaseSubTitle(String releaseSubTitle) {
        this.releaseSubTitle = releaseSubTitle;
    }

    public String getReleaseInfoUrl() {
        return releaseInfoUrl;
    }

    public void setReleaseInfoUrl(String releaseInfoUrl) {
        this.releaseInfoUrl = releaseInfoUrl;
    }
}
