1.build.gradle版本管理
2.

移动接口安全规范
接口结构
接口一律采用https,如：8080 443
1.https://api.nearby.ren/version/controller/action
请求参数
字段名             变量名             类型          描述
业务动作           action             String(32)    操作，如:add
业务内容           content            String(32)    业务数据，json对象
控制模块           controller         String(32)    业务模块，如：user
随机字符串         nonceStr           String(32)    随机字符串, 不长于32位。推荐随机数生成算法
签名               sign               String(32)    签名，详见签名生成算法
签名类型           sign_type          String(32)    签名类型, 如HMAC-SHA256或MD5, 默认为MD5
服务id             serviceId          String(32)    用于服务器快速定位用户的临时数据, 在app第一次注册的时候获取

返回结果
字段名             变量名             类型          描述
业务动作           action             String(32)    操作，如:add
业务内容           content            String(32)    业务数据，json对象
控制模块           controller         String(32)    业务模块，如：user
返回状态码         returnCode         String(16)    SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
返回信息           returnMsg          String(128)   返回信息，如非空，为错误原因签名失败参数格式校验错误

签名算法
第一步
将所有的数据（剔除签名字段），按照key值的ASCII码从小到大排序，直接输出成json字符串stringA.
*参数名ASCII码从小到大排序(字典序)
*参数名区分大小写
第二步
在stringA的后面拼接和服务器约定的密钥，使用密码.得到stringATemp，再对stringATemp进行MD5运算，将得到的字符串全部转换成大写，得到sign的值

假设待提交的数据：
{
    "action": "add",
    "nonceStr": "ibuaiVcKdpRxkhJA",
    "content": {
        "userName": "111",
        "passWord":"123"
    },
    "controller": "user",
}
第一步之后得到字符串

1.stringA = {"action": "add","content": {"userName": "111","passWord":"123" },"controller": "user","nonceStr":"ibuaiVcKdpRxkhJA"}

第二步拼接key
stringATemp = stringA + password
sign=MD5(stringSignTemp).toUpperCase()="9A0A8659F005D6984697E2CA0A9CF3B7"

最终得到的发出去的数据为：
{
    "action": "add",
    "nonceStr": "ibuaiVcKdpRxkhJA",
    "content": {
        "userName": "111",
        "passWord":"123"
    },
    "controller": "user",
    "sign": "9A0A8659F005D6984697E2CA0A9CF3B7"
}

随机数
主要保证签名不可预测

java
public static String random(){
        return UUID.randomUUID().toString().replace("-","");
}

OC
+ (NSString *)random {
    return [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

ANDROID

实现算法的第一步和第二步 (使用fastjson, 可自动实现key值排序)
/**
 * 计算jsonObject的json值, 并添加key
 *
 * @param jsonObject
 * @param uuid, 和服务器约定的密钥, 可用password代替
 * @return
 */
public static String encode(JSONObject jsonObject, String uuid) {
    StringBuilder sb = new StringBuilder();
    sb.append(jsonObject.toJSONString());
    sb.append(uuid);
    String md5 = MD5.stringMD5(sb.toString());
    return md5;
}

调用

    /**
    * 对发送的json加密
    *
    * @param jsonObject
    * @param uuid
    * @param serviceId
    * @return
    */
   public static JSONObject encryptJSON(JSONObject jsonObject, String uuid, String serviceId) {
       jsonObject.put("sign", encode(jsonObject, uuid));
       jsonObject.put("serviceId", serviceId);
       return jsonObject;
   }

校验服务器返回数据

    /**
     * 检查客户的提交json是否正确
     *
     * @param jsonObject
     * @param uuid, 和客户端约定的密钥, 一般可用用户密码
     * @return
     */
    public static boolean checkEncrypt(JSONObject jsonObject, String uuid) {
        String checkNumService = jsonObject.getString("sign");
        jsonObject.remove("sign");
        return checkNumService != null && checkNumService.equals(encode(jsonObject, uuid));
    }

Android Studio3.0apk安装包损坏解决方案
在AndroidManifest.xml中添加 buildToolsVersion版本为27.0.X， compileSdkVersion改为27就可以。如下：(26不行的)

多环境配置
    productFlavors {
        //开发环境
        idevelop {
            buildConfigField "int", "ENV_TYPE", "1"
            applicationId 'www.nearby.ren.develop'
            manifestPlaceholders = [
                    app_name: "develop",
                    app_icon: "@drawable/icon_develop"
            ]
        }
        //测试环境
        itest {
            buildConfigField "int", "ENV_TYPE", "2"
            applicationId 'www.nearby.ren.test'
            manifestPlaceholders = [
                    app_name: "test",
                    app_icon: "@drawable/icon_test"
            ]
        }
        //生产环境
        iproduct {
            buildConfigField "int", "ENV_TYPE", "3"
            applicationId 'www.nearby.ren.product'
            manifestPlaceholders = [
                    app_name: "product",
                    app_icon: "@drawable/icon_product"
            ]
        }

        //友盟渠道发包
      /*  yingyongbao {}
        huawei {}
        baidu {}
        xiaomi {}
        qh360 {}*/
    }

1.多环境打包命令：
  01.gradle assemble

多渠道配置
在App 目录下新建一个channel(txt)文件
Qh360 #360
Yyb #应用宝
Wdj #豌豆荚
Lenovo #联想
Xiaomi #小米
Huawei #华为
Meizhu #魅族

buildscript {
    dependencies {
        classpath 'com.meituan.android.walle:plugin:1.1.4'
    }
}
并在当前App的 build.gradle文件中apply这个插件，并添加上用于读取渠道号的AAR
apply plugin: 'walle'
dependencies { compile 'com.meituan.android.walle:library:1.1.4'}




  walle {
      // 指定渠道包的输出路径
      apkOutputFolder = new File("${project.buildDir}/outputs/channels");
      // 定制渠道包的APK的文件名称
      apkFileNameFormat = '${appName}-${packageName}-${channel}-${buildType}-v${versionName}-${versionCode}-${buildTime}.apk';
      // 渠道配置文件
      channelFile = new File("${project.getProjectDir()}/channel")
  }

apkOutputFolder：指定渠道包的输出路径， 默认值为new File("${project.buildDir}/outputs/apk")

apkFileNameFormat：定制渠道包的APK的文件名称, 默认为'${appName}-${buildType}-${channel}.apk'

   projectName - 项目名字
     appName - App模块名字
     packageName - applicationId (App包名packageName)
     buildType - buildType (release/debug等)
     channel - channel名称 (对应渠道打包中的渠道名字)
     versionName - versionName (显示用的版本号)
     versionCode - versionCode (内部版本号)
     buildTime - buildTime (编译构建日期时间)
     fileSHA1 - fileSHA1 (最终APK文件的SHA1哈希值)
     flavorName - 编译构建 productFlavors 名


 多渠道打包命令：
 生成单个渠道包: ./gradlew clean assembleReleaseChannels -PchannelList=meituan

 生成多个渠道包: ./gradlew clean assembleReleaseChannels -PchannelList=meituan,dianping

 生成所有渠道信息 ./gradlew clean assembleReleaseChannels



 butterknife 组件化开发 集成模式 将module R 改成 R2 组件开发模式 将module R2 改成 R
 开发组件
https://blog.csdn.net/xj032w2j4ccjhow8s8/article/details/79004830



tinker和andResguard整合操作的步骤
1.上线前，先执行reguardRelease任务，打出资源混淆过的Apk[build\outputs\apk\release\app-release]，生成在备份目录/bakApk/reguard-MM-dd-HH-mm-ss/目录中，同时，需要将该文件夹备份，作为下次热更新的基准包；
上线后，出现bug，需要打补丁
2.在修复完bug的时候，先执行reguardRelease任务，生成新的进行过资源混淆的apk,
3.将备份好的基准包放置在app模块下的/build/bakApk/目录下，修改app模块下build.gradle中基准包目录，如下：
def baseApkDir(){
    return "resguard-0119-11-29-43"  例如：上线基包是resguard-0119-11-29-42 ，此处则更改为 return "resguard-0119-11-29-42"
}
4.执行buildTinkerPatchRelease任务，找到patch目录下的补丁包，[build\outputs\patch\release]patch_signed_7zip 文件。



///优化开发教程
https://www.androidos.net.cn/articles
https://blog.csdn.net/sk719887916/article/details/51700659
//封装http
https://github.com/leishengwei/WeHttp