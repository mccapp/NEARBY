package www.nearby.ren.di;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ren.nearby.home.ANearbyMain;
import ren.nearby.home.GuidePages;
import ren.nearby.home.HomeActivity;
import ren.nearby.home.NearbyFragmentDynamic;
import ren.nearby.home.NearbyFragmentHome;
import ren.nearby.home.NearbyFragmentMe;
import ren.nearby.home.NearbyFragmentMessage;
import ren.nearby.home.NearbyFragmentPrivateLetter;
import ren.nearby.home.NearbyFragmentRelatedToMe;
import ren.nearby.home.SplashAct;
import ren.nearby.home.mvp.FragmentMainModule;
import ren.nearby.home.mvp.FragmentMeModule;


import ren.nearby.lib.http.scope.ActivityScoped;
import ren.nearby.lib.http.scope.BaseActivityComponent;
import ren.nearby.lib.http.scope.BaseFragmentComponent;
import ren.nearby.lib.http.scope.BaseFragmentV4Component;
import ren.nearby.main.CompressAct;
import ren.nearby.main.NWelcome;
import ren.nearby.main.PlatformAct;
import ren.nearby.main.TinkerAct;
import ren.nearby.main.login.LoginAct;
import ren.nearby.main.music.MusicAct;
import ren.nearby.main.tdagger2.TemplateDaggerAct;
import ren.nearby.main.tdagger2.TemplateDaggerAct2;
import ren.nearby.main.tdagger2.TemplateDaggerAct2Module;
import ren.nearby.main.tdagger2.TemplateDaggerActModule;
import ren.nearby.main.web.WebAct;

import ren.nearby.textview.StoragePathAct;
import ren.nearby.textview.TextViews;
import ren.nearby.textview.di.ActModule;
import ren.nearby.view.NWelcomeV;
import ren.nearby.view.WebViewProceIndep;
import www.nearby.ren.AOPActivity;


/**
 * 配置所有activity注入
 * Created by Administrator on 2018/5/3 0003.
 */


@Module(subcomponents = {
        // 注入 activity
        BaseActivityComponent.class,
        //android.app.Fragment 兼容的最低版本是android:minSdkVersion="11" 即3.0版
        BaseFragmentComponent.class,
        //android.support.v4.app.Fragment 兼容的最低版本是android:minSdkVersion="4" 即1.6版
        BaseFragmentV4Component.class
})
public abstract class AllActivitysModule {


    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerActModule.class)
    abstract LoginAct loginAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerActModule.class)
    abstract AOPActivity aopActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerActModule.class)
    abstract TemplateDaggerAct templateDaggerAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract TemplateDaggerAct2 templateDaggerAct2();


    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract MusicAct musicAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract TinkerAct tinkerAct();
    @ActivityScoped
    @ContributesAndroidInjector(modules =  TemplateDaggerAct2Module.class)
    abstract WebViewProceIndep webViewProceIndep();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract CompressAct compressAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract NWelcome nWelcome();
    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract NWelcomeV nWelcomeV();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract WebAct webAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TemplateDaggerAct2Module.class)
    abstract PlatformAct platformAct();
    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract GuidePages guidePages();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract StoragePathAct storagePathAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract SplashAct splashAct();



    @ActivityScoped
    @ContributesAndroidInjector(modules = ActModule.class)
    abstract TextViews textViews();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMainModule.class)
    abstract ANearbyMain aNearbyMain();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMainModule.class)
    abstract HomeActivity homeActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentHome nearbyFragmentHome();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentMessage nearbyFragmentMessage();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentMe nearbyFragmentMe();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentDynamic nearbyFragmentDynamic();


    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentPrivateLetter nearbyFragmentPrivateLetter();




    @ActivityScoped
    @ContributesAndroidInjector(modules = FragmentMeModule.class)
    abstract NearbyFragmentRelatedToMe nearbyFragmentRelatedToMe();

}
