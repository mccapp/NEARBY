package www.nearby.ren;

/**
 * Created by Administrator on 2018/1/11 0011.
 */

public class APPHelperBuilder {
    private final String versionCode;
    private final String versionName;
    private final String versionLog;
    private final String take;
    private final String learn;
    private final String run;


    private APPHelperBuilder(Builder builder) {
        this.versionCode = builder.versionCode;
        this.versionName = builder.versionCode;
        this.versionLog = builder.versionCode;
        this.take = builder.take;
        this.learn = builder.learn;
        this.run = builder.run;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public String getVersionLog() {
        return versionLog;
    }

    public String getTake() {
        return take;
    }

    public String getLearn() {
        return learn;
    }

    public String getRun() {
        return run;
    }

    public static class Builder {
        private final String versionCode;
        private final String versionName;
        private final String versionLog;
        private String take;
        private String learn;
        private String run;

        public Builder(String versionCode, String versionName, String versionLog) {
            this.versionCode = versionCode;
            this.versionName = versionName;
            this.versionLog = versionLog;

        }

        public Builder take(String take) {
            this.take = take;
            return this;
        }

        public Builder learn(String learn) {
            this.learn = learn;
            return this;
        }

        public Builder run(String run) {
            this.run = run;
            return this;
        }

        public APPHelperBuilder build() {
            return new APPHelperBuilder(this);
        }

    }
}
