package www.nearby.ren;

import android.view.View;
import android.widget.Button;

import com.orhanobut.logger.Logger;

import ren.nearby.aop.AOPAnnotation;
import ren.nearby.lib.ui.base.BaseActivity;

/**
 * /**可行的操作，
 * Created by Administrator on 2018/9/30 0030.
 */

public class AOPActivity extends BaseActivity {
    Button bt;
    @Override
    public int getLayoutRes() {
        return R.layout.aop_activity;
    }

    @Override
    public void initView() {
        bt = findViewById(R.id.bt);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test2( v);
            }
        });
    }
//    @APPAnnotation("测试来了")//可行
    @AOPAnnotation("测试来了")//可行
    public void test2(View v) {
        Logger.e("Hello, I am CSDN_LQR");
    }
}
