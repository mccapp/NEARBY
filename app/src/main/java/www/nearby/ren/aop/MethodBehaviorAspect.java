package www.nearby.ren.aop;

import android.view.View;

import com.orhanobut.logger.Logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import java.util.Calendar;
import www.nearby.ren.R;


/**
 * Created by Administrator on 2018/9/30 0030.
 */

@Aspect
public class MethodBehaviorAspect {


    private static final String TAG = "aspect_aby";

    @Pointcut("execution(@www.nearby.ren.aop.APPAnnotation * *(..))")
    public void vehavior() {
    }

  /*  @Around("vehavior()")
    public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger.e("来了...");
        Logger.e("@Around");
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();
        APPAnnotation behaviorTrace = methodSignature.getMethod().getAnnotation(APPAnnotation.class);
        String value = behaviorTrace.value();
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long duration = System.currentTimeMillis() - start;
        Logger.e(String.format("%s类中%s方法执行%s功能,耗时：%dms)", className, methodName, value, duration));

        return result;
    }*/
  public static final int MIN_CLICK_DELAY_TIME = 600;//间隔时间600ms

    @Around("vehavior()")
    public void aroundJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {

        View view = null;
        for (Object arg : joinPoint.getArgs()) {
            if (arg instanceof View) view = (View) arg;
            if (view != null) {
                Object tag = view.getTag(R.id.bt);
                long lastClickTime = ((tag != null) ? (long) tag : 0);
                Logger.e("SingleClickAspect lastClickTime:" + lastClickTime);
                long currentTime = Calendar.getInstance().getTimeInMillis();
                if(currentTime - lastClickTime >MIN_CLICK_DELAY_TIME){
                    view.setTag(R.id.bt,currentTime);
                    Logger.e("SingleClickAspect currentTime:" + currentTime);
                    joinPoint.proceed();
                }
            }
        }
    }


}
