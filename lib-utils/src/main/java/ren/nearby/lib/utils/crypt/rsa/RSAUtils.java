package ren.nearby.lib.utils.crypt.rsa;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;

/**
 * Created by Administrator on 2017/6/12 0012.
 * 非对称加密密钥
 */


public class RSAUtils {
    public static final String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApxMKgsXzAFnFmn0NhWlN2LkeskZ8ahZPX6c3seYH7H7eOPthlJs+IKP0nVedpekOBY2sO4mOD6gTExohFE/ClWuCfRwtDnnuCQM8YBJ/5lO5jtTJ+DS5KbjTROWxG9g+pjwb9huRBTJg0Nk/iSIbg8Mer83TXn30wGIet/9ZZmFNG8Jk2edOwV8tiaw8tdZnhMv75xJaTlDMlL1y62XVfusESdHqLugs+rLlxAq/6iA8yOIKIulfHvuyDwv0/BrkTszxy3BlNgI8Xw1L/jE1fRr5JxevSXjzHzlyb7f/NE4gAmI6n7SEmkk6bk3SpXi0uWBHjheJ/rUrd35HOf6EEwIDAQAB";
//      public static    String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCnEwqCxfMAWcWafQ2FaU3YuR6yRnxqFk9fpzex5gfsft44+2GUmz4go/SdV52l6Q4Fjaw7iY4PqBMTGiEUT8KVa4J9HC0Oee4JAzxgEn/mU7mO1Mn4NLkpuNNE5bEb2D6mPBv2G5EFMmDQ2T+JIhuDwx6vzdNeffTAYh63/1lmYU0bwmTZ507BXy2JrDy11meEy/vnElpOUMyUvXLrZdV+6wRJ0eou6Cz6suXECr/qIDzI4goi6V8e+7IPC/T8GuROzPHLcGU2AjxfDUv+MTV9GvknF69JePMfOXJvt/80TiACYjqftISaSTpuTdKleLS5YEeOF4n+tSt3fkc5/oQTAgMBAAECggEAK9fVVEf54sesEURrvezKXusFYScaEwTTw3gwJ2EEB02rpyxgiQzMGuirknahih5EbbSl0+HfA8DJcKPUtm3P1EIoNgik0j2w93B/8X4pThn+gbrZXWcIiChWWKgl9/ahSiJP/B9zyPdVbLj18Vif+83CScQriYpoJewZv5MktLa7YphvRxmshhdo4oVBTr2bsowhScbwqvmimAHLfS2YkX5bCqFT/2szNp7iPjmaX8smyMGw5gjuerR+2Im9CcnMPQpPI2LJNPZXbwWwSh0MU3JlZEQiJnEf3Bsblu2CNyoNJPciGXDfWxVS1gI2mE9K80zbF6xw9s1452LTJMIIIQKBgQDTDHq5N7wTz+e78OhZNrvp1Ge4O+lIYXd9+3iEyUnLiK04SIIZmBa+w7XqNYP5Tkf7xnF0RJcJQ/aR+7PY9ecWYpXtUacy4/NtxmhGlS4/LJ+9ooSaBDxM6s7tZj+5VKGkYf38VRzVpYlTpBp/jvyl/N/omzYbDAPBq9XuhUVK2QKBgQDKqNdJg/eNtbelIZcWBI2E68mEgtFE3U3OltJ/lFMHkGGiErFGgCUEj37j17Sk0+kQ3xCb8h8uah0/FNqSZn+wpTuhGuCeELAKU3/3c1oYVcyz9gnc4DSMDC5jEV9eEUSjLUsKkh4WUj/6Jp+fQ4eXBMnAFVUdFtuxO6FIbW86ywKBgEkOQtUmo+Y/y7nVFLj9J2Mz8YtmwcQaaSClEfz1KKRZ9YxMHSz65rS2Tpx6VerQ3D4dXVyf+aBtdEboWEM8K/glvDxgTX5iRfuz+JimP1IDAnDEdx5RZQZDKcon4NsonqOmgCL+23JhUP4xCjMpre9Pb7q3L62BJTO/ZdihLXFRAoGAT4V5FMvHs79+VDmeA3G95k5vQKPkYdVE1q+v4YfeHLW22dXj4foejG+SJu1ZYOQWLBybc9XKDnhtCXexXIkDydjMUScuxFdf/G1PZVXKIIpqTKNas2T2uY1+FjmW5l54RF9ddsRnMfMMT6ggMmpZJhUm5Ux5nXR+uEjhvH3Ui4kCgYAHcB1QKimyDErP6nw6TvCMFPy7anThlgTxYWc5qmznj8lRddk3dYOkOMzDYYuiY22FAtw7AW3+gv+nj2M0Rz4aF7wsxsmqbyRAAFqxOKQpLQn/zyLTVLEdYcxWVwf0N36uUVBsjMRbLC3FLOKrFnTpl6n/H4L7taLD2zbz9oqLug==";

    public static final String RSA = "RSA";//非对称加密密钥算法
    public static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";//加密填充方式
    public static final int DEFAULT_KEY_SIZE = 2048;//密钥默认长度
    public static final byte[] DEFAULT_SPLIT = "#RART#".getBytes();//当要加密的内容超过bufferSize，则采用partSplit进行分块加密
    public static final int DEFAULT_BUFFERSIZE = (DEFAULT_KEY_SIZE / 8) - 11;//当前密钥支持加密的最大字节数


    public static KeyPair generateRSAKeyPair(int keyLength) {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA);
            kpg.initialize(keyLength);
            return kpg.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 用公钥对字符串进行加密
     *
     * @param data
     * @param publicKey
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPublicKey(byte[] data, byte[] publicKey) throws Exception {
        //得到公钥
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKey);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        PublicKey keyPublic = kf.generatePublic(keySpec);

        //加密数据
        Cipher cp = Cipher.getInstance(ECB_PKCS1_PADDING);
        cp.init(Cipher.ENCRYPT_MODE, keyPublic);
        return cp.doFinal(data);
    }

    /**
     * 私钥加密
     *
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPrivateKey(byte[] data, byte[] privateKey) throws Exception {
        //得到公钥
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKey);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        PrivateKey keyPrivate = kf.generatePrivate(keySpec);

        //加密数据
        Cipher cp = Cipher.getInstance(ECB_PKCS1_PADDING);
        cp.init(Cipher.ENCRYPT_MODE, keyPrivate);
        return cp.doFinal(data);
    }

    /**
     * 公钥解密
     *
     * @param data
     * @param publicKey
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPublicKey(byte[] data, byte[] publicKey) throws Exception {
        //得到公钥
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKey);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        PublicKey keyPublic = kf.generatePublic(keySpec);

        //数据解密
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, keyPublic);
        return cipher.doFinal(data);
    }

    /***
     * 私钥解密
     *
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPrivateKey(byte[] data, byte[] privateKey) throws Exception {
        //得到私钥
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKey);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        PrivateKey keyPrivate = kf.generatePrivate(keySpec);

        //数据加密
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, keyPrivate);
        return cipher.doFinal(data);
    }

    /**
     * 用公钥对字符串进行分段加密
     *
     * @param data
     * @param publicKey
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPublicKeyForSpilt(byte[] data, byte[] publicKey) throws Exception {
        int dataLen = data.length;
        if (dataLen <= DEFAULT_BUFFERSIZE) {
            return encryptByPublicKey(data, publicKey);
        }
        List<Byte> allBytes = new ArrayList<Byte>(2048);
        int bufIndex = 0;
        int subDataLoop = 0;
        byte[] buf = new byte[DEFAULT_BUFFERSIZE];
        for (int i = 0; i < dataLen; i++) {
            buf[bufIndex] = data[i];
            if (++bufIndex == DEFAULT_BUFFERSIZE || i == dataLen - 1) {
                subDataLoop++;
                if (subDataLoop != 1) {
                    for (byte b : DEFAULT_SPLIT) {
                        allBytes.add(b);
                    }
                }
                byte[] encryptBytes = encryptByPublicKey(buf, publicKey);
                for (byte b : encryptBytes) {
                    allBytes.add(b);
                }
                bufIndex = 0;
                if (i == dataLen - 1) {
                    buf = null;
                } else {
                    buf = new byte[Math.min(DEFAULT_BUFFERSIZE, dataLen - i - 1)];
                }
            }
        }
        byte[] bytes = new byte[allBytes.size()];
        {
            int i = 0;
            for (Byte b : allBytes) {
                bytes[i++] = b.byteValue();
            }
        }
        return bytes;
    }

    /**
     * 私钥分段加密
     *
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPrivateKeyForSpilt(byte[] data, byte[] privateKey) throws Exception {
        int dataLen = data.length;
        if (dataLen <= DEFAULT_BUFFERSIZE) {
            return encryptByPrivateKey(data, privateKey);
        }
        List<Byte> allBytes = new ArrayList<Byte>(2048);
        int bufIndex = 0;
        int subDataLoop = 0;
        byte[] buf = new byte[DEFAULT_BUFFERSIZE];
        for (int i = 0; i < dataLen; i++) {
            buf[bufIndex] = data[i];
            if (++bufIndex == DEFAULT_BUFFERSIZE || i == dataLen - 1) {
                subDataLoop++;
                if (subDataLoop != 1) {
                    for (byte b : DEFAULT_SPLIT) {
                        allBytes.add(b);
                    }
                }
                byte[] encryptBytes = encryptByPrivateKey(buf, privateKey);
                for (byte b : encryptBytes) {
                    allBytes.add(b);
                }
                bufIndex = 0;
                if (i == dataLen - 1) {
                    buf = null;
                } else {
                    buf = new byte[Math.min(DEFAULT_BUFFERSIZE, dataLen - i - 1)];
                }
            }
        }
        byte[] bytes = new byte[allBytes.size()];
        {
            int i = 0;
            for (Byte b : allBytes) {
                bytes[i++] = b.byteValue();
            }
        }
        return bytes;
    }

    /**
     * 公钥分段解密
     *
     * @param encrypted
     * @param publicKey
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPublicKeyForSpilt(byte[] encrypted, byte[] publicKey) throws Exception {
        int splitLen = DEFAULT_SPLIT.length;
        if (splitLen <= 0) {
            return decryptByPublicKey(encrypted, publicKey);
        }
        int dataLen = encrypted.length;
        List<Byte> allBytes = new ArrayList<Byte>(1024);
        int latestStartIndex = 0;
        for (int i = 0; i < dataLen; i++) {
            byte bt = encrypted[i];
            boolean isMatchSplit = false;
            if (i == dataLen - 1) {
                // 到data的最后了
                byte[] part = new byte[dataLen - latestStartIndex];
                System.arraycopy(encrypted, latestStartIndex, part, 0, part.length);
                byte[] decryptPart = decryptByPublicKey(part, publicKey);
                for (byte b : decryptPart) {
                    allBytes.add(b);
                }
                latestStartIndex = i + splitLen;
                i = latestStartIndex - 1;
            } else if (bt == DEFAULT_SPLIT[0]) {
                // 这个是以split[0]开头
                if (splitLen > 1) {
                    if (i + splitLen < dataLen) {
                        // 没有超出data的范围
                        for (int j = 1; j < splitLen; j++) {
                            if (DEFAULT_SPLIT[j] != encrypted[i + j]) {
                                break;
                            }
                            if (j == splitLen - 1) {
                                // 验证到split的最后一位，都没有break，则表明已经确认是split段
                                isMatchSplit = true;
                            }
                        }
                    }
                } else {
                    // split只有一位，则已经匹配了
                    isMatchSplit = true;
                }
            }
            if (isMatchSplit) {
                byte[] part = new byte[i - latestStartIndex];
                System.arraycopy(encrypted, latestStartIndex, part, 0, part.length);
                byte[] decryptPart = decryptByPublicKey(part, publicKey);
                for (byte b : decryptPart) {
                    allBytes.add(b);
                }
                latestStartIndex = i + splitLen;
                i = latestStartIndex - 1;
            }
        }
        byte[] bytes = new byte[allBytes.size()];
        {
            int i = 0;
            for (Byte b : allBytes) {
                bytes[i++] = b.byteValue();
            }
        }
        return bytes;
    }

    /**
     * 私钥分段解密
     *
     * @param encrypted
     * @param privateKey
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPrivateKeyForSpilt(byte[] encrypted, byte[] privateKey) throws Exception {
        int splitLen = DEFAULT_SPLIT.length;
        if (splitLen <= 0) {
            return decryptByPrivateKey(encrypted, privateKey);
        }
        int dataLen = encrypted.length;
        List<Byte> allBytes = new ArrayList<Byte>(1024);
        int latestStartIndex = 0;
        for (int i = 0; i < dataLen; i++) {
            byte bt = encrypted[i];
            boolean isMatchSplit = false;
            if (i == dataLen - 1) {
                // 到data的最后了
                byte[] part = new byte[dataLen - latestStartIndex];
                System.arraycopy(encrypted, latestStartIndex, part, 0, part.length);
                byte[] decryptPart = decryptByPrivateKey(part, privateKey);
                for (byte b : decryptPart) {
                    allBytes.add(b);
                }
                latestStartIndex = i + splitLen;
                i = latestStartIndex - 1;
            } else if (bt == DEFAULT_SPLIT[0]) {
                // 这个是以split[0]开头
                if (splitLen > 1) {
                    if (i + splitLen < dataLen) {
                        // 没有超出data的范围
                        for (int j = 1; j < splitLen; j++) {
                            if (DEFAULT_SPLIT[j] != encrypted[i + j]) {
                                break;
                            }
                            if (j == splitLen - 1) {
                                // 验证到split的最后一位，都没有break，则表明已经确认是split段
                                isMatchSplit = true;
                            }
                        }
                    }
                } else {
                    // split只有一位，则已经匹配了
                    isMatchSplit = true;
                }
            }
            if (isMatchSplit) {
                byte[] part = new byte[i - latestStartIndex];
                System.arraycopy(encrypted, latestStartIndex, part, 0, part.length);
                byte[] decryptPart = decryptByPrivateKey(part, privateKey);
                for (byte b : decryptPart) {
                    allBytes.add(b);
                }
                latestStartIndex = i + splitLen;
                i = latestStartIndex - 1;
            }
        }
        byte[] bytes = new byte[allBytes.size()];
        {
            int i = 0;
            for (Byte b : allBytes) {
                bytes[i++] = b.byteValue();
            }
        }
        return bytes;
    }
}
