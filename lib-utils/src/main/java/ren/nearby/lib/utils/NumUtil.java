package ren.nearby.lib.utils;

import com.orhanobut.logger.Logger;

/**
 * Created by Administrator on 2016/4/23.
 */
public class NumUtil {
    public static String NumberFormat(float f, int m) {
        Logger.e(  "2-f=" + f + "m=" + m);
        return String.format("%.0" + m + "f", f);
    }

    public static float NumberFormatFloat(float f, int m) {
        Logger.e(  "1-f=" + f + "m=" + m);
        String strfloat = NumberFormat(f, m);
        Logger.e(  "3-str float=" +strfloat);
        return Float.parseFloat(strfloat);
    }
}
