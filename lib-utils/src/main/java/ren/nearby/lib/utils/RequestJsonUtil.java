package ren.nearby.lib.utils;

import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

/**
 * Created by Administrator on 2017/10/25 0025.
 */

public class RequestJsonUtil {

    /**
     *
     * @param hashMap
     * @param uuid  和服务器约定的密钥, 可用password代替
     * @return
     */
    public static String sortByJoinKey(Map<String, Object> hashMap, String uuid) {
        //1.map按照key值的ASCII码从小到大排序
        Map<String, Object> map = sortMapByKey(hashMap);
        Logger.e("StringA = " + map.toString());
        StringBuffer sb = new StringBuffer();
        sb.append(new JSONObject(map));
        sb.append(uuid);
        return sb.toString();
    }

    public static String random(){
        return UUID.randomUUID().toString().replace("-","");
    }


    public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String, Object> sortMap = new TreeMap<>(new MapKeyComparator());
        sortMap.putAll(map);
        return sortMap;
    }

    static class MapKeyComparator implements Comparator<String> {

        @Override
        public int compare(String str1, String str2) {

            return str1.compareTo(str2);
        }
    }
}
