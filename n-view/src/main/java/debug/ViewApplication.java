package debug;


import ren.nearby.lib.http.BaseApplication;
import ren.nearby.lib.http.HttpModule;
import ren.nearby.lib.http.data.DaggerTasksRepositoryComponent;
import ren.nearby.lib.http.data.TasksRepositoryComponent;

/**
 * <p>类说明</p>
 *
 * @author nearby 2017/2/15 20:09
 * @version V1.2.0
 * @name HomeApplication
 */
public class ViewApplication extends BaseApplication {

    private String tag = "BMain";

    /*    public static AppComponent2 getAppComponent() {
            return appComponent;
        }

        private static AppComponent2 appComponent;

        */
    private static TasksRepositoryComponent mRepositoryComponent;

    public static TasksRepositoryComponent getTasksRepositoryComponent() {
        return mRepositoryComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
/*        appComponent = DaggerAppComponent
                .builder()
                .httpModule(new HttpModule())
                .build();*/
//        login();
        mRepositoryComponent = DaggerTasksRepositoryComponent
                .builder()
                .httpModule(new HttpModule(this, 0))
                .build();
    }


    @Override
    public String getLogTag() {
        return tag;
    }

    /**
     * 在这里模拟登陆，然后拿到sessionId或者Token
     * 这样就能够在组件请求接口了
     */
//    private void login() {
//        HttpClient client = new HttpClient.Builder()
//                .baseUrl("http://gank.io/api/data/")
//                .url("福利/10/1")
//                .build();
//        client.get(new OnResultListener<String>() {
//
//            @Override
//            public void onSuccess(String result) {
//                Logger.e(result);
//            }
//
//            @Override
//            public void onError(int code, String message) {
//                Logger.e(message);
//            }
//
//            @Override
//            public void onFailure(String message) {
//                Logger.e(message);
//            }
//        });
//    }
}
