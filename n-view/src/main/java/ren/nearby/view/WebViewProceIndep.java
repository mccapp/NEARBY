package ren.nearby.view;

import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.orhanobut.logger.Logger;

import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2017/12/18 0018.
 */

public class WebViewProceIndep extends BaseActivity {
    WebView view_web_view;



    @Override
    public void onClickLeft(View v) {
        super.onClickLeft(v);
        Logger.e("on left");
    }


    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.view_web))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        view_web_view = (WebView)findViewById(R.id.view_web_view);
        view_web_view.loadUrl("http://b.nearby.ren/");
        view_web_view.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl("http://b.nearby.ren/");
                return true;
            }
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_web_view_proce_indep;
    }


}
