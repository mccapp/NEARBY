package ren.nearby.view;

import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;

import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.orhanobut.logger.Logger;

import ren.nearby.lib.http.RxSpUtil;
import ren.nearby.lib.ui.base.BaseActivity;


/**
 * Created by Administrator on 2017/8/4 0004.
 */

public class NWelcomeV extends BaseActivity {

    Button view_btn_result;

    @Override
    public int getLayoutRes() {
        return R.layout.view_welcome;
    }



    @Override
    public void initView() {
        super.initView();
        toolBarBuilder
                .setIconLeft(R.mipmap.btn_back_white)
                .setBack(true)
                .setTitle(getResources().getString(R.string.view_welcome))
                .setBackgroundColor(R.color.colorPrimary)
                .setTitleColor(R.color.white)
                .build();
        view_btn_result =(Button) findViewById(R.id.view_btn_result);
        view_btn_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(104);
                finish();
            }
        });
//        getString();
//        getFloat();

        initVrPaNormalView();
    }





    void getString() {
        RxSpUtil.saveValue(NWelcomeV.this, "view_", "a", "哈哈哈");
        RxSpUtil.getValue(NWelcomeV.this, "", "view_", "a", new RxSpUtil.SpResult<String>() {
            @Override
            public void onResult(String aString) {
                Logger.e("String = " + aString);
            }
        });
    }

    void getFloat() {
        RxSpUtil.saveValue(NWelcomeV.this, "view_", "a_float", 120.0f);
        RxSpUtil.getValue(NWelcomeV.this, 4.0f, "view_", "a_float", new RxSpUtil.SpResult<Float>() {
            @Override
            public void onResult(Float aFloat) {
                Logger.e("Float = " + aFloat);
            }
        });
    }

    VrPanoramaView mVrPanoramaView;
    VrPanoramaView.Options paNormalOptions;

    //初始化VR图片
    private void initVrPaNormalView() {
        mVrPanoramaView = (VrPanoramaView) findViewById(R.id.view_mVrPanoramaView);
        paNormalOptions = new VrPanoramaView.Options();
        paNormalOptions.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER;
//      mVrPanoramaView.setFullscreenButtonEnabled (false); //隐藏全屏模式按钮
        mVrPanoramaView.setInfoButtonEnabled(false); //设置隐藏最左边信息的按钮
        mVrPanoramaView.setStereoModeButtonEnabled(true); //设置隐藏立体模型的按钮
        mVrPanoramaView.setEventListener(new ActivityEventListener()); //设置监听
        //加载本地的图片源
        mVrPanoramaView.loadImageFromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.view_andes1), paNormalOptions);
        //设置网络图片源
//        panoWidgetView.loadImageFromByteArray();
    }

    private class ActivityEventListener extends VrPanoramaEventListener {
        @Override
        public void onLoadSuccess() {//图片加载成功
            Logger.e("图片加载成功");
        }


        @Override
        public void onLoadError(String errorMessage) {//图片加载失败
            Logger.e("图片加载失败");
        }

        @Override
        public void onClick() {//当我们点击了VrPanoramaView 时候触发            super.onClick();
            Logger.e("VrPanoramaView 时候触发 ");
        }

        @Override
        public void onDisplayModeChanged(int newDisplayMode) {
            super.onDisplayModeChanged(newDisplayMode);
        }
    }
}
