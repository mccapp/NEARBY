// MessageSender.aidl
package ren.nearby.view.aidl;


import ren.nearby.view.aidl.data.MessageModel;

interface MessageSender {

 void sendMessage(in MessageModel messageModel);
}
