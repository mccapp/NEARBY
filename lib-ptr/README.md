        //基础默认属性设置
        mPtrFrameLayout.setPinContent(true);//[可选]原生状态[下拉遮盖ui]
        mPtrFrameLayout.setLoadingMinTime(1000);
        mPtrFrameLayout.setDurationToCloseHeader(1500);
        
        //header 字母
        StoreHouseHeader header = new StoreHouseHeader(this);
        header.setPadding(0, PtrLocalDisplay.dp2px(20), 0, PtrLocalDisplay.dp2px(20));
        header.initWithString("Ultra PTR");
        
        // header 字体
        final StoreHouseHeader header = new StoreHouseHeader(this);
        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, 0);
        header.initWithPointList(PtrLocalDisplay.getPointList());       
         
         // header 最新原生
        final MaterialHeader header = new MaterialHeader(this);
        int[] headerColors = getResources().getIntArray(R.array.google_colors);
        header.setColorSchemeColors(headerColors);
        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        header.setPtrFrameLayout(ptrFrameLayout);
        
        
        //footer 字母
        StoreHouseHeader footer = new StoreHouseHeader(this);
        footer.setPadding(0, PtrLocalDisplay.dp2px(20), 0, PtrLocalDisplay.dp2px(20));
        footer.initWithString("Ultra Footer");

        // footer 字体
        final StoreHouseHeader footer = new StoreHouseHeader(this);
        footer.setPadding(0, PtrLocalDisplay.dp2px(15), 0, 0);
        footer.initWithPointList(PtrLocalDisplay.getPointList());
        
        // footer 最新原生
        final MaterialHeader footer = new MaterialHeader(this);
        int[] footerColors = getResources().getIntArray(R.array.google_colors);
        footer.setColorSchemeColors(footerColors);
        footer.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        footer.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        footer.setPtrFrameLayout(ptrFrameLayout);




                ptrFrameLayout = (PtrFrameLayout) view.findViewById(R.id.ptr_frame_layout);

                //header
        //        StoreHouseHeader header = new StoreHouseHeader(this);
        //        header.setPadding(0, PtrLocalDisplay.dp2px(20), 0, PtrLocalDisplay.dp2px(20));
        //        header.initWithString("Ultra PTR");

                // header
        //        final StoreHouseHeader header = new StoreHouseHeader(this);
        //        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, 0);
        //        header.initWithPointList(PtrLocalDisplay.getPointList());

        //         header
        //        final RentalsSunHeaderView header = new RentalsSunHeaderView(this);
        //        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        //        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        //        header.setUp(mPtrFrameLayout);

                //header
                final MaterialHeader header = new MaterialHeader(getActivity());
                int[] headerColors = getResources().getIntArray(R.array.google_colors);
                header.setColorSchemeColors(headerColors);
                header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
                header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
                header.setPtrFrameLayout(ptrFrameLayout);

                ptrFrameLayout.setHeaderView(header);
                ptrFrameLayout.addPtrUIHandler(header);

                //footer
        //        StoreHouseHeader footer = new StoreHouseHeader(this);
        //        footer.setPadding(0, PtrLocalDisplay.dp2px(20), 0, PtrLocalDisplay.dp2px(20));
        //        footer.initWithString("Ultra Footer");


                // footer
        //        final StoreHouseHeader footer = new StoreHouseHeader(this);
        //        footer.setPadding(0, PtrLocalDisplay.dp2px(15), 0, 0);
        //        footer.initWithPointList(PtrLocalDisplay.getPointList());

                // footer
        //        final RentalsSunHeaderView footer = new RentalsSunHeaderView(this);
        //        footer.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        //        footer.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        //        footer.setUp(mPtrFrameLayout);

                // footer
                final MaterialHeader footer = new MaterialHeader(getActivity());
                int[] footerColors = getResources().getIntArray(R.array.google_colors);
                footer.setColorSchemeColors(footerColors);
                footer.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
                footer.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
                footer.setPtrFrameLayout(ptrFrameLayout);

                ptrFrameLayout.setFooterView(footer);
                ptrFrameLayout.addPtrUIHandler(footer);

        //        ptrFrameLayout.setPinContent(true);//原生状态
        //        ptrFrameLayout.setLoadingMinTime(1000);
                ptrFrameLayout.setDurationToCloseHeader(1500);
                ptrFrameLayout.setPtrHandler(new PtrDefaultHandler2() {
                    @Override
                    public void onLoadMoreBegin(PtrFrameLayout ptrFrameLayout) {
                        textBeanList.addAll(dummyStrings());
                        simpleRecyclerCardAdapter.addList(textBeanList);
                        ptrFrameLayout.refreshComplete();
                    }

                    @Override
                    public void onRefreshBegin(PtrFrameLayout ptrFrameLayout) {
                        textBeanList.clear();
                        textBeanList.addAll(dummyStrings());
                        simpleRecyclerCardAdapter.addList(textBeanList);
                        ptrFrameLayout.refreshComplete();
                    }
                });
                ptrFrameLayout.refreshComplete();



                  <nearby.libs.ptr.PtrFrameLayout
                        android:id="@+id/ptr_frame_layout"
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:layout_marginBottom="@dimen/view_bottom_margin"
                        android:background="@color/gold"
                        cube_ptr:ptr_duration_to_close="200"
                        cube_ptr:ptr_duration_to_close_header="1000"
                        cube_ptr:ptr_keep_header_when_refresh="true"
                        cube_ptr:ptr_mode="both"
                        cube_ptr:ptr_pull_to_fresh="false"
                        cube_ptr:ptr_ratio_of_header_height_to_refresh="1.2"
                        cube_ptr:ptr_resistance="1.7">

                        <android.support.v7.widget.RecyclerView
                            android:id="@+id/recyclerView"
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:scrollbars="vertical" />
                    </nearby.libs.ptr.PtrFrameLayout>